<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessList extends Model
{
    protected $table = 'access_list';
}
