<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCosts extends Model
{
    protected $table = "shipping_costs";
    protected $fillable = array("month", "cost", "odoo_id");
}
