<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RechargeProduct extends Model
{
    /**
     * @var string
     */
    protected $table = 'recharge_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['variant_id', 'product_title', 'variant_title', 'product_id', 'price', 'value'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $id
     * @param $column
     * @return array
     */
    public static function getProduct($id, $column)
    {
        return DB::table('recharge_products')
            ->where('variant_id', '=', $id)
            ->pluck($column);
    }
}
