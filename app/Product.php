<?php

namespace App;

use DB;
use Carbon\Carbon;
use App\Services\Curl;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @param $id
     * @return array|static[]
     */
    public static function getActiveGslApp($id)
    {
        return DB::table('gsl')
            ->where('id', '=', $id)
            ->where('campaign_active', '=', '1')
            ->take(1)
            ->get();
    }

    /**
     * @param $id
     * @return array|static[]
     */
    public static function getGslApp($id)
    {
        return DB::table('gsl')
            ->where('id', '=', $id)
            ->take(1)
            ->get();
    }

    /**
     * @param $id
     * @return array|static[]
     */
    public static function getGslProductId($id)
    {
        return DB::table('gsl')
            ->where('product_id', '=', $id)
            ->take(1)
            ->get();
    }

    /**
     * @param $array
     */
    public static function insertGslCampaign($array)
    {
        $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $array['end_date'], 'America/Chicago');

        DB::table('gsl')->insert([
            'product_id' => $array['product_id'],
            'end_date' => $end_date->toDateTimeString(),
            'sales_goal' => $array['sales_goal'],
            'min_quantity' => $array['min_quantity'],
            'payout' => $array['payout'],
            'display_payout' => $array['display_payout'],
            'payout_title' => $array['payout_title'],
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * @param $id
     * @param $array
     */
    public static function updateGslCampaign($id, $array)
    {
        $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $array['end_date'], 'America/Chicago');

        DB::table('gsl')->where('id', '=', $id)->update([
            'product_id' => $array['product_id'],
            'end_date' => $end_date->toDateTimeString(),
            'sales_goal' => $array['sales_goal'],
            'min_quantity' => $array['min_quantity'],
            'payout' => $array['payout'],
            'display_payout' => $array['display_payout'],
            'payout_title' => $array['payout_title'],
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * @param $id
     */
    public static function deleteCampaign($id)
    {
        DB::table('gsl')
            ->where('id', '=', $id)
            ->delete();
    }

    /**
     * @return array|static[]
     */
    public static function listInactiveL()
    {
        return DB::table('gsl')
            ->where('campaign_active', '=', '0')
            ->orderBy('end_date', 'desc')
            ->get();
    }

    /**
     * @return array|static[]
     */
    public static function listActiveL()
    {
        return DB::table('gsl')
            ->where('campaign_active', '=', '1')
            ->get();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function isL($id)
    {
        $api_url = 'https://' . '';
        $path = 'admin/products/'.$id.'.json';
        $url = $api_url.'/'.$path;
        $data = json_decode(Curl::get($url));

        if (! isset($data->errors)) {
            if (self::hasTag('L', $data->product->tags) === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool
     */
    public static function hasTag($needle, $haystack)
    {
        $haystack = explode(', ', $haystack);

        foreach ($haystack as $stack) {
            if (strtolower($needle) == strtolower($stack)) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    public static function check_for_expired()
    {
        $rows = DB::table('gsl')
            ->where('campaign_active', '=', '1')
            ->get();

        foreach ($rows as $row) {
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $row->end_date, 'America/Chicago');

            if ($end_date <= Carbon::now()->setTimezone('America/Chicago')) {
                DB::table('gsl')
                    ->where('id', '=', $row->id)
                    ->update([
                        'campaign_active' => 0,
                        'updated_at' => Carbon::now()->setTimezone('America/Chicago')->toDateTimeString(),
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return array|static[]
     */
    public static function ajax_data($id)
    {
        return DB::table('gsl')
            ->where('product_id', '=', $id)
            ->where('campaign_active', '=', '1')
            ->take(1)
            ->get();

    }

    /**
     * @param $id
     * @return int
     */
    public static function getSalesGoal($id)
    {
        $result = DB::table('gsl')
            ->where('product_id', '=', $id)
            ->pluck('sales_goal');

        if (empty($result)) {
            return 0;
        }

        return $result[0];
    }

    /**
     * @param $id
     * @return int
     */
    public static function getSoldItems($id)
    {
        $result = DB::table('gsl')
            ->where('product_id', '=', $id)
            ->pluck('items_sold');

        if (empty($result)) {
            return 0;
        }

        return $result[0];
    }

    /**
     * @param $id
     * @return int
     */
    public static function getSalesGoalById($id)
    {
        $result = DB::table('gsl')
            ->where('id', '=', $id)
            ->pluck('sales_goal');

        if (empty($result)) {
            return 0;
        }

        return $result[0];
    }

    /**
     * @param $id
     * @return int
     */
    public static function getSoldItemsById($id)
    {
        $result = DB::table('gsl')
            ->where('id', '=', $id)
            ->pluck('items_sold');

        if (empty($result)) {
            return 0;
        }

        return $result[0];
    }

    /**
     * @param $id
     */
    public static function deactivateL($id)
    {
        DB::table('gsl')
            ->where('id', '=', $id)
            ->update([
                'campaign_active' => 0,
                'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * @param $id
     */
    public static function activateL($id)
    {
        DB::table('gsl')
            ->where('id', '=', $id)
            ->update([
                'campaign_active' => 1,
                'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
