<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerData extends Model
{
    protected $table = "customer_data";
    protected $fillable = ["name", "email", "shopify_id", "lifetime_value", "last_order"];
}
