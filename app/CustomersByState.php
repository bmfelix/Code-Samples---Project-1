<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersByState extends Model
{
    protected $table = "customers_by_state";
    protected $fillable = array("state", "quantity");
}
