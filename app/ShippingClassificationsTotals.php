<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingClassificationsTotals extends Model
{
    protected $table = 'shipping_classifications_totals';
    protected $fillable = ['month', 'type', 'quantity'];
}
