<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingClassifications extends Model
{
   protected $table = "shipping_classifications";
   protected $fillable = array('month', 'type', 'shopify_id');
}
