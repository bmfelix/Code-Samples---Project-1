<?php

namespace App\Services;

use Mail;

class Curl
{
    /**
     * @param $url
     * @param $json
     * @return mixed
     */
    public static function put($url, $json)
    {
        usleep(500000);
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        self::checkForErrors($response, $url);

        return $response;
    }

    /**
     * @param $response
     * @param $url
     */
    private static function checkForErrors($response, $url)
    {
        $json = json_decode($response);

        if (isset($json->error)) {
            
        }
    }

    /**
     * @param $url
     * @param $json
     * @return mixed
     */
    public static function post($url, $json = NULL, $http_headers = null)
    {
        usleep(500000);
        $curl = curl_init();

        if($http_headers === null){
            $headers = ['Content-Type:application/json'];
        } else {
            $headers = $http_headers;
        }

        if($json === null){
            $json = json_encode([]);
        }
        
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => $headers,
        ]);
        

        $response = curl_exec($curl);
        curl_close($curl);
        self::checkForErrors($response, $url);

        return $response;
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function get($url)
    {
        usleep(500000);
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        self::checkForErrors($response, $url);

        return $response;
    }

    /**
     * @param $url
     * @param $json
     * @return mixed
     */
    public static function delete($url)
    {
        usleep(500000);
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'DELETE',
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        self::checkForErrors($response, $url);

        return $response;
    }
}
