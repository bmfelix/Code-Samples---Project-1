<?php

namespace App\Services;

use App\Services\Curl;

class Shopify
{
    public static function getOrCreateShopifyAccount($info)
    {
        $api_url = 'https://' . '';
        $url = $api_url . '/admin/customers/search.json?query=' . $info['email'];

        $customer_info = Curl::get($url);
        $decoded_info = json_decode($customer_info, true);

        if (!empty($decoded_info['customers'])) {
            return $decoded_info['customers'][0]['id'];
        } else {
            return self::createShopifyAccount($info);
        }

    }

    public static function createShopifyAccount($info)
    {

        $api_url = 'https://' . '';
        $url = $api_url . '/admin/customers.json';

        $json = json_encode([
            'customer' => [
                'accepts_marketing' => true,
                'first_name' => $info['first_name'],
                'last_name' => $info['last_name'],
                'email' => $info['email'],
                'addresses' => [
                    [
                        'address1' => $info['address1'],
                        'address2' => array_key_exists('address2', $info) ? $info['address2'] : null,
                        'city' => $info[ 'city' ],
                        'province' => $info['state'],
                        'zip' => $info['zip'],
                        'country' => $info['country']
                    ]
                ],
                'send_email_invite' => true
            ]
        ]);

        $response = Curl::post($url, $json);
        $newCustomer = json_decode($response);
        $newCustomerId = $newCustomer->customer->id;

        return $newCustomerId;
    }

    public static function addCustomerTag($id, $tag) 
    {
        $api_url = 'https://' . '';
        $url = $api_url . "/admin/customers/".$id.".json";
        
        $request = [
            "customer" => [
                "id" => $id,
                "tags" => Shopify::existingCustomerTags($id).', '.$tag
            ]
        ];

        $response = Curl::put($url, json_encode($request));

        return json_decode($response);
    }

    public static function existingCustomerTags($id)
    {
        $api_url = 'https://' . '';
        $url = $api_url . "/admin/customers/".$id.".json";

        $response = json_decode(Curl::get($url));
        
        return $response->customer->tags;
    }


}