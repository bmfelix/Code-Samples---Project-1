<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Services\Curl;

class GiveawayEntry extends Model
{
    //
    protected $table = 'giveaway';
    protected $fillable = [
        'first_name',
        'last_name',
        'shopify_id',
        'email',
        'address1',
        'address2',
        'city',
        'state',
        'zip'
    ];

    public static function alreadyEnteredToday($email)
    {
        $today = Carbon::now();

        $previousEntries = GiveawayEntry::where('email', $email)->get()->all();

        foreach ($previousEntries as $entry){
            if ($entry->created_at->isSameDay($today)){
                return true;
            } 
        }

        return false;
    }

    // public static function addContestTag($)

    // public static function createShopifyAccount()
}