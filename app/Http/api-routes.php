<?php

Route::group(['middleware' => 'cors', 'prefix' => 'api'], function()
{
    // Route::resource('authenticate', 'APIController', ['only' => ['index']]);
    Route::get('google/sheets', ['middleware' => 'jwt.auth', 'uses' => 'GoogleController@getData']);
    Route::post('authenticate', 'APIController@authenticate');
    Route::post('giveaway', ['uses' => 'GiveawayController@create']);
});