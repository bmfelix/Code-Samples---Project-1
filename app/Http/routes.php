<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        if (Auth::check()) {
            if (Auth::user()->hasRole('admin')) {
                return View::make('pages.admin');
            } elseif (Auth::user()->hasRole('shipping')) {
                return View::make('pages.shipping');
            } elseif (Auth::user()->hasRole('customerservice')) {
                return View::make('pages.customerservice');
            } elseif (Auth::user()->hasRole('wholesale')) {
                return View::make('pages.gsretailer');
            } elseif (Auth::user()->hasRole('operations')) {
                return View::make('pages.ops');
            } elseif (Auth::user()->hasRole('customs')) {
                return View::make('pages.customs');
            } elseif (Auth::user()->hasRole('club')) {
                return View::make('pages.club');
            } elseif (Auth::user()->hasRole('jobs')) {
                return View::make('pages.jobs');
            } elseif (Auth::user()->hasRole('designer')) {
                return View::make('pages.imageupload'); 
            } else {
                return View::make('pages.home');
            }
        } else {
            return View::make('auth.login');
        }
    });

    Route::get('login', function () {
        if (Auth::check()) {
            if (Auth::user()->hasRole('admin')) {
                return View::make('pages.admin');
            } elseif (Auth::user()->hasRole('shipping')) {
                return View::make('pages.shipping');
            } elseif (Auth::user()->hasRole('customerservice')) {
                return View::make('pages.customerservice');
            } elseif (Auth::user()->hasRole('wholesale')) {
                return View::make('pages.gsretailer');
            } elseif (Auth::user()->hasRole('operations')) {
                return View::make('pages.ops');
            } elseif (Auth::user()->hasRole('customs')) {
                return View::make('pages.customs');
            } elseif (Auth::user()->hasRole('club')) {
                return View::make('pages.club');
            } elseif (Auth::user()->hasRole('designer')) {
                return View::make('pages.imageupload'); 
            } else {
                return View::make('pages.home');
            }
        } else {
            return View::make('auth.login');
        }
    });

    Route::get('queue-monitor', function () {
        return Response::view('queue-monitor::status-page');
    });

    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
        'roles' => 'RolesController',
        'permissions' => 'PermissionsController',
    ]);

    /* IMAGE UPLOAD ROUTES */
    Route::get('image-upload', ['middleware' => ['role:designer|admin'], 'uses' => 'Uploads\UploadsController@index']);
    Route::post('image-upload', ['middleware' => ['role:designer|admin'], 'uses' => 'Uploads\UploadsController@UploadImage']);
    /* END OF IMAGE UPLOAD ROUTES */

    /* GENERAL ROUTES */
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');
    Route::post('shopify/hook/customer', ['uses' => 'Shopify\ShopifyController@customerhook']);
    Route::post('shopify/hook/starbucks', ['uses' => 'Shopify\ShopifyController@starbuckshook']);
    Route::post('shopify/hook/tagcustomer', ['uses' => 'Shopify\ShopifyController@tagCustomer']);
    Route::post('shopify/hook/twoFour_inventory', ['uses' => 'Shopify\ShopifyController@twoFour_inventory']);
    Route::post('shopify/hook/twoFour_orders', ['uses' => 'Shopify\ShopifyController@twoFour_orders']);
    Route::post('shopify/hook/split_orders', ['uses' => 'Shopify\ShopifyController@split_orders']);
    Route::post('shopify/hook/walkin', ['uses' => 'Shopify\ShopifyController@walkin']);
    Route::post('shopify/hook/freetier', ['uses' => 'Shopify\ShopifyController@add_free_tier']);
    Route::post('shopify/hook/gsl', ['uses' => 'Shopify\ShopifyController@gsl_orders']);
    Route::get('shopify/unfraud', ['uses' => 'Shopify\ShopifyController@cancelFraudById']);
    Route::get('admin/jobs', ['uses' => 'Admin\AdminController@jobs', 'as' => 'admin.jobs']);
    Route::post('alerts/meraki', ['uses' => 'Admin\AdminController@merakiAlerts']);
    /* END OF CORS ROUTES */

    /* CORS ROUTES */
    Route::get('ao/{id}/edit', ['middleware' => 'cors', 'uses' => 'AlphaOutpost\AOController@edit']);
    Route::get('ao/{id}/show', ['middleware' => 'cors', 'uses' => 'AlphaOutpost\AOController@show']);
    Route::get('shipping/clock_init/{reset?}', ['middleware' => 'cors', 'uses' => 'Shipping\ShippingController@clock_init']);
    Route::get('shipping/clock_reset', ['middleware' => 'cors', 'uses' => 'Shipping\ShippingController@clock_reset']);
    Route::get('shipping/clock_update/{pause?}', ['middleware' => 'cors', 'uses' => 'Shipping\ShippingController@clock_update']);
    Route::get('shipping/clock_paused', ['middleware' => 'cors', 'uses' => 'Shipping\ShippingController@clock_paused']);
    Route::get('shopifyapp/oauth/callback', ['middleware' => 'cors', 'uses' => 'Shopify\ShopifyAppController@oauth']);
    Route::get('shopifyapp/toolbox/{access_token?}', ['middleware' => 'cors', 'uses' => 'Shopify\ShopifyAppController@toolbox', 'as' => 'shopify.toolbox']);
    Route::get('shopifyapp/gcimport', ['middleware' => 'cors', 'uses' => 'Shopify\ShopifyAppController@gcimport', 'as' => 'shopify.gcimport']);
    Route::post('shopifyapp/gcimport/upload', ['middleware' => 'cors', 'uses' => 'Shopify\ShopifyAppController@gcupload']);
    /* END OF CORS ROUTES */

    /* SHIPPING ROUTES */
    Route::get('shipping/timer', ['middleware' => ['role:shipping|admin'], 'uses' => 'Shipping\ShippingController@timer']);
    Route::get('shipping/walkins', ['middleware' => ['role:shipping|admin'], 'uses' => 'Shipping\ShippingController@walkin']);
    Route::get('shipping/walkindata', ['middleware' => ['role:shipping|admin'], 'uses' => 'Shipping\ShippingController@walkindata']);
    Route::get('shipping/updatewalkindata/{id?}', ['middleware' => ['role:shipping|admin'], 'uses' => 'Shipping\ShippingController@updatewalkindata']);
    /* END OF SHIPPING ROUTES */

    /* CUSTOMER SERVICE ROUTES */
    Route::get('cs/walkins', ['middleware' => ['role:customerservice|admin'], 'uses' => 'Shipping\ShippingController@cswalkins', 'as' => 'cs.walkins']);
    Route::patch('cs/{id}/completewalkin', ['middleware' => ['role:customerservice|admin'], 'uses' => 'Shipping\ShippingController@completewalkin']);
    Route::get('ao/codes', ['middleware' => ['role:customerservice|admin'], 'uses' => 'AlphaOutpost\AOController@index', 'as' => 'ao.home']);
    Route::post('ao/search', ['middleware' => ['role:customerservice|admin'], 'uses' => 'AlphaOutpost\AOController@search']);
    Route::patch('ao/{id}/reactivate', ['middleware' => ['role:customerservice|admin'], 'uses' => 'AlphaOutpost\AOController@reactivate']);
    Route::get('ao/codegen/{number}', ['middleware' => ['role:customerservice|admin'], 'uses' => 'AlphaOutpost\AOController@generate_codes']);
    Route::get('cs/giftcards', ['middleware' => ['role:customerservice|admin'], 'uses' => 'CustomerService\CustomerServiceController@index', 'as' => 'cs.giftcards']);
    Route::post('cs/giftcards/search', ['middleware' => ['role:customerservice|admin'], 'uses' => 'CustomerService\CustomerServiceController@search']);
    /* END OF CUSTOMER SERVICE ROUTES */

    /* JUST REQUIRES AUTHENTICATION ROUTES*/
    Route::get('account/{id}/edit', ['middleware' => ['auth'], 'uses' => 'Accounts\AccountsController@index', 'as' => 'user.edit']);
    Route::patch('account/{id}/edit', ['middleware' => ['auth'], 'uses' => 'Accounts\AccountsController@userupdate']);
    /* END OF JUST REQUIRES AUTHENTICATION ROUTES*/

    /* ADMIN ONLY ROUTES */
    Route::get('accounts/create/{new?}', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@create', 'as' => 'account.create']);
    Route::post('accounts/create/{new?}', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@create']);
    Route::get('accounts/{id}/edit', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@edit', 'as' => 'account.edit']);
    Route::patch('accounts/{id}/edit', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@update']);
    Route::delete('accounts/{id}/delete', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@destroy']);
    Route::get('role/create/{new?}', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@rolecreate']);
    Route::post('role/create/{new?}', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@rolecreate', 'as' => 'role.create']);
    Route::get('role/{id}/edit', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@roleedit', 'as' => 'role.edit']);
    Route::patch('role/{id}/edit', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@roleupdate']);
    Route::delete('role/{id}/delete', ['middleware' => ['role:admin'], 'uses' => 'Accounts\AccountsController@roledestroy']);
    Route::get('admin/accounts', ['middleware' => ['role:admin'], 'uses' => 'Admin\AdminController@accounts', 'as' => 'admin.accounts']);
    Route::get('admin/roles', ['middleware' => ['role:admin'], 'uses' => 'Admin\AdminController@roles', 'as' => 'admin.roles']);
    Route::get('admin/failedjobs', ['middleware' => ['role:admin|jobs'], 'uses' => 'Admin\AdminController@failedjobs', 'as' => 'admin.failedjobs']);
    Route::get('admin/walkinstats', ['middleware' => ['role:admin|shipping|customerservice'], 'uses' => 'Admin\AdminController@walkinstats', 'as' => 'admin.walkinstats']);
    Route::get('admin/soundtest', ['middleware' => ['role:admin'], 'uses' => 'Admin\AdminController@soundtest', 'as' => 'admin.soundtest']);
    Route::get('ao/importcodes', ['middleware' => ['role:admin'], 'uses' => 'AlphaOutpost\AOController@importcodes', 'as' => 'ao.importcodes']);
    Route::post('ao/uploadcodes', ['middleware' => ['role:admin'], 'uses' => 'AlphaOutpost\AOController@uploadcodes', 'as' => 'ao.uploadcodes']);
    Route::get('admin/reports', ['middleware' => ['role:admin'], 'uses' => 'Reports\ReportsController@index']);
    Route::get('admin/reports/shipping-shopify', ['uses' => 'Reports\ReportsController@getShippingDataFromShopify']);
    Route::get('admin/reports/shipping-odoo', ['uses' => 'Reports\ReportsController@getShippingDataFromOdoo']);
    Route::get('admin/reports/shipping-classifications-totals', ['uses' => 'Reports\ReportsController@sumShippingClassifications']);
    Route::get('admin/reports/customers', ['uses' => 'Reports\ReportsController@getCustomerData']);
    /* END OF ADMIN ONLY ROUTES */

    /*  RETAILER ROUTES */
    Route::get('retailer/app', ['uses' => 'Accounts\RetailerController@retail', 'as' => 'shopify.retail']);
    Route::post('retailer/app', ['middleware' => 'cors', 'uses' => 'Accounts\RetailerController@retail_app']);
    Route::get('retailer/apps', ['middleware' => ['role:wholesale|admin'], 'uses' => 'Accounts\RetailerController@retail_approve', 'as' => 'shopify.retailapps']);
    Route::get('retailer/{id}/approve', ['middleware' => ['role:wholesale|admin'], 'uses' => 'Accounts\RetailerController@retail_application', 'as' => 'shopify.retailapprove']);
    Route::patch('retailer/{id}/approve', ['middleware' => ['role:wholesale|admin'], 'uses' => 'Accounts\RetailerController@retail_application_approve', 'as' => 'shopify.retailapproval']);
    Route::delete('retailer/{id}/remove', ['middleware' => ['role:wholesale|admin'], 'uses' => 'Accounts\RetailerController@destroy']);
    Route::get('retailer/{id}/delete', ['middleware' => ['role:wholesale|admin'], 'uses' => 'Accounts\RetailerController@removeApplication']);

    Route::get('recharge/{id}/showshit', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@showShit']);
    /* END OF  RETAILER ROUTES */

    /* L CAMPAIGN ROUTES */
    Route::get('campaign/new', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@newcampaign', 'as' => 'ops.campaign']);
    Route::post('campaign/new', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@campaignsubmit']);
    Route::get('campaign/list', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@campaign_list', 'as' => 'ops.list']);
    Route::get('campaign/{id}/edit', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@campaign_edit', 'as' => 'ops.update']);
    Route::patch('campaign/{id}/edit', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@campaignupdate', 'as' => 'ops.updatesubmit']);
    Route::get('campaign/{id}/delete', ['middleware' => ['role:operations|admin'], 'uses' => 'L\CampaignController@destroy']);
    Route::get('campaign/{id}/show', ['middleware' => 'cors', 'uses' => 'L\CampaignController@campaign']);
    Route::get('campaign/{id}/activate', ['middleware' => 'cors', 'uses' => 'L\CampaignController@activate_campaign']);
    Route::get('campaign/{id}/deactivate', ['middleware' => 'cors', 'uses' => 'L\CampaignController@deactivate_campaign']);
    Route::get('campaign/{id}/data', ['middleware' => 'cors', 'uses' => 'L\CampaignController@ajax_data']);
    /* END L CAMPAIGN ROUTES */

    /* CLUB ROUTES */
    Route::get('club/update', ['middleware' => ['role:club|admin'], 'uses' => 'Club\ClubController@index', 'as' => 'club.club']);
    Route::get('club/bulktag', ['middleware' => ['role:club|admin'], 'uses' => 'Club\ClubController@active', 'as' => 'club.active']);
    Route::post('club/deactivate', ['middleware' => ['role:club|admin'], 'uses' => 'Club\ClubController@deactivate']);
    Route::post('club/activate', ['middleware' => ['role:club|admin'], 'uses' => 'Club\ClubController@activate']);
    /* END OF CLUB ROUTES */

    /* EXPORTS ROUTES */
    Route::get('exports/run/{type}', ['middleware' => ['role:admin'], 'uses' => 'Exports\ExportsController@run']);
    /* END OF EXPORTS ROUTES */

    /* twoFour THRESHOLDS */
    Route::get('threshold', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@index', 'as' => 'twoFour.home']);
    Route::get('threshold/import', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@create', 'as' => 'twoFour.import']);
    Route::post('threshold/upload', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@uploadskus', 'as' => 'twoFour.upload']);
    Route::get('threshold/{id}/edit', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@show', 'as' => 'twoFour.update']);
    Route::patch('threshold/{id}/edit', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@update', 'as' => 'twoFour.updatesubmit']);
    Route::post('threshold/search', ['middleware' => ['role:operations|admin'], 'uses' => 'Ops\ThresholdController@search']);
    Route::get('shopify/threshold/{sku}', ['middleware' => ['role:operations|admin'], 'uses' => 'Shopify\ShopifyController@getThreshold']);
    Route::get('shopify/twoFourdbclean', ['middleware' => ['role:admin'], 'uses' => 'Shopify\ShopifyController@cleanupDatabase']);
    /* END OF twoFour THRESHOLDS */

    /* ODOO-RETAILER ROUTES */
    Route::post('odooretail/hook/order/inbound', ['uses' => 'Exports\RetailerController@syncOrder']);                                            //live order import from shopify
    Route::post('odooretail/hook/order/inboundafterpaid', ['uses' => 'Exports\RetailerController@syncOrderAfterPaid']);
    Route::get('odooretail/live/fulfill/{origin}', ['uses' => 'Exports\RetailerController@fulfillOdooOrder']);                                //mark items fulfilled in shopify from odoo
    Route::get('odooretail/invsync/{sku}/{qty}/{shopify_id}', ['uses' => 'Exports\RetailerController@liveUpdateInv']);                        //update inventory levels in shopify from odoo
    Route::post('odooretail/cancel/order', ['uses' => 'Exports\RetailerController@cancelOdooOrder']);                                        //cancel odoo order
    Route::get('odooretail/test/idupdate', ['middleware' => ['role:admin'], 'uses' => 'Exports\RetailerController@updateOdooDBWithShopID']);    //update products in odoo with their associated shopify product id's
    Route::get('odooretail/import/{id}', ['uses' => 'Exports\RetailerController@importOrderByID']);                                            //import single order by shopify id
    Route::get('shopifyretail/archive', ['middleware' => ['role:admin'], 'uses' => 'Exports\RetailerController@archiveCancelled']);            //bulk archive cancelled orders in shopify
    Route::get('odooretail/order/available', ['uses' => 'Exports\RetailerController@getClubOrderStockIDs']);                                    //force item availibility, loop through ID's
    Route::get('odooretail/create/label', ['uses' => 'Exports\RetailerController@generateLabelMethod']);                                        //create shipping labels or tracking numbers
    Route::post('odooretail/create/product', ['uses' => 'Exports\RetailerController@createProduct']);                                            //attempts at creating products
    /* END OF ODOO-RETAILER ROUTES */

    /* ODOO ROUTES */
    Route::post('odoo/hook/order/inbound', ['uses' => 'Exports\OdooController@syncOrder']);                                            //live order import from shopify
    Route::post('odoo/hook/order/inboundafterpaid', ['uses' => 'Exports\OdooController@syncOrderAfterPaid']);
    Route::get('odoo/live/fulfill/{origin}', ['uses' => 'Exports\OdooController@fulfillOdooOrder']);                                //mark items fulfilled in shopify from odoo
    Route::get('odoo/invsync/{sku}/{qty}/{shopify_id}', ['uses' => 'Exports\OdooController@liveUpdateInv']);                        //update inventory levels in shopify from odoo
    Route::post('odoo/cancel/order', ['uses' => 'Exports\OdooController@cancelOdooOrder']);                                        //cancel odoo order
    Route::get('odoo/test/idupdate', ['middleware' => ['role:admin'], 'uses' => 'Exports\OdooController@updateOdooDBWithShopID']);    //update products in odoo with their associated shopify product id's
    Route::get('odoo/secretitimport/{id}', ['uses' => 'Exports\OdooController@importOrderByIDLive']);                                            //import single order by shopify id
    Route::get('odoo/neededimports', ['uses' => 'Exports\OdooController@runNeededImports']); 
    Route::get('odoo/neededimportslive', ['uses' => 'Exports\OdooController@runLiveImports']);
    Route::get('odoo/cron/clubcrons', ['uses' => 'Exports\OdooController@runNeededImportsClub']);
    Route::get('odoo/cron/twoFourcrons', ['uses' => 'Exports\OdooController@runNeededImportstwoFour']);
    Route::get('odoo/cron/clubcronslive', ['uses' => 'Exports\OdooController@runLiveImportsClub']);
    Route::get('odoo/cron/twoFourcronslive', ['uses' => 'Exports\OdooController@runLiveImportstwoFour']);
    Route::get('odoo/import/{id}', ['uses' => 'Exports\OdooController@importOrderByID']);
    Route::get('odoo/runconfirm', ['uses' => 'Exports\OdooController@runOdooConfirm']); 
    Route::get('shopify/archive', ['middleware' => ['role:admin'], 'uses' => 'Exports\OdooController@archiveCancelled']);            //bulk archive cancelled orders in shopify
    Route::get('odoo/order/available', ['uses' => 'Exports\OdooController@getClubOrderStockIDs']);                                    //force item availibility, loop through ID's
    Route::get('odoo/create/label', ['uses' => 'Exports\OdooController@generateLabelMethod']);                                        //create shipping labels or tracking numbers
    Route::get('shopify/create_gc', ['middleware' => ['role:admin'], 'uses' => 'Shopify\ShopifyController@create_giftcard']);        //bulk create giftcards from email array
    Route::post('odoo/create/product', ['uses' => 'Exports\OdooController@createProduct']);                                            //attempts at creating products
    Route::get('report/totals/{start?}/{stop?}', ['uses' => 'Exports\OdooController@getShopifyOrderCountByDate']);
    Route::get('odoo/pullimports/{start?}/{stop?}', ['uses' => 'Exports\OdooController@pullImports']); 
    
    /* END OF ODOO ROUTES */

    Route::get('shopify/create/gc', ['uses' => 'Shopify\ShopifyController@createGiftCards']);
    Route::get('shopify/clubfix', ['uses' => 'Shopify\ShopifyController@runClubFix']);
    
    

    /* LEGACY ODOO ROUTES */
    Route::get('odoo/orders/{type}/{start}/{stop}', ['uses' => 'Exports\OdooController@getOrders']);
    Route::get('odoo/fix/lineitems', ['uses' => 'Exports\OdooController@fixImportLineItems']);
    Route::get('odoo/bulkimport', ['uses' => 'Exports\OdooController@bulkImportByNumber']);
    Route::get('odoo/unfuck', ['uses' => 'Exports\OdooController@unFuckOdoo']);
    Route::get('odoo/closeold', ['uses' => 'Exports\OdooController@closeOldOrders']);
    /* END OF LEGACY ODOO ROUTES */

    /* ODOO CRONS */
    Route::get('odoo/cron/quotes', ['uses' => 'Exports\OdooController@fixQuotesCron']);
    Route::get('odoo/cron/lmquotes', ['uses' => 'Exports\OdooController@fixQuotesLastMonthCron']);
    Route::get('odoo/cron/wa', ['uses' => 'Exports\OdooController@fixWACron']);
    Route::get('odoo/cron/dupes', ['uses' => 'Exports\OdooController@fixDupesCron']);
    Route::get('odoo/cron/clubdupes', ['uses' => 'Exports\OdooController@fixClubDupesCron']);
    Route::get('odoo/cron/lmwa', ['uses' => 'Exports\OdooController@fixLastMonthWACron']);
    Route::get('odoo/cron/lmclub', ['uses' => 'Exports\OdooController@fixLastMonthClubCron']);
    Route::get('odoo/cron/lmpa', ['uses' => 'Exports\OdooController@fixLastMonthPACron']);
    Route::get('odoo/cron/pa', ['uses' => 'Exports\OdooController@fixPACron']);
    Route::get('odoo/unreserve/club', ['uses' => 'Exports\OdooController@unreserveClub']);
    Route::get('odoo/reserve/club', ['uses' => 'Exports\OdooController@fixLastMonthClubCron']);
    Route::get('odoo/test', ['uses' => 'Exports\OdooController@testOdoo']);
    Route::get('displayjesus', ['uses' => 'Exports\OdooController@displayJesus']);
    Route::get('odoo/bulkdeliver/{product_id}', ['uses' => 'Exports\OdooController@bulk_deliver']);
    Route::get('odoo/done', ['uses' => 'Exports\OdooController@setDoneStatus']);
    Route::get('odoo/tracking', ['uses' => 'Exports\OdooController@checkTrackingID']);
    Route::post('odoo/customer/update', ['uses' => 'Exports\OdooController@updateCustomer']);
    Route::get('odoo/fixshipstatus', ['uses' => 'Exports\OdooController@fixShippingInfo']);
    /* END OF ODOO CRONS */


    /* TAGGED EMAIL ROUTES */
    Route::get('shopify/emails/importproducts', ['uses' => 'Shopify\ShopifyController@storeProducts']);
    Route::post('shopify/emails/sendemail', ['uses' => 'Shopify\ShopifyController@completedOrders']);
    Route::get('shopify/reports/grab', ['uses' => 'Exports\OdooController@getReport']);
    
    /* END OF TAGGED EMAIL ROUTES */

    /* CUSTOMER ADDRESS ROUTES */
    Route::get('recharge/{id}/changeaddress', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@validateAddressForm']);
    Route::get('recharge/{id}/address', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@getRechargeAddresses']);
    Route::get('recharge/order/{id}/size/{variant}', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@changeRechargeSize']);
    Route::get('recharge/customersearch/', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@validateCustomerSearch']);
    Route::get('recharge/{id}/{type}/club', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@rechargeCustomerSearch']);
    Route::get('recharge/{id}/sizechange/{variant}', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@changeRechargeVariant']);
    Route::get('recharge/getdata', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@sendData']);
    Route::get('recharge/changes', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@displayChanges']);
    Route::get('recharge/{orders}/customers', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@checkShopifyOrder']);
    Route::get('recharge/update/{id}/orderid/{order_id}', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@updateOrderID']);
    Route::get('recharge/shopifyorders', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@getShopifyData']);
    Route::get('recharge/hidechangedorders', ['middleware' => 'cors' ,'uses' => 'Recharge\RechargeController@hideChangedOrders']);
    Route::get('recharge/{orders}/removeorders', ['middleware' => 'cors', 'uses' => 'Recharge\RechargeController@removeOrdersFromView']);
    /* END CUSTOMER ADDRESS ROUTES */

    /* PRS CUSTOMER FORM ROUTES */
    Route::post('shopify/prsformsubmit', ['middleware' => 'cors', 'uses' => 'PRS\PRSController@prsFormSubmit']);
    Route::get('shopify/createprsorder', ['middleware' => 'cors', 'uses' => 'PRS\PRSController@createShopifyOrder']);
    /* END PRS CUSTOMER FORM ROUTES */

    /* API */
    Route::get('shopify/get/totalorders/{day?}', ['uses' => 'API\ShopifyController@storeOrders']);
    Route::get('shopify/get/openorders/{day?}', ['uses' => 'API\ShopifyController@storeOrdersOpen']);
    Route::get('shopify/get/closedorders/{day?}', ['uses' => 'API\ShopifyController@storeOrdersClosed']);
    Route::get('shopify/get/qty/{day?}', ['uses' => 'API\ShopifyController@storeQuantityByDay']);
    Route::get('shopify/get/data/{table?}/{day?}', ['uses' => 'API\ShopifyController@getData']);
    Route::get('shopify/get/datastart/{week?}', ['uses' => 'API\ShopifyController@countWeeks']);
    Route::get('social/comments', ['uses' => 'API\SocialController@getComments']);
    Route::get('drive_images/{image}', ['uses' => 'API\ImageController@show'])->name('images');

    Route::get('fedex/get/token', ['uses' => 'API\FedexController@authenticate']);

    Route::post('quickbase/paynow', ['uses' => 'API\CustomsController@payNow', 'as' => 'customs.pay']);
    Route::get('quickbase/get/token', ['uses' => 'API\CustomsController@authenticate']);
    Route::get('quickbase/search/{order_id?}', ['uses' => 'API\CustomsController@searchByOrderNumber']);
    Route::get('quickbase/pay', ['uses' => 'API\CustomsController@index']);
    Route::get('quickbase/test', ['uses' => 'API\CustomsController@testBuild']);
    Route::get('vpn/test', ['uses' => 'Exports\OdooController@testConnection']);
    
    /* END API */

    /* EMPLOYEE ACCESS */

    Route::get('employee/request/access', ['uses' => 'API\EmployeeController@requestAccess']);
    Route::post('employee/request/access', ['uses' => 'API\EmployeeController@submitAccess', 'as' => 'access.submit']);

    /* END EMPLOYEE ACCESS */

    Route::resource('accounts', 'Accounts\AccountsController');
    Route::resource('shopify', 'Shopify\ShopifyController');
    Route::resource('shipping', 'Shipping\ShippingController');
    Route::resource('ao', 'AlphaOutpost\AOController');
    Route::resource('shopifyapp', 'Shopify\ShopifyAppController');
    Route::resource('campaign', 'L\CampaignController');
    Route::resource('retailer', 'Accounts\RetailerController');
    // Route::resource('customdesigner', 'Designer\DesignerController');
    Route::resource('club', 'Club\ClubController');
    Route::resource('exports', 'Exports\ExportsController');

    
    Route::get('giveaway/winners', ['middleware' => 'role:admin', 'uses' => 'API\GiveawayController@index']);
    Route::get('giveaway/winners/select', ['middleware' => 'role:admin', 'uses' => 'API\GiveawayController@selectWinner']);
    Route::put('giveaway/winners/select', ['middleware' => 'role:admin', 'uses' => 'API\GiveawayController@winner']);
    Route::get('giveaway/winner/cancelWinner/{id}', ['middleware' => 'role:admin', 'uses' => 'API\GiveawayController@cancelWinner']);
});
