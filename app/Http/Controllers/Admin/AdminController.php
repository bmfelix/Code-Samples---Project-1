<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Admin Controller.
 *
 * all actual methods requiring being an admin
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.0
 */
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * display all accounts.
     *
     * @return \Illuminate\Http\Response
     */
    public function accounts()
    {
        $users = User::orderBy('name')->get(); // Get all users from the database
        return view('admin.accounts', ['table' => $users]);
    }

    /**
     * display all roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        $rows = Role::get(); // Get all users from the database
        return view('admin.roles', ['table' => $rows]);
    }

    /**
     * display all currently running queue jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobs(Request $request)
    {
        ini_set('memory_limit', '4096M');
        $default_rows = $this->dredispag(20, $request);
        $fixes_rows = $this->fredispag(20, $request);
        $imports_rows = $this->iredispag(20, $request);
        $club_rows = $this->credispag(20, $request);
        $cron_rows = $this->cronredispag(20, $request);
        $eron_rows = $this->eronredispag(20, $request);
        $wa_rows = $this->waredispag(20, $request);
        $pa_rows = $this->paredispag(20, $request);
        $gs_rows = $this->gsredispag(20, $request);

        return view('admin.jobs', ['drows' => $default_rows, 'frows' => $fixes_rows, 'irows' => $imports_rows, 'crows' => $club_rows, 'cronrows' => $cron_rows, 'eronrows' => $eron_rows, 'warows' => $wa_rows, 'parows' => $pa_rows, 'gsrows' => $gs_rows]);
    }

    public function dredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:default', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function fredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:fixes', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function gsredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:twoFour', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function iredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:import', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function credispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:club', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function cronredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:crons', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function eronredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:edits', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function waredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:waiting_available_crons', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    public function paredispag($perPage = 20, $request){
        $page = Input::get('page', 1); // Get the current page or default to 1
        $offset = ($page * $perPage) - $perPage;
        $max = $offset + $perPage;
        $array = Redis::lrange('queues:partial_available_crons', 0, -1); 

        return new LengthAwarePaginator(
            array_slice(
                $array,
                $offset,
                $perPage,
                true
            ),
            count($array),
            $perPage,
            $page,
            [
                'path' => $request->url(),
                'query' => $request->query()
            ]
        );
    }

    /**
     * display all failed jobs, not nessecarily a live view.
     *
     * @return \Illuminate\Http\Response
     */
    public function failedjobs()
    {
        $rows = DB::table('failed_jobs')->orderby('id', 'DESC')->paginate(20);

        return view('admin.failedjobs', ['rows' => $rows]);
    }

    /**
     * display all walkin orders along with avg times.
     *
     * @return \Illuminate\Http\Response
     */
    public function walkinstats()
    {
        $orders = DB::table('walkin_customers')->where('completed', '=', 1)->get();
        $count = sizeof($orders);
        $time = 0;
        if (! empty($orders)) {
            foreach ($orders as $order) {
                $order->created_at = Carbon::parse(''.$order->created_at.'')->setTimezone('America/Chicago');
                $order_detail = DB::table('walkin_customers_order_detail')->where('parent_id', '=', $order->id)->get();
                $order->detail = $order_detail;
                $time = $time + floatval($order->elapsed_time);
            }

            $avg_time = round(($time / $count), 2);
            if ($avg_time <= 5) {
                $widget = 'green-widget';
            } elseif ($avg_time > 5 && $avg_time <= 7) {
                $widget = 'yellow-widget';
            } else {
                $widget = 'red-widget';
            }

            return view('admin.walkinstats', ['orders' => $orders, 'avg' => $avg_time, 'widget' => $widget]);
        } else {
            return view('admin.walkinstats', ['orders' => '', 'avg' => '', 'widget' => '']);
        }
    }

    /**
     * displays a view with buttons to test sounds.
     *
     * @return \Illuminate\Http\Response
     */
    public function soundtest()
    {
        return view('admin.soundtest');
    }

    public function merakiAlerts(Request $request)
    {
        echo '<pre>';
        print_r($request->all());
        echo '</pre>';
        die;
    }
}
