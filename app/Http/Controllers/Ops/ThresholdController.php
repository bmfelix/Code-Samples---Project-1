<?php

namespace App\Http\Controllers\Ops;

use DB;
use Input;
use Request;
use Session;
use Redirect;
use Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Thresholds;

class ThresholdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = $database_data = DB::table('thresholds')->orderby('updated_at', 'desc')->paginate(100);
        $search = '';

        foreach ($rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
        }

        return view('threshold.threshold')->with(compact('rows', 'search'));
    }

    public function search(Request $request)
    {
        $input = $_POST;
        $rows = $database_data = DB::table('thresholds')->where('sku', 'like', '%'.$input['term'].'%')->paginate(10);

        foreach ($rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
        }

        $search = $input['term'];

        return view('threshold.threshold')->with(compact('rows', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threshold.import');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $app = DB::table('thresholds')->where('id', '=', $id)->take(1)->get();

        return view('threshold.edit', ['app' => $app]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'xs_low_threshold.required' => 'XS Low Threshold must enter a valid input.',
            'xs_low_threshold.numeric' => 'XS Low Threshold can only be a number.',
            's_low_threshold.required' => 'S Low Threshold must enter a valid input.',
            's_low_threshold.numeric' => 'S Low Threshold can only be a number.',
	        's_m_low_threshold.required' => 'S/M Low Threshold must enter a valid input.',
	        's_m_low_threshold.numeric' => 'S/M Low Threshold can only be a number.',
            'm_low_threshold.required' => 'M Low Threshold must enter a valid input.',
            'm_low_threshold.numeric' => 'M Low Threshold can only be a number.',
            'l_low_threshold.required' => 'L Low Threshold must enter a valid input.',
            'l_low_threshold.numeric' => 'L Low Threshold can only be a number.',
            'l_xl_low_threshold.required' => 'L Low Threshold must enter a valid input.',
            'l_xl_low_threshold.numeric' => 'L Low Threshold can only be a number.',
            'xl_low_threshold.required' => 'XL Low Threshold must enter a valid input.',
            'xl_low_threshold.numeric' => 'XL Low Threshold can only be a number.',
            'xxl_low_threshold.required' => '2XL Low Threshold must enter a valid input.',
            'xxl_low_threshold.numeric' => '2XL Low Threshold can only be a number.',
            'xxxl_low_threshold.required' => '3XL Low Threshold must enter a valid input.',
            'xxxl_low_threshold.numeric' => '3XL Low Threshold can only be a number.',
            'xxxxl_low_threshold.required' => '4XL Low Threshold must enter a valid input.',
            'xxxxl_low_threshold.numeric' => '4XL Low Threshold can only be a number.',
            'xs_high_threshold.required' => 'XS High Threshold must enter a valid input.',
            'xs_high_threshold.numeric' => 'XS High Threshold can only be a number.',
            's_high_threshold.required' => 'S High Threshold must enter a valid input.',
            's_high_threshold.numeric' => 'S High Threshold can only be a number.',
            's_m_high_threshold.required' => 'S High Threshold must enter a valid input.',
            's_m_high_threshold.numeric' => 'S High Threshold can only be a number.',
            'm_high_threshold.required' => 'M High Threshold must enter a valid input.',
            'm_high_threshold.numeric' => 'M High Threshold can only be a number.',
            'l_high_threshold.required' => 'L High Threshold must enter a valid input.',
            'l_high_threshold.numeric' => 'L High Threshold can only be a number.',
            'l_xl_high_threshold.required' => 'L High Threshold must enter a valid input.',
            'l_xl_high_threshold.numeric' => 'L High Threshold can only be a number.',
            'xl_high_threshold.required' => 'XL High Threshold must enter a valid input.',
            'xl_high_threshold.numeric' => 'XL High Threshold can only be a number.',
            'xxl_high_threshold.required' => '2XL High Threshold must enter a valid input.',
            'xxl_high_threshold.numeric' => '2XL High Threshold can only be a number.',
            'xxxl_high_threshold.required' => '3XL High Threshold must enter a valid input.',
            'xxxl_high_threshold.numeric' => '3XL High Threshold can only be a number.',
            'xxxxl_high_threshold.required' => '4XL High Threshold must enter a valid input.',
            'xxxxl_high_threshold.numeric' => '4XL High Threshold can only be a number.',
          ];

        $rules = [
            'xs_low_threshold' => 'required|numeric',
            's_low_threshold' => 'required|numeric',
            's_m_low_threshold' => 'required|numeric',
            'm_low_threshold' => 'required|numeric',
            'l_low_threshold' => 'required|numeric',
            'l_xl_low_threshold' => 'required|numeric',
            'xl_low_threshold' => 'required|numeric',
            'xxl_low_threshold' => 'required|numeric',
            'xxxl_low_threshold' => 'required|numeric',
            'xxxxl_low_threshold' => 'required|numeric',
            'xs_high_threshold' => 'required|numeric',
            's_high_threshold' => 'required|numeric',
            'm_high_threshold' => 'required|numeric',
            'l_high_threshold' => 'required|numeric',
            'l_xl_high_threshold' => 'required|numeric',
            'xl_high_threshold' => 'required|numeric',
            'xxl_high_threshold' => 'required|numeric',
            'xxxl_high_threshold' => 'required|numeric',
            'xxxxl_high_threshold' => 'required|numeric',
        ];

        $validation = Validator::make($_POST, $rules, $messages);

        if ($validation->passes()) {
            $result = $this->update_form($id, $_POST);
            if ($result === true) {
                Session::flash('status', 'Threshold Updated!');
            } else {
                Session::flash('statuserror', 'Threshold Not Updated.  Check Error Log.');
            }
        } else {
            return Redirect::route('twoFour.update', $id)
              ->withInput()
              ->withErrors($validation);
        }

        return Redirect::route('twoFour.home');
    }

    public function update_form($id, $post)
    {
        $result = true;

        foreach ($post as $key => $value) {
            $$key = $value;
        }
        $now = Carbon::now();

        DB::table('thresholds')->where('id', '=', $id)->update([
            'xs_low' => $xs_low_threshold,
            's_low' => $s_low_threshold,
            's_m_low' => $s_m_low_theshold,
            'm_low' => $m_low_threshold,
            'l_low' => $l_low_threshold,
            'l_xl_low' => $l_xl_low_threshold,
            'xl_low' => $xl_low_threshold,
            'xxl_low' => $xxl_low_threshold,
            'xxxl_low' => $xxxl_low_threshold,
            'xxxxl_low' => $xxxxl_low_threshold,
            'xs_high' => $xs_high_threshold,
            's_high' => $s_high_threshold,
            's_m_high' => $s_m_high_threshold,
            'm_high' => $m_high_threshold,
            'l_high' => $l_high_threshold,
            'l_xl_high' => $l_xl_high_threshold,
            'xl_high' => $xl_high_threshold,
            'xxl_high' => $xxl_high_threshold,
            'xxxl_high' => $xxxl_high_threshold,
            'xxxxl_high' => $xxxxl_high_threshold,
            'updated_at' => $now->toDateTimeString(),
          ]);

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadskus(Request $request)
    {
        $file = ['file' => Request::file('csv')];
        $rules = ['file' => 'required'];
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
           return \Redirect::route('twoFour.import')->withInput()->withErrors($validator);
        } else {
            $file = ['extension' => strtolower(Request::file('csv')->getClientOriginalExtension())];
            $rules = ['extension' => 'required|in:csv'];
            $validator = Validator::make($file, $rules);

            if ($validator->fails()) {
                // send back to the page with the input data and errors
                return \Redirect::route('twoFour.import')->withInput()->withErrors($validator);
            } else {
                $path = storage_path().'/app/';
                $fileName = \Uuid::generate(4).'.txt'; // renameing image
                  Request::file('csv')->move($path, $fileName); // uploading file to given path
                  // sending back with message

                  $result = $this->processfile($fileName);
                if ($result === true) {
                    Session::flash('status', 'Imported successfully');
                } else {
                    Session::flash('statuserror', 'import error.');
                }
            }
        }

        return Redirect::route('twoFour.home');
    }

    /**
     * handles the actual importing of codes from CSV to database.
     *
     * @param  string $fileName
     * @return bool
     */
    public function processfile($fileName)
    {
        $path = storage_path('app/'.$fileName.'');
        $result = true;
        $master_array = [];

        if (($handle = fopen($path, 'r')) !== false) {
            $now = Carbon::now();
            $count = 0;

            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($count > 0) {
                    $data_array[] = [
                                            'sku'=>$data[0],
                                            'title' =>$data[1],
                                            'xs_low' =>(empty($data[2])? 0 : $data[2]),
                                            's_low' =>(empty($data[3])? 0 : $data[3]),
                                            's_m_low' =>(empty($data[4])? 0 : $data[4]),
                                            'm_low' =>(empty($data[5])? 0 : $data[5]),
                                            'l_low' =>(empty($data[6])? 0 : $data[6]),
                                            'l_xl_low' =>(empty($data[7])? 0 : $data[7]),
                                            'xl_low' =>(empty($data[8])? 0 : $data[8]),
                                            'xxl_low' =>(empty($data[9])? 0 : $data[9]),
                                            'xxxl_low' =>(empty($data[10])? 0 : $data[10]),
                                            'xxxxl_low' =>(empty($data[11])? 0 : $data[11]),
                                            'xs_high' =>(empty($data[13])? 0 : $data[13]),
                                            's_high' =>(empty($data[14])? 0 : $data[14]),
                                            's_m_high' =>(empty($data[15])? 0 : $data[15]),
                                            'm_high' =>(empty($data[16])? 0 : $data[16]),
                                            'l_high' =>(empty($data[17])? 0 : $data[17]),
                                            'l_xl_high' =>(empty($data[18])? 0 : $data[18]),
                                            'xl_high' =>(empty($data[19])? 0 : $data[19]),
                                            'xxl_high' =>(empty($data[20])? 0 : $data[20]),
                                            'xxxl_high' =>(empty($data[21])? 0 : $data[21]),
                                            'xxxxl_high' =>(empty($data[22])? 0 : $data[22]),
                                            ];
                }

                $count++;
            }

            fclose($handle);
        }

        foreach ($data_array as $d) {
            $database_data = Thresholds::where('sku', '=', $d['sku'])->first();

            if (empty($database_data)) {
                DB::table('thresholds')->insert([
                    'sku' => $d['sku'],
                    'title' => $d['title'],
                    'xs_low' => $d['xs_low'],
                    's_low' => $d['s_low'],
                    's_m_low' => $d['s_m_low'],
                    'm_low' => $d['m_low'],
                    'l_low' => $d['l_low'],
                    'l_xl_low' => $d['l_xl_low'],
                    'xl_low' => $d['xl_low'],
                    'xxl_low' => $d['xxl_low'],
                    'xxxl_low' => $d['xxxl_low'],
                    'xxxxl_low' => $d['xxxxl_low'],
                    'xs_high' => $d['xs_high'],
                    's_high' => $d['s_high'],
                    's_m_high' => $d['s_m_high'],
                    'm_high' => $d['m_high'],
                    'l_high' => $d['l_high'],
                    'l_xl_high' => $d['l_xl_high'],
                    'xl_high' => $d['xl_high'],
                    'xxl_high' => $d['xxl_high'],
                    'xxxl_high' => $d['xxxl_high'],
                    'xxxxl_high' => $d['xxxxl_high'],
                    'created_at' => $now->toDateTimeString(),
                    'updated_at' => $now->toDateTimeString(),
                ]);
            } else {
                $database_data = $database_data->toArray();
                $diff = array_diff_assoc($database_data, $d);

                if(count($diff) > 0) {
                    DB::table('thresholds')->where('sku', $d['sku'])->update([
                        'title' => $d['title'],
                        'xs_low' => $d['xs_low'],
                        's_low' => $d['s_low'],
                        's_m_low' => $d['s_m_low'],
                        'm_low' => $d['m_low'],
                        'l_low' => $d['l_low'],
                        'l_xl_low' => $d['l_xl_low'],
                        'xl_low' => $d['xl_low'],
                        'xxl_low' => $d['xxl_low'],
                        'xxxl_low' => $d['xxxl_low'],
                        'xxxxl_low' => $d['xxxxl_low'],
                        'xs_high' => $d['xs_high'],
                        's_high' => $d['s_high'],
                        's_m_high' => $d['s_m_high'],
                        'm_high' => $d['m_high'],
                        'l_high' => $d['l_high'],
                        'l_xl_high' => $d['l_xl_high'],
                        'xl_high' => $d['xl_high'],
                        'xxl_high' => $d['xxl_high'],
                        'xxxl_high' => $d['xxxl_high'],
                        'xxxxl_high' => $d['xxxxl_high'],
                        'updated_at' => $now->toDateTimeString(),
                    ]);
                }
            }
        }

        return $result;
    }
}
