<?php

namespace App\Http\Controllers\Shopify;

use Carbon\Carbon;
use phpish\shopify;
use Storage;
use App\Services\Curl;
use App\Jobs\RunDBFix;
use App\Jobs\RunInv;
use App\Jobs\RunWalkins;
use App\Jobs\RunLOrders;
use App\Jobs\RuntwoFourOrders;
use App\Jobs\RunSGFreeTier;
use App\Jobs\RunSplitOrder;
use App\Jobs\RunSBCustomer;
use App\Jobs\RunTagCustomers;
use App\Jobs\RunGiftCardAuto;
use Illuminate\Http\Request;
use App\Jobs\RunClubCustomer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ShopifyController extends Controller
{
    public function storeProducts()
    {
        $shopify = shopify\client('', '' '', true);
        $now = Carbon::now();

        try {
            $totalproducts = $shopify('GET /admin/products/count.json', ['published_status'=>'online_store']);
            $limit = 250;
            $totalpage = ceil($totalproducts / $limit);

            for ($i = 1; $i <= $totalpage; $i++) {
                $products = $shopify('GET /admin/products.json?limit='.$limit.'&page='.$i);
                foreach ($products as $product) {
                    $product_title = $product['title'];
                    $product_id = $product['id'];
                    $product_tags = $product['tags'];
                    $category = null;

                    $database_data = DB::table('TaggedProducts')->where([
                        ['title', '=', $product_title],
                        ['productId', '=', $product_id],
                    ])->get();

                    if ($this->has_tag('', $product_tags)) {
                        $category = '';
                    } else if ($this->has_tag('', $product_tags)) {
                        $category = '';
                    }

                    if (empty($database_data) && $category !== null) {
                        DB::table('TaggedProducts')->insert([
                            'category' => $category,
                            'title' => $product_title,
                            'productId' => $product_id,
                            'tags' => $product_tags,
                            'created_at' => $now->toDateTimeString(),
                            'updated_at' => $now->toDateTimeString(),
                        ]);
                    }
                }
            }
        } catch (shopify\ApiException $e) {
            Log::error($e->getMessage());

            return;
        }
    }

    public function completedOrders(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            if (isset($order_data->customer)) {
                $customer = $order_data->customer;
            }

            if (isset($order_data->shipping_address)) {
                $shipping_data = $order_data->shipping_address;
            }

            if (isset($order_data->line_items)) {
                $line_items = $order_data->line_items;
            }

            if (isset($order_data->name)) {
                $order_number = $order_data->name;
            }

            $tagged_items = [];
            $lag_tagged_items = [];

            foreach ($line_items as $item) {
                $check_items = DB::table('TaggedProducts')->where('title', '=', $item->title)->first();

                if (! is_null($check_items)) {
                    if ($check_items->category == "") {
                        array_push($lag_tagged_items, $item);
                    } else if ($check_items->category == "") {
                        array_push($tagged_items, $item);
                    }
                }
            }

            if ($this->checkForTaggedItems($order_data, "") === "") {
                $this->sendEmail('orders@.com', $shipping_data, $tagged_items, $order_number, $customer);
            }
            if ($this->checkForTaggedItems($order_data, "") === "") {
                
                $this->sendEmail('', $shipping_data, $lag_tagged_items, $order_number, $customer);
            }

            return;
        }
    }

    public function checkForTaggedItems($order_data, $tag) {
        if (! isset($order_data->customer)) {
            return false;
        }

        $line_items = $order_data->line_items;

        foreach ($line_items as $item) {
            $product_id = $item->product_id;
            $check_id = DB::table('TaggedProducts')->where('productId', '=', $product_id)->first();

            if (! is_null($check_id)) {
                if ($check_id->category === $tag) {
                    return $tag;
                }
            }
        }

        return false;
    }

    public function sendEmail($emails, $shipping_data, $tagged_items, $order_number, $customer)
    {
        
    }

    private function validateSignature($request)
    {
        if ($request->header('x-shopify-hmac-sha256')) {
            $data = file_get_contents('php://input');

            $calculated_hmac = base64_encode(hash_hmac('sha256', $data, ''), true));

            if ($calculated_hmac === $request->header('x-shopify-hmac-sha256')) {
                return true;
            }
        }

        return false;
    }

    public function customerhook(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunClubCustomer($order_data));
            $this->dispatch($job);

            return;
        }
    }

    public function starbuckshook(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            //$job = (new RunSBCustomer($order_data));
            //$this->dispatch($job);
            $this->fireStarbucksUpdate($order_data);
            return;
        }
    }

    public function tagCustomer(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunTagCustomers($order_data));
            $this->dispatch($job);
            //$this->fireTagCustomer($order_data);
            return;
        }
    }

    public function fireClubUpdate($order_data)
    {
        if (! isset($order_data->customer)) {
            return;
        }

        $customer = $order_data->customer;
        if ($customer->id != '') {
            $club_ids = [];

            $api_url = '';
            $path = 'admin/customers/'.$customer->id.'.json';
            $url = $api_url.'/'.$path;
            $customer_data = json_decode(Curl::get($url));

            if (!isset($customer_data->customer->tags)) {
                $is_club_member = false;
            } else {
                $is_club_member = $this->has_tag('', $customer_data->customer->tags);
            }
            $has_club_shirt = $this->has_club_item($club_ids, $order_data->line_items);

            if ($is_club_member === false && $has_club_shirt === true) {
                $json_array = [
                    'customer' => [
                        'id' => $customer_data->customer->id,
                        'tags' => '',
                    ],
                ];

                $json = json_encode($json_array);

                $url = $api_url.'/'.$path;
                $data_return = json_encode(Curl::put($url, $json));
                
            }
        }
    }

    public function fireStarbucksUpdate($data)
    {

        if (! isset($data->email)) {
            return;
        }


        if (($data->email = filter_var($data->email, FILTER_VALIDATE_EMAIL)) !== false) {
            // okay, should be valid now
            $domain = substr($data->email, strrpos($data->email, '@') + 1); 
            
            if($domain === '.com'){
                $json_array = [
                    'customer' => [
                        'id' => $data->id,
                        'tags' => '',
                    ],
                ];

                $json = json_encode($json_array);

                $api_url = '';
                $path = 'admin/customers/'.$data->id.'.json';
                $url = $api_url.'/'.$path;
                $data_return = json_encode(Curl::put($url, $json));
            }
        }

    }

    public function fireTagCustomer($data)
    {
        $data = $data->customer;
  
        if (! isset($data->email)) {
            return;
        }

        if (($data->email = filter_var($data->email, FILTER_VALIDATE_EMAIL)) !== false) {
            // okay, should be valid now
            $now = Carbon::now()->format('Y-m-d');
            
            if($now >= '2018-11-22'  && $now <= '2018-11-30'){
                $json_array = [
                    'customer' => [
                        'id' => $data->id,
                        'tags' => '',
                    ],
                ];

                $json = json_encode($json_array);

                $api_url = '';
                $path = 'admin/customers/'.$data->id.'.json';
                $url = $api_url.'/'.$path;
                $data_return = json_encode(Curl::put($url, $json));
            }
        }

    }

    public function createGiftCards()
    {
        $list = [];

        foreach($list as $l){
            $job = (new RunGiftCardAuto($l))->onQueue('crons');
            $this->dispatch($job);
        }
    }

    public function runSpecificGiftCard($email)
    {
        
            $api_url = '';
            $path = 'admin/customers/search.json?query=email:'.$email.'&limit=1';
            $url = $api_url.'/'.$path;
            $data = json_decode(Curl::get($url));
            


            if(!isset($data->customers[0])){
                $contents = Storage::get('gc.txt');
                
                if($contents){
                    $contents . ", " . $email;
                } else {
                    $contents = $email;
                }

                Storage::put('gc.txt', $contents);
                return;
            }
            
            $customer = $data->customers[0];
            
            if ($customer->email === $email){
                $json = 
                [
                    "gift_card" => [
                    "customer_id" => $customer->id,
                    "initial_value" => "25",
                    "note" => "twoFour Violation",
                    "expires_on" => "2020-01-15",
                    ]
                ];

                $json = json_encode($json);

                $api_url = '';
                $path = 'admin/gift_cards.json';
                $url = $api_url.'/'.$path;
                $gc_resp = json_decode(Curl::post($url, $json));

            }
    }

    public function runClubFix()
    {
        set_time_limit(0);
        $list = [];

        $gc = [
                "Creation Failed" => [],
                "Creation Success" => [],
            ];

        foreach($list as $l){
            $api_url = '';
            $path = 'admin/customers/search.json?query=email:'.$l.'&limit=1';
            $url = $api_url.'/'.$path;
            $data = json_decode(Curl::get($url));

            if(!isset($data->customers[0])){
                $gc["Creation Failed"][] = [
                    "email" => $l
                ];
                
                continue;
            }
            
            $customer = $data->customers[0];

            $bfcmtag = $this->has_tag('', $customer->tags);
            $club_tag = $this->has_tag('', $customer->tags);

            if ($customer->email === $l){
                if ($bfcmtag === true && $club_tag === false) {
                
                    $tags = $customer->tags . ", ";
                    //$json = json_encode($json);

                    

                    $api_url = '';
                    //set params to allow us to update tags
                    $path = 'admin/customers/'.$customer->id.'.json';
                    $url = $api_url.'/'.$path;

                    //set new json array
                    $json_array = [
                        'customer' => [
                            'id' => $customer->id,
                            'tags' => $tags,
                        ],
                    ];

                    //encode it and make it json
                    $json = json_encode($json_array);

                    //push it to shopify
                    $put_resp = json_decode(Curl::put($url, $json));
                }
            }
        }

        echo '<pre>';
        print_r($gc);
        echo '</pre>';
        die;
    }

    public function twoFour_inventory(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunInv($order_data));
            $this->dispatch($job);
            //$this->fireInv($order_data);
            return;
        }
    }

    public function fireInv($order_data)
    {
        if (! isset($order_data->customer)) {
            return;
        }

        $customer = $order_data->customer;
        $line_items = $order_data->line_items;
        $thresholds = '';
        $low_violators = [];
        $high_violators = [];

        $api_url = '';

        foreach ($line_items as $item) {

            //set collect params to get collect id
            $get_col_id = 'admin/collects.json?product_id='.$item->product_id.'&collection_id=';
            $url = $api_url.'/'.$get_col_id;
            $collects = json_decode(Curl::get($url));
            $collect_id = '';

            if(empty($collects->collects)){
                continue;
            }

            //grab collect id.
            foreach ($collects->collects as $collect) {
                $collect_id = $collect->id;
            }

            //if we have a collect id, that means the product is tied to the twoFour collection, which means we can successfully remove it from the collection
            if (! $collect_id) {
                continue;
            }


            $path = 'admin/products/'.$item->product_id.'.json';
            $url = $api_url.'/'.$path;
            $shopify_data = Curl::get($url);
            $decoded_shopify_data = json_decode($shopify_data);

            if (empty($decoded_shopify_data->product)) {
                continue;
            }

            $product = $decoded_shopify_data->product;
            $tags = $product->tags;
            $collect_id = false;

            foreach ($product->variants as $variant) {

                //get size from variant option 1
                $size = strtolower($variant->option1);
                $title = strtolower($variant->title);
                $sku = strtoupper($variant->sku);
                $price = $variant->price;
                if (strpos($title, 'freegifts') && $price === '0.00') {
                    continue;
                }

                $thresholds = self::getThreshold($sku);
                $conv_size = self::sizeConversion($size);

                if (! empty($thresholds) && ! empty($conv_size)) {
                    $conv_size_h = $conv_size.'_high';
                    $conv_size_l = $conv_size.'_low';

                    //use size to get threshold level
                    $this_items_threshold_high = $thresholds[0]->$conv_size_h;
                    $this_items_threshold_low = $thresholds[0]->$conv_size_l;
                } else {
                    $this_items_threshold_high = 75;
                    $this_items_threshold_low = 15;
                }

                //get inventory quantity from variants
                $inventory_quantity = $variant->inventory_quantity;

                //compared to see if threshold is below threshold
                if ($inventory_quantity <= $this_items_threshold_low) {

                    //if we have a collect id, that means the product is tied to the twoFour collection, which means we can successfully remove it from the collection
                    if ($this->has_tag('twoFour', $tags) === true || $this->has_tag('twoFour', $tags) === true) {
                        //strip twoFour from tags
                        $new_tags = str_replace('twoFour, ', '', $tags);
                        $new_tags = str_replace('twoFour, ', '', $new_tags);

                        //set params to allow us to update tags
                        $path = 'admin/products/'.$product->id.'.json';
                        $url = $api_url.'/'.$path;

                        //set new json array
                        $json_array = [
                            'product' => [
                                'id' => $product->id,
                                'tags' => $new_tags,
                            ],
                        ];

                        //encode it and make it json
                        $json = json_encode($json_array);

                        //push it to shopify
                        $put_resp = json_decode(Curl::put($url, $json));

                        //if we get a response back, it was successful, lets fire off an email alert, to alert someone of the offending product(s)
                        if ($put_resp) {
                            $low_violators[] = $variant->sku;
                            $now = Carbon::now();
                            $db_data = DB::table('twoFour_inventory')->where('sku', '=', $variant->sku)->where('threshold', 'low')->first();
                            if (empty($db_data)) {
                                $this->insertInventoryDatabase($variant->sku, $inventory_quantity, $now, 'low');
                            } else {
                                $this->updateInventoryDatabase($variant->sku, $inventory_quantity, $now, 'low', $db_data);
                            }
                        }
                    }
                } elseif ($inventory_quantity > $this_items_threshold_low && $inventory_quantity <= $this_items_threshold_high) {
                    $high_violators[] = $variant->sku;
                    $now = Carbon::now();
                    $db_data = DB::table('twoFour_inventory')->where('sku', '=', $variant->sku)->where('threshold', 'high')->first();

                    if (empty($db_data)) {
                        $this->insertInventoryDatabase($variant->sku, $inventory_quantity, $now, 'high');
                    } else {
                        $this->updateInventoryDatabase($variant->sku, $inventory_quantity, $now, 'high', $db_data);
                    }
                } elseif ($inventory_quantity > $this_items_threshold_low && $inventory_quantity > $this_items_threshold_high) {
                    $db_data_high = DB::table('twoFour_inventory')->where('sku', '=', $variant->sku)->where('threshold', 'high')->first();
                    $db_data_low = DB::table('twoFour_inventory')->where('sku', '=', $variant->sku)->where('threshold', 'low')->first();

                    if (! empty($db_data_high)) {
                        $this->deleteFromInventoryDatabase($db_data_high);
                    }

                    if (! empty($db_data_low)) {
                        $this->deleteFromInventoryDatabase($db_data_low);
                    }
                }
            }
        }

        $database_data = DB::table('twoFour_inventory')->select('*')->orderBy('last_sent', 'desc')->take(1)->get();

        if (! empty($database_data)) {
            if ($database_data[0]->last_sent != '0000-00-00 00:00:00') {
                $now = Carbon::now();
                $then = Carbon::parse(''.$database_data[0]->last_sent.'');
                $difference = $now->diffInHours($then);
            } elseif ($database_data[0]->last_sent == '0000-00-00 00:00:00') {
                $difference = false;
            }
        } else {
            $difference = false;
        }

        if ($difference === false || $difference >= 1) {
            $subject = 'twoFour Threshold Exceeded';
            $now = Carbon::now();

            if (sizeof($high_violators) > 0) {
                $hdata = DB::table('twoFour_inventory')->select('*')->where('threshold', '=', 'high')->orderBy('last_sent', 'desc')->get();
                $high_list_of_products = $this->prettyfy_list($hdata);

                if (sizeof($low_violators) > 0) {
                    $ldata = DB::table('twoFour_inventory')->select('*')->where('threshold', '=', 'low')->orderBy('last_sent', 'desc')->get();
                    $low_list_of_products = $this->prettyfy_list($ldata);

                    foreach ($ldata as $d) {
                        $this->updateInventoryAlertTime($d->id, $now);
                    }

                    $this->send_twoFour_alert($subject, $low_list_of_products, 'low');
                } else {
                    foreach ($hdata as $d) {
                        $this->updateInventoryAlertTime($d->id, $now);
                    }
                    $this->send_twoFour_alert($subject, $high_list_of_products, 'high');
                }
            }
        }
    }

    public function cleanupDatabase()
    {
        $data = DB::table('twoFour_inventory')->get();

        $api_url = '';
        $api_end_point = 'admin/products/count.json';
        $api = $api_url.'/'.$api_end_point;

        $count = json_decode(Curl::get($api));
        $total_pages = ceil($count->count / 250);

        foreach ($data as $d) {

            //self::updateDB($d, $api_url, $total_pages);
            $job = (new RunDBFix($d, $api_url, $total_pages));
            $this->dispatch($job);
        }
    }

    public function updateDB($d, $api_url, $total_pages)
    {
        $sku_found = false;

        for ($i = 1; $i <= $total_pages; $i++) {
            $endpoint = 'admin/products.json?page='.$i.'&limit=250';
            $url = $api_url.'/'.$endpoint;

            $page_data = json_decode(Curl::get($url));

            foreach ($page_data->products as $product) {
                foreach ($product->variants as $variant) {
                    if ($variant->sku == $d->sku) {
                        $sku_found = $variant;
                        break 3;
                    }
                }
            }
        }

        if ($sku_found !== false) {
            $sku = $sku_found->sku;
            $vsize = $sku_found->option1;

            switch ($vsize) {

                case 's':
                case 'm':
                case 'l':
                case 'xl':
                    $size = $vsize;
                    break;
                case '2xl':
                    $size = 'xxl';
                    break;
                case '3xl':
                    $size = 'xxxl';
                    break;

                default:
                    $size = false;

            }

            if ($size !== false) {
                $high_threshold = $size.'_high';
                $low_threshold = $size.'_low';

                $thresholds = DB::table('thresholds')->where('sku', '=', $sku)->limit(1)->get();

                if (! empty($thresholds)) {
                    $high = $thresholds[0]->$high_threshold;
                    $low = $thresholds[0]->$low_threshold;

                    if ($sku_found->inventory_quantity > $high) {
                        DB::table('twoFour_inventory')->where('sku', '=', $sku)->delete();
                    }
                } else {
                    DB::table('twoFour_inventory')->where('sku', '=', $d->sku)->delete();
                }
            } else {
                DB::table('twoFour_inventory')->where('sku', '=', $d->sku)->delete();
            }
        } else {
            DB::table('twoFour_inventory')->where('sku', '=', $d->sku)->delete();
        }
    }

    public function getThreshold($sku)
    {
        $explode_sku = explode('-', $sku);
        $actual_sku = $explode_sku[0];

        $data = DB::table('thresholds')->where('sku', '=', $actual_sku)->limit(1)->get();

        return $data;
    }

    public function sizeConversion($size)
    {
        $conv_size = '';
        switch ($size) {
            case 'xsmall':
                $conv_size = 'xs';
                break;

            case 'small':
                $conv_size = 's';
                break;

	        case 's/m':
	        	$conv_size = 's_m';
	        	break;

            case 'medium':
                $conv_size = 'm';
                break;

            case 'large':
                $conv_size = 'l';
                break;

	        case 'l/xl':
	        	$conv_size = 'l_xl';
	        	break;

            case 'xlarge':
                $conv_size = 'xl';
                break;

            case '2xlarge':
                $conv_size = 'xxl';
                break;

            case '3xlarge':
                $conv_size = 'xxxl';
                break;

	        case '4xlarge':
	        	$conv_size = 'xxxxl';
	        	break;
        }

        return $conv_size;
    }

    public function updateInventoryAlertTime($id, $now)
    {
        DB::table('twoFour_inventory')->where('id', '=', $id)->update([
            'last_sent' => $now->toDateTimeString(),
        ]);
    }

    public function updateInventoryDatabase($sku, $quantity, $now, $threshold, $db_data)
    {
        DB::table('twoFour_inventory')->where('id', '=', $db_data->id)->update([
            'sku' => $sku,
            'threshold' => $threshold,
            'quantity' => $quantity,
            'updated_at' => $now->toDateTimeString(),
        ]);
    }

    public function deleteFromInventoryDatabase($db_data)
    {
        DB::table('twoFour_inventory')->where('id', '=', $db_data->id)->delete();
    }

    public function insertInventoryDatabase($sku, $quantity, $now, $threshold)
    {
        DB::table('twoFour_inventory')->insert([
            'sku' => $sku,
            'threshold' => $threshold,
            'quantity' => $quantity,
            'created_at' => $now->toDateTimeString(),
            'updated_at' => $now->toDateTimeString(),
        ]);
    }

    public function prettyfy_list($violators)
    {
        $products = '';
        foreach ($violators as $v) {
            if ($v) {
                $products .= '<p>'.$v->sku.'</p>';
            }
        }

        return $products;
    }

    public function send_twoFour_alert($subject, $products, $type)
    {

        // send email
        if ($type == 'low') {
            $emails = [];
        } else {
            $emails = [];
        }

        Mail::send('email.twoFour', ['product' => $products, 'type' => $type], function ($m) use ($subject, $emails) {
            $m->from('email@domain.com', '');

            $m->to($emails)->subject($subject);
        });
    }

    public function get_thresholds()
    {
        $thresholds = [
            'xsmall' => ['high' => 75, 'low' => 15],
            'small' => ['high' => 75, 'low' => 15],
            'medium' => ['high' => 75, 'low' => 35],
            'large' => ['high' => 75, 'low' => 40],
            'xlarge' => ['high' => 75, 'low' => 35],
            '2xlarge' => ['high' => 75, 'low' => 25],
            '3xlarge' => ['high' => 75, 'low' => 15],
            'default'  => ['high' => 75, 'low' => 15],
        ];

        return $thresholds;
    }

    public function twoFour_orders(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RuntwoFourOrders($order_data));
            $this->dispatch($job);
            //$this->firetwoFourOrders($order_data);
            return;
        }
    }

    public function walkin(Request $request)
    {
        if ($this->validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            //$job = (new RunWalkins($order_data));
            //$this->dispatch($job);
            $this->fireWalkins($order_data);
            return;
        }
    }

    public function fireWalkins($order_data)
    {
        if ($order_data->location_id === 12345 && $order_data->source_name === "pos") {
            $line_items = $order_data->line_items;
            $id = $order_data->id;
            $skus = [];

            foreach ($line_items as $item) {
                $skus[] = [
                    'order_id' => $id,
                    'product_id' => $item->product_id,
                    'sku' => $item->sku,
                    'product' => $item->title,
                    'size' => $item->variant_title,
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                ];
            }

            $now = Carbon::now();
            $data = DB::table('walkin_customers')->where('order_id', '=', $id)->take(1)->get();

            if (empty($data)) {
                $walkin_db_id = DB::table('walkin_customers')->insertGetId([
                    'order_id' => $id,
                    'status' => 'new',
                    'completed' => 0,
                    'elapsed_time' => 0,
                    'created_at' => $now->toDateTimeString(),
                    'updated_at' => $now->toDateTimeString(),
                ]);

                foreach ($skus as $sku) {
                    DB::table('walkin_customers_order_detail')->insert([
                        'parent_id' => $walkin_db_id,
                        'order_id' => $id,
                        'product_id' => $sku['product_id'],
                        'sku' => $sku['sku'],
                        'product' => $sku['product'],
                        'size' => $sku['size'],
                        'quantity' => $sku['quantity'],
                        'price' => $sku['price'],
                        'created_at' => $now->toDateTimeString(),
                        'updated_at' => $now->toDateTimeString(),
                    ]);
                }
            }
        } else {
            return;
        }
    }

    public function firetwoFourOrders($order_data)
    {
        if (! isset($order_data->customer)) {
            return;
        }

        $api_url = '';
        $customer = $order_data->customer;
        $line_items = $order_data->line_items;
        $twoFour_count = 0;
        $line_item_count = sizeof($line_items);

        foreach ($line_items as $item) {
            $product_id = $item->product_id;
            $path = 'admin/products/'.$product_id.'.json';
            $url = $api_url.'/'.$path;
            $product_data = json_decode(Curl::get($url));

            if (! isset($product_data->product)) {
                continue;
            }

            if ($this->has_tag('twoFour', $product_data->product->tags) === true) {
                $twoFour_count++;
            }
        }

        if ($twoFour_count === $line_item_count) {
            if (isset($order_data->tags) && $order_data->tags != '') {
                $tags = $order_data->tags.', twoFour';
            } else {
                $tags = 'twoFour';
            }

            $json_array = [
                'order' => [
                    'id' => $order_data->id,
                    'tags' => $tags,
                    'note' => 'twoFour',
                ],
            ];
            $json = json_encode($json_array);

            $path = 'admin/orders/'.$order_data->id.'.json';
            $url = $api_url.'/'.$path;

            $result = Curl::put($url, $json);
        }
    }

    public function add_free_tier(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunSGFreeTier($order_data));
            $this->dispatch($job);
            //$this->fireFreeTier($order_data);
            return;
        }
    }

    public function fireFreeTier($order_data)
    {
        $api_url = '';

        if ($order_data->cancel_reason != '') {
            return;
        }

        $shopgate_order = $this->is_shopgate_order($order_data->note_attributes);
        if ($shopgate_order === true) {
            $line_items = $order_data->line_items;
            $subtotal = floatval($order_data->subtotal_price);

            if ($subtotal >= 99.99) {
                $shipping = ['title'=>'Free Shipping', 'price'=>'0'];
            } else {
                $shipping = ['title'=>'Standard Shipping', 'price'=>'5.95'];
            }

            if (! isset($order_data->billing_address)) {
                $billing_address = $order_data->shipping_address;
            } else {
                $billing_address = $order_data->billing_address;
            }

            $free_tier_items = $this->create_free_tier_array($subtotal);

            $new_line_items = array_merge($line_items, $free_tier_items);

            if (isset($order_data->tags) && $order_data->tags != '') {
                $tags = $order_data->tags.', Shopgate Free Tier';
            } else {
                $tags = 'Shopgate Free Tier';
            }

            $order_array['order'] = [
                'line_items' => $new_line_items,
                'customer' => $order_data->customer,
                'billing_address' => $billing_address,
                'shipping_address' => $order_data->shipping_address,
                'email' => $order_data->email,
                'send_receipt' => true,
                'total_weight' => $order_data->total_weight,
                'fullfillment_status' => $order_data->fulfillment_status,
                'tags' => $tags,
                'total_tax' => $order_data->total_tax,
                'total_price' => $order_data->total_price,
                'shipping_lines' => [$shipping],
                'source_name' =>'gs-api',
                'note_attributes' => ['name'=>'Original Order ID', 'value' => $order_data->order_number],
            ];

            $json = json_encode($order_array);

            $path = 'admin/orders.json';
            $url = $api_url.'/'.$path;
            $pdata2 = json_decode(Curl::post($url, $json));

            $rjson['order'] = [
                'amount' => false,
                'restock' => true,
                'reason' => 'Other',
                'email' => false,
            ];
            $rjson = json_encode($rjson);
            $path = 'admin/orders/'.$order_data->id.'/cancel.json';
            $url = $api_url.'/'.$path;
            $product_data = json_decode(Curl::post($url, $rjson));

            $subject = 'Order Update: #'.$order_data->order_number.'';
            $emails = $order_data->email;
            $template = 'email.shopgatetier';
            $from = '';
            $from_name = '';
            $this->send_alert($subject, $emails, $template, $from, $from_name);
        }
    }

    public function create_free_tier_array($subtotal)
    {
        $free_tier_items = [];
        if ($subtotal > 0 && $subtotal <= 49.99) {
            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];
        } elseif ($subtotal >= 50.00 && $subtotal <= 99.99) {
            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];
        } elseif ($subtotal >= 150.00 && $subtotal <= 249.99) {
            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];
        } elseif ($subtotal >= 250.00 && $subtotal <= 499.99) {
            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];
        } elseif ($subtotal >= 500.00) {
            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];

            $free_tier_items[] = [
                'variant_id' => 12345,
                'quantity' => 1,
                'tax_lines' => [],
                'price' => 0.00,
            ];
        }

        return $free_tier_items;
    }

    public function is_shopgate_order($data)
    {
        foreach ($data as $d) {
            if ($d->name === 'Shopgate Order Number') {
                return true;
            }
        }

        return false;
    }

    public function gsl_orders(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunLOrders($order_data));
            $this->dispatch($job);
            //$this->fireLOrders($order_data);
            return;
        }
    }

    public function fireLOrders($order_data)
    {
        $api_url = '';
        $line_items = $order_data->line_items;
        foreach ($line_items as $item) {
            $product_id = $item->product_id;
            $path = 'admin/products/'.$product_id.'.json';
            $url = $api_url.'/'.$path;
            $product_data = json_decode(Curl::get($url));

            if (! isset($product_data->product)) {
                continue;
            }

            if ($this->has_tag('L', $product_data->product->tags) === true) {
                $app = DB::table('gsl')->where('product_id', '=', $product_data->product->id)->where('campaign_active', '=', '1')->take(1)->get();
                if (! empty($app)) {
                    $current_sold = $app[0]->items_sold;
                    $new = $current_sold + $item->quantity;

                    DB::table('gsl')->where('product_id', '=', $product_data->product->id)->where('campaign_active', '=', '1')->update([
                        'items_sold' => $new,
                    ]);
                }
            }
        }
    }

    public function split_orders(Request $request)
    {
        if (self::validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $order_data = json_decode($data);

            $job = (new RunSplitOrder($order_data));
            $this->dispatch($job);
            //$this->fireSplit($order_data);
            return;
        }
    }

    public function fireSplit($order_data)
    {
        if (! isset($order_data->customer)) {
            return;
        }

        $customer = $order_data->customer;
        $line_items = $order_data->line_items;
        $api_url = '';

        if ($order_data->tags == 'SplitOrder' || $order_data->cancel_reason != '') {
            return;
        }

        $line_item_count = sizeof($line_items);
        $item_array = [];

        $does_shipping_item_exist = $this->search_for_shipping_item(23456, $order_data);

        if (false === $does_shipping_item_exist) {
            $does_shipping_item_exist = $this->search_for_shipping_item(23456, $order_data);
        }

        if (true === $does_shipping_item_exist) {
            $items_that_need_split = json_decode(json_encode($this->split_order($order_data, $api_url)));

            if (empty($items_that_need_split->not_ready)) {
                return;
            }

            $order_tax_not_ready = $this->calculate_order_tax($items_that_need_split->not_ready);
            $order_tax_ready = $this->calculate_order_tax($items_that_need_split->ready);
            $order_price_not_ready = $this->calculate_order_price($items_that_need_split->not_ready);
            $order_price_ready = $this->calculate_order_price($items_that_need_split->ready);

            if (! isset($order_data->billing_address)) {
                $billing_address = $order_data->shipping_address;
            } else {
                $billing_address = $order_data->billing_address;
            }

            $order_array['order'] = [
                'line_items' => $items_that_need_split->not_ready,
                'customer' => $order_data->customer,
                'billing_address' => $billing_address,
                'shipping_address' => $order_data->shipping_address,
                'email' => $order_data->email,
                'send_receipt' => true,
                'fullfillment_status' => $order_data->fulfillment_status,
                'note' => 'Split Order - Original ID: '.$order_data->order_number.'',
                'tags' => 'SplitOrder',
                'total_tax' => $order_tax_not_ready,
                'total_price' => $order_price_not_ready,
                'source_name' =>'gsapi',
            ];

            $json = json_encode($order_array);
            $path = 'admin/orders.json';
            $url = $api_url.'/'.$path;
            $pdata1 = json_decode(Curl::post($url, $json));

            $order_array['order'] = [
                'line_items' => $items_that_need_split->ready,
                'customer' => $order_data->customer,
                'billing_address' => $order_data->billing_address,
                'shipping_address' => $order_data->shipping_address,
                'email' => $order_data->email,
                'send_receipt' => true,
                'fullfillment_status' => $order_data->fulfillment_status,
                'note' => 'Split Order - Original ID: '.$order_data->order_number.'',
                'tags' => 'SplitOrder',
                'total_tax' => $order_tax_ready,
                'total_price' => $order_price_ready,
                'source_name' =>'gsapi',
            ];

            $json = json_encode($order_array);

            $path = 'admin/orders.json';
            $url = $api_url.'/'.$path;
            $pdata2 = json_decode(Curl::post($url, $json));

            $rjson['order'] = [
                'amount' => false,
                'restock' => true,
                'reason' => 'Other',
                'email' => false,
            ];
            $rjson = json_encode($rjson);
            $path = 'admin/orders/'.$order_data->id.'/cancel.json';
            $url = $api_url.'/'.$path;
            $product_data = json_decode(Curl::post($url, $rjson));

            $subject = 'Order Update: #'.$order_data->order_number.'';
            $emails = $order_data->email;
            $template = 'email.splitorder';
            $from = '';
            $from_name = ' Customer Service';
            $this->send_alert($subject, $emails, $template, $from, $from_name);
        }
    }

    public function search_for_shipping_item($needle, $order)
    {
        foreach ($order->line_items as $item) {
            $product_id = $item->product_id;

            if ($product_id === $needle) {
                return true;
            }
        }

        return false;
    }

    public function calculate_order_tax($order)
    {
        $price = 0.00;
        foreach ($order as $item) {
            foreach ($item->tax_lines as $tax) {
                $price += $tax->price;
            }
        }

        return $price;
    }

    public function calculate_order_price($order)
    {
        $price = 0.00;
        foreach ($order as $item) {
            $price += $item->price;
        }

        return $price;
    }

    public function split_order($order, $api_url)
    {
        $tags = [
                    'ready' => [],
                    'not_ready' => [],
                ];

        foreach ($order->line_items as $item) {
            $product_id = $item->product_id;
            $variant_id = $item->variant_id;
            $quantity = $item->quantity;
            $tax_lines = $item->tax_lines;
            $price = $item->price;

            $path = 'admin/products/'.$product_id.'.json';
            $url = $api_url.'/'.$path;
            $product_data = json_decode(Curl::get($url));

            foreach ($product_data->product as $k => $v) {
                if ($k == 'tags') {
                    $tag_array = explode(', ', $v);

                    $tag_result_preorder = $this->searchForTag('preorder', $tag_array);
                    $tag_result_gsl = $this->searchForTag('gsl', $tag_array);

                    if ($tag_result_gsl === true || $tag_result_preorder === true) {
                        $result = $this->isSplitItemAccountedFor($variant_id, $tags['ready']);
                        $result2 = $this->isSplitItemAccountedFor($variant_id, $tags['not_ready']);
                        if ($result !== true && $result2 !== true) {
                            $tags['not_ready'][] = [
                                        'variant_id' => 12345,
                                        'quantity' => $quantity,
                                        'tax_lines' => $tax_lines,
                                        'price' => $price,
                                    ];
                        }
                    } else {
                        if ($tag_result_gsl !== true && $tag_result_preorder !== true) {
                            $result = $this->isSplitItemAccountedFor($variant_id, $tags['ready']);
                            $result2 = $this->isSplitItemAccountedFor($variant_id, $tags['not_ready']);
                            if ($result == false && $result2 == false) {
                                $tags['ready'][] = [
                                            'variant_id' => 12345,
                                            'quantity' => $quantity,
                                            'tax_lines' => $tax_lines,
                                            'price' => $price,
                                        ];
                            }
                        }
                    }
                }
            }
        }

        return $tags;
    }

    public function searchForTag($needle, $array)
    {
        if (! empty($array)) {
            foreach ($array as $a) {
                if (strtolower($a) == strtolower($needle)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isSplitItemAccountedFor($needle, $array)
    {
        if (! empty($array)) {
            foreach ($array as $a) {
                if ($a['variant_id'] == $needle) {
                    return true;
                }
            }
        }

        return false;
    }

    public function has_tag($needle, $haystack)
    {
        $haystack = explode(', ', $haystack);
        foreach ($haystack as $stack) {
            if (strtolower($needle) == strtolower($stack)) {
                return true;
            }
        }

        return false;
    }

    public function has_club_item($needle, $haystack)
    {
        foreach ($haystack as $stack) {
            foreach ($needle as $n) {
                if ($n == $stack->product_id) {
                    return true;
                }
            }
        }

        return false;
    }

    public function send_alert($subject, $emails, $template, $from, $from_name)
    {
        Mail::send($template, [''], function ($m) use ($subject, $emails, $from, $from_name) {
            $m->from($from, $from_name);

            $m->to($emails)->subject($subject);
        });
    }

    public function cancelFraudById()
    {
        $ids = [];
        $api_url = '';

        foreach($ids as $id){
        $array = [
            'reason' => 'fraud',
            'restock' => 'true'
        ];

        $json = json_encode($array);

        $path = '/admin/orders/'.$id.'/cancel.json';
        $url = $api_url.'/'.$path;
        json_decode(Curl::post($url, $json));
        }
    }
}
