<?php

namespace App\Http\Controllers\AlphaOutpost;

use DB;
use Input;
use Session;
use Redirect;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * AO Controller.
 *
 * all methods for viewing, editing, importing, Alpha Outpost views, etc
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.2
 */
class AOController extends Controller
{
    /**
     * Display a listing of all AO Codes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = $database_data = DB::table('ao_codes')->paginate(10);
        $search = '';

        foreach ($rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
            switch ($row->code_used) {
                case 'Y':
                    $row->code_used = 'YES';
                    break;
                default:
                    $row->code_used = 'NO';
            }
        }

        return view('ao.ao')->with(compact('rows', 'search'));
    }

    /**
     * search functionality to find a specific AO code in the table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $input = $request->all();
        $rows = $database_data = DB::table('ao_codes')->where('code', '=', $input['term'])->paginate(10);

        foreach ($rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
            switch ($row->code_used) {
                case 'Y':
                    $row->code_used = 'YES';
                    break;
                default:
                    $row->code_used = 'NO';
            }
        }

        $search = $input['term'];

        return view('ao.ao')->with(compact('rows', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * displays code if exists, 600 error if no code exists.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $database_data = DB::table('ao_codes')->where('code', '=', $id)->take(1)->get();
        if (empty($database_data)) {
            return 600;
        } else {
            return $database_data;
        }
    }

    /**
     * updates AO code to display it was used.
     *
     * @param  int  $id
     * @return int
     */
    public function edit($id)
    {
        $now = Carbon::now();
        DB::table('ao_codes')->where('code', '=', $id)->update([
            'code_used' => 'Y',
            'updated_at' => $now->toDateTimeString(),
        ]);

        return 1;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * reactivates previously used AO Code.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivate($id)
    {
        $now = Carbon::now();
        DB::table('ao_codes')->where('code', '=', $id)->update([
            'code_used' => 'N',
            'updated_at' => $now->toDateTimeString(),
            'updated_by' => \Auth::user()->id,
        ]);

        return Redirect::route('ao.home');
    }

    /**
     * displays AO codes import form.
     *
     * @return \Illuminate\Http\Response
     */
    public function importcodes()
    {
        return view('ao.import');
    }

    /**
     * handles the import of AO Codes via CSV.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadcodes(Request $request)
    {
        $file = ['file' => Request::file('csv')];
        $rules = ['file' => 'required'];
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
           return \Redirect::route('ao.importcodes')->withInput()->withErrors($validator);
        } else {
            $file = ['extension' => strtolower(Request::file('csv')->getClientOriginalExtension())];
            $rules = ['extension' => 'required|in:csv'];
            $validator = Validator::make($file, $rules);

            if ($validator->fails()) {
                // send back to the page with the input data and errors
                return \Redirect::route('ao.importcodes')->withInput()->withErrors($validator);
            } else {
                $path = storage_path().'/app/';
                $fileName = \Uuid::generate(4).'.txt'; // renameing image
                  Request::file('csv')->move($path, $fileName); // uploading file to given path
                  // sending back with message

                  $result = $this->processfile($fileName);
                if ($result === true) {
                    Session::flash('status', 'Imported successfully');
                } else {
                    Session::flash('statuserror', "One or more of the code's imported were found to exist in the database.  However, all new codes were imported.");
                }
            }
        }

        return Redirect::route('ao.importcodes');
    }

    /**
     * handles the actual importing of codes from CSV to database.
     *
     * @param  string $fileName
     * @return bool
     */
    public function processfile($fileName)
    {
        $path = storage_path('app/'.$fileName.'');
        $result = '';

        if (($handle = fopen($path, 'r')) !== false) {
            $now = Carbon::now();
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                foreach ($data as $d) {
                    if ($d == 0) {
                        continue;
                    }
                    $database_data = DB::table('ao_codes')->where('code', '=', $d)->take(1)->get();

                    if (empty($database_data)) {
                        DB::table('ao_codes')->insert([
                            'code' => $d,
                            'created_at' => $now->toDateTimeString(),
                            'updated_at' => $now->toDateTimeString(),
                        ]);
                    } else {
                        $error = 'Code Exists: '.$d.'';
                        \Log::error($error);
                        $result .= $error.' ';
                    }
                }
            }
            fclose($handle);
            if ($result == '') {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * generates AO codes, based on quantity passed in and saves to local disk a CSV.
     *
     * @param  int $number
     * @return \Illuminate\Http\Response\Download
     */
    public function generate_codes($number)
    {
        $length = 50;
        $code_list = [];

        for ($i = 1; $i <= $number; $i++) {
            $randnum = rand(1111111111, 1999999999);
            $num_exists = $this->check_code_at_db($randnum);
            while ($num_exists === true) {
                $randnum = rand(1111111111, 1999999999);
                $num_exists = $this->check_code_at_db($randnum);
            }

            $now = Carbon::now();
            DB::table('ao_codes')->insert([
                'code' => $randnum,
                'created_at' => $now->toDateTimeString(),
                'updated_at' => $now->toDateTimeString(),
            ]);

            $code_list['code'][] = $randnum;
        }

        $download_data = $this->download_csv($code_list);

        return \Response::download($download_data['filename'], 'ao_generated_code.csv', $download_data['headers']);
    }

    /**
     * downloads the CSV of generated codes.
     *
     * @param  string $code_list
     * @return csv file download
     */
    public function download_csv($code_list)
    {
        $filename = storage_path().'/ao_generated_code.csv';
        $handle = fopen($filename, 'w+');

        foreach ($code_list['code'] as $row) {
            fputcsv($handle, [$row]);
        }

        fclose($handle);

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment',
        ];

        return ['filename' => $filename, 'headers' => $headers];
    }

    /**
     * function to check if code exists for generated codes.
     *
     * @param  int $randnum
     * @return bool
     */
    public function check_code_at_db($randnum)
    {
        $database_data = DB::table('ao_codes')->where('code', '=', $randnum)->take(1)->get();
        if (empty($database_data)) {
            return false;
        } else {
            return true;
        }
    }
}
