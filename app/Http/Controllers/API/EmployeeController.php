<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Services\Curl;
use Auth;
use App\AccessList;


class EmployeeController extends Controller
{
    //
    public function __construct()
    {
       
    }

    public function index()
    {
        
    }

    public function requestAccess(Request $request)
    {
        $user = Auth::user();
        if(!$user){
            $request->session()->flash('statuserror', 'You must be logged in and validated to request access for an employee!');
            return redirect()->to('/');
        }
        
        if($user->hasRole('admin') || $user->hasRole('management')){
            $list = AccessList::all();
            return view('employee.requestaccess')->with('list', $list);
        } else {
            $request->session()->flash('statuserror', 'You must be an admin or a department manager to request access for an employee!');
            return redirect()->to('/');
        }
    }

    public function submitAccess()
    {

    }

}
