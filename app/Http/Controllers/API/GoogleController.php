<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Request;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Drive;
use Illuminate\Support\Facades\Storage;

class GoogleController extends Controller
{
    private function prompt()
    {
        echo "<script> var authCode = prompt('Enter the access code'); </script>";
        $authCode = "<script> document.write(authCode); </script>";

        print($authCode);
    }

    private function getClient($type)
    {
        $client = new Google_Client();
        $client->setApprovalPrompt('force');
        
        switch($type) {
            case "sheets":
                $client->setApplicationName('Product Matrix');
                $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
                break;
            case "drive":
                $client->setApplicationName('Product Images');
                $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
                break;
        }

        $uris = explode(",", ''));

        $credsArr = [
            'client_id' => ''),
            'project_id' => ''),
            'auth_uri' => ''),
            'token_uri' => ''),
            'auth_provider_x509_cert_url' => ''),
            'client_secret' => ''),
            'redirect_uris' => [$uris[0], $uris[1]]
        ];

        $creds = array(
            "installed" => $credsArr
        );

        $client->setAuthConfig($creds);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        switch($type) {
            case "sheets":
                $credentialsPath = public_path() . "/sheets-token.json";             
                break;
            case "drive":
                $credentialsPath = public_path() . "/drive-token.json";
                break;
        }

        // $credentialsPath = 'token.json';
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Let's hope this never happens

            print("Problem with token.");
            exit;
            
            // $authUrl = $client->createAuthUrl();
            // echo "<script> window.open('".$authUrl."') </script>";
            
            // // Sometime this works, may need troubleshooting
            // $authCode = $this->prompt();
            
            // switch($type) {
            //     case "sheets":
            //         $accessToken = $client->fetchAccessTokenWithAuthCode("");
            //         break;
            //     case "drive":
            //         $accessToken = $client->fetchAccessTokenWithAuthCode("");
            //         break;
            // }

            // // Check to see if there was an error.
            // if (array_key_exists('error', $accessToken)) {
            //     throw new \Exception(join(', ', $accessToken));
            // }

            // // Store the credentials to disk.
            // if (!file_exists(dirname($credentialsPath))) {
            //     mkdir(dirname($credentialsPath), 0700, true);
            // }
            // file_put_contents($credentialsPath, json_encode($accessToken));
            // printf("Credentials saved to %s\n", $credentialsPath);
            
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    public function getData()
    {
        $client = $this->getClient("sheets");
        $service = new \Google_Service_Sheets($client);

        $spreadsheetId = '';
        $range = 'A3:N2599';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->values;

        $count = 0;

        foreach($values as $arr) {
            if(!isset($arr[12])) {
                unset($values[$count]);
            }
            else if($arr[12] != 'GRN') {
                unset($values[$count]);
            }
            $count++;
        }

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );    

        $images = self::getImageUrls();

        $ret = [];

        foreach($values as $row) {
            $ret[$row[0]] = array(
                'sku' => (string) $row[0],
                'name' => (string) $row[1],
                'gender' => (string) $row[2],
                'brand' => (string) $row[3],
                'category' => (string) $row[4],
                'retailPrice' => (string) $row[5],
                'inventoryUnits' => (string) $row[6],
                'inventoryRetailValue' => (string) $row[7],
                'mtdSalesUnits' => (string) $row[8],
                'mtdSales' => (string) $row[9],
                'ytdSalesUnits' => (string) $row[10],
                'ytdSales' => (string) $row[11],
                'productAge' => (string) $row[13],
            );

            foreach($images as $image) {
                if(explode(".", $image)[0] == $row[0]) {
                    $ret[$row[0]]['image'] = (string) "" . $image;
                }
            }

            if(!isset($ret[$row[0]]['image'])) {
                $ret[$row[0]]['image'] = null;
            }
        }

        return json_encode($ret);
    }

    public static function getImageURLs()
    {

        $images = scandir(public_path() . '/drive_images');
        $count = 0;
        foreach($images as $image) {
            if(strpos($image, "") !== 0) {
                unset($images[$count]);
            }
            $count++;
        }
        
        return $images;
    }

    public function testView() 
    {
        return view('test');
    }
}

?>