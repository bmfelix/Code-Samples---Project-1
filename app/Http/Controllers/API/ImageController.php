<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class ImageController extends Controller
{
    //
    public function show(Request $request) {
        $image = $request['requestUri'];
        return request()->file(asset($image));
    }
}
