<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Services\Curl;



class FedexController extends Controller
{
    //
    public function __construct()
    {
       
    }

    public function index()
    {
        
    }

    

    public function authenticate()
    {
        $url = "https://api-sandbox.supplychain.fedex.com/api/sandbox/fsc/oauth2/token";

        $postfields = [
            "grant_type" => "password",
            "client_id" => ''),
            "client_secret"=> ''),
            "username" => ''),
            "password" => ''),
            "scope" => "Fulfillment_Returns",
        ];

        $headers = [
            "accept:application/json",
            "content-type:application/x-www-form-urlencoded",
            "org_name:",
            "x-org-name:",
        ];
        

        $postfields = http_build_query($postfields);

        $auth = json_decode(Curl::post($url, $postfields, $headers));
        
        setcookie("fedex_access_token", $auth->access_token, time()+3600);
        setcookie("fedex_refresh_token", $auth->refresh_token, time()+$auth->refresh_token_expires_in);


    }
}
