<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use phpish\shopify;
use App\Services\Curl;
use Illuminate\Http\Request;
use DB;
use Log;
use App\Http\Controllers\Controller;
use Mail;

class ShopifyController extends Controller
{
    public function storeOrders($day = null)
    {
        $shopify = shopify\client('', '', '', true);
        if( ! isset($day) ) {
            $now = Carbon::now()->format('Y-m-d');
        } else {
            $now = $day;
        }

        try {
            $totalproducts = $shopify('GET /admin/orders/count.json', ['status'=>'any','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00']);
            $limit = 250;
            $totalpage = ceil($totalproducts / $limit);

          //  for ($i = 1; $i <= $totalpage; $i++) {
           //     $orders = $shopify('GET /admin/orders.json', ['status'=>'open', 'status'=>'closed','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00','limit'=>'250', 'page'=>$i]);
          //      foreach ($orders as $order) {

                    $database_data = DB::table('shopify_orders_by_day')->where([
                        ['date', '=', $now],
                    ])->get();

                    

                    if (empty($database_data)) {
                        DB::table('shopify_orders_by_day')->insert([
                            'num_of_orders' => $totalproducts,
                            'date' => $now,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString(),
                        ]);
                        echo "Record Created";
                        return;
                    } else {
                        DB::table('shopify_orders_by_day')->where('id','=', (int)$database_data{0}->id)->update([
                            'num_of_orders' => $totalproducts,
                            'updated_at' => Carbon::now()->toDateTimeString(),
                        ]);
                        echo "Record Updated";
                        return;
                    }
               // }
           // }
        } catch (shopify\ApiException $e) {
            Log::error($e->getMessage());

            return;
        }
    }

    public function getData($table, $day = null)
    {
        if( ! isset($day) ) {
            $now = Carbon::now()->format('Y-m-d');
        } else {
            $now = $day;
        }

        if($table == "shopify_units_purchased" || $table == "WeekLongData"){
            $qty_field = "units";
        } else {
            $qty_field = "num_of_orders";
        }

        $database_data = DB::table($table)->select($qty_field, 'date')->orderBy('date','desc')->get();

        return json_encode(array_reverse($database_data));
    }

    public function storeOrdersOpen($day = null)
    {
        $shopify = shopify\client('', '', '', true);
        if( ! isset($day) ) {
            $now = Carbon::now()->format('Y-m-d');
        } else {
            $now = $day;
        }

        try {
            $totalproducts = $shopify('GET /admin/orders/count.json', ['status'=>'open','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00']);

            $database_data = DB::table('shopify_orders_by_day_unshipped')->where([
                ['date', '=', $now],
            ])->get();
            

            if (empty($database_data)) {
                DB::table('shopify_orders_by_day_unshipped')->insert([
                    'num_of_orders' => $totalproducts,
                    'date' => $now,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Created";
                return;
            } else {
                DB::table('shopify_orders_by_day_unshipped')->where('id','=', (int)$database_data{0}->id)->update([
                    'num_of_orders' => $totalproducts,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Updated";
                return;
            }

        } catch (shopify\ApiException $e) {
            Log::error($e->getMessage());

            return;
        }
    }

    public function storeOrdersClosed($day = null)
    {
        
        $shopify = shopify\client('', '', '', true);
        if( ! isset($day) ) {
            $now = Carbon::now()->format('Y-m-d');
        } else {
            $now = $day;
        }

        try {
            $totalproducts = $shopify('GET /admin/orders/count.json', ['status'=>'closed','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00']);

            $database_data = DB::table('shopify_orders_by_day_shipped')->where([
                ['date', '=', $now],
            ])->get();

            

            if (empty($database_data)) {
                DB::table('shopify_orders_by_day_shipped')->insert([
                    'num_of_orders' => $totalproducts,
                    'date' => $now,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Created";
                return;
            } else {
                DB::table('shopify_orders_by_day_shipped')->where('id','=', (int)$database_data{0}->id)->update([
                    'num_of_orders' => $totalproducts,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Updated";
                return;
            }

        } catch (shopify\ApiException $e) {
            Log::error($e->getMessage());

            return;
        }
    }

    public function storeQuantityByDay($day = null)
    {
        $shopify = shopify\client('', '', '', true);

        if( ! isset($day) ) {
            $now = Carbon::now()->format('Y-m-d');
        } else {
            $now = $day;
        }

        try {
            $totalproducts = $shopify('GET /admin/orders/count.json', ['status'=>'any','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00']);
            $limit = 250;
            $totalpage = ceil($totalproducts / $limit);
            $product_total = 0;

            for ($i = 1; $i <= $totalpage; $i++) {
                $orders = $shopify('GET /admin/orders.json', ['status'=>'any','created_at_min'=>$now.'T00:00:00-05:00', 'created_at_max'=>$now.'T23:59:59-05:00','limit'=>'250', 'page'=>$i]);
                foreach ($orders as $order) {
                    foreach($order['line_items'] as $item){
                        $product_total = $product_total + $item['quantity'];
                    }
                
                }
            }

            $database_data = DB::table('shopify_units_purchased')->where([
                ['date', '=', $now],
            ])->get();

            

            if (empty($database_data)) {
                DB::table('shopify_units_purchased')->insert([
                    'units' => $product_total,
                    'date' => $now,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Created";
                return;
            } else {
                DB::table('shopify_units_purchased')->where('id','=', (int)$database_data{0}->id)->update([
                    'units' => $product_total,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]);
                echo "Record Updated";
                return;
            }


        } catch (shopify\ApiException $e) {
            Log::error($e->getMessage());

            return;
        }
    }

    private function getWeekCount($sow, $eow){
        $total = 0;
        $database_data = DB::table('shopify_units_purchased')->where('date','>=',$sow)->where('date','<=',$eow)->orderBy('date', 'asc')->get();
        foreach($database_data as $dd){
            $total += $dd->units;
        }

        $database_data = DB::table('WeekLongData')->where([
            ['date', '=', $sow],
        ])->get();

        if (empty($database_data)) {
            DB::table('WeekLongData')->insert([
                'units' => $total,
                'date' => $sow,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            echo "Record Created";
            return;
        } else {
            DB::table('WeekLongData')->where('id','=', (int)$database_data{0}->id)->update([
                'units' => $total,
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            echo "Record Updated";
            return;
        }
    }


    public function countWeeks($week = null){
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        if( ! isset($week) ){
            $start_on = Carbon::now()->startOfWeek();
            $end_on = Carbon::now()->endOfWeek();
        } else {
            $start_on = Carbon::parse($week)->startOfWeek();
            $end_on = Carbon::parse($week)->endOfWeek();
        }

        $week_count = $this->getWeekCount($start_on, $end_on);
        
    }
}
