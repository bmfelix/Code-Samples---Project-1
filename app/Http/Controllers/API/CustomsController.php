<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Services\Curl;
use Cartalyst\Stripe\Stripe;
use Mail;



class CustomsController extends Controller
{
    //
    public function __construct()
    {
       
    }

    public function index($order_id = null)
    {
        return view('customs.pay');
    }
    

    public function authenticate()
    {
        $headers = ['Content-Type: application/xml', 'QUICKBASE-ACTION: API_Authenticate'];
        $url = "";
        $data = array(
            "username" => ''),
            "password" => ''),
            "hours" => 24,
        );
        $xml = new \SimpleXMLElement('<qdbapi/>');
        $this->to_xml($xml, $data);
        $auth = Curl::post($url, $xml->asXML(), $headers);
        $returned_data = simplexml_load_string($auth);

        return (string)$returned_data->ticket;
    }

    public function searchByOrderNumber($order_id = null, $return = "json")
    {
        $query = "({3.EX.'".$order_id."'} OR {26.EX.'".$order_id."'})";
        $ticket = $this->authenticate();
        $headers = ['Content-Type: application/xml', 'QUICKBASE-ACTION: API_DoQuery'];
        $url = "" . '');
        $data = array(
            "ticket" => $ticket,
            "apptoken" => ''),
            "query" => $query,
            "includeRids" => 1,
        );
        $xml = new \SimpleXMLElement('<qdbapi/>');
        $this->to_xml($xml, $data);

        $auth = Curl::post($url, $xml->asXML(), $headers);
        $returned_data = simplexml_load_string($auth);

        $rid = (int)$returned_data->record->attributes()->rid;
        $status = (string)$returned_data->record->payment;
        $due = (float)$returned_data->record->total_order;
        $email = (string)$returned_data->record->order_created_by;
        $project_name = (string)$returned_data->record->project_name;

        $order_details = [
            "status" => $status,
            "amount_due" => $due,
            "rid" => $rid,
            "email" => $email,
            "project" =>$project_name,
        ];

        if($return === "json"){
            return json_encode($order_details);
        } else {
            return $order_details;
        }
    }

    public function payNow(Request $request)
    {
        $stripe = Stripe::make(''));
        
        try{

            $token = $stripe->tokens()->create([
                'card' => [
                'number' => $request->get('cc_number'),
                'exp_month' => $request->get('cc_expire_month'),
                'exp_year' => $request->get('cc_expire_year'),
                'cvc' => $request->get('cc_cvv'),
                ]
            ]);


            if (!isset($token['id'])) {
                echo "Token Not Created";
                die;
            }

            $details = $this->searchByOrderNumber($request->get('order_number'), false);


            $charge = $stripe->charges()->create([
                'card' => $token['id'],
                'currency' => 'USD',
                'amount' => $details['amount_due'],
                'description' => 'Order # '. $request->get('order_number') .'',
                'receipt_email' => $request->get('email')
            ]);


            if($charge['paid'] === true){

                // $ticket = $this->authenticate();
                // $headers = ['Content-Type: application/xml', 'QUICKBASE-ACTION: API_EditRecord'];
                // $url = "" . '');
                // $data = array(
                //     "ticket" => $ticket,
                //     "apptoken" => ''),
                //     "rid" => $details['rid'],
                //     "field" => [
                //         [
                //             "@attributes" => [
                //                 "name" => "payment"
                //             ],
                //             'Paid',
                //         ],
                //         [
                //             "@attributes" => [
                //                 "fid" => 15
                //             ],
                //             $charge['id'],
                //         ]
                //     ],

                // );

                // $xml = new \SimpleXMLElement('<qdbapi/>');
                // $xml->addChild('ticket', $ticket);
                // $xml->addChild('apptoken', ''));
                // $xml->addChild('rid', $details['rid']);
                // $f1 = $xml->addChild('field', 'Paid');
                // $f1->addAttribute('name', 'payment');
                // $f2 = $xml->addChild('field', 'Stripe Payment ID: ' . $charge['id']);
                // $f2 ->addAttribute('fid',15);

                // $auth = Curl::post($url, $xml->asXML(), $headers);
                //$returned_data = simplexml_load_string($auth);

                $this->sendEmail($details, $charge['id'], $request->get('order_number'));
                echo "true";
                die;

            } else {
                
                echo $charge['outcome']['reason'];
                die;
                
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
            echo $e->getMessage();
            die;
        } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            echo $e->getMessage();
            die;
        }
        die;
    }

    private function sendEmail($details, $payment_id, $order_no)
    {
        
    }

    private function to_xml(\SimpleXMLElement $object, array $data)
    {   
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $new_object = $object->addChild($key);
                to_xml($new_object, $value);
            } else {
                // if the key is an integer, it needs text with it to actually work.
                if ($key == (int) $key) {
                    $key = "$key";
                }

                $object->addChild($key, $value);
            }   
        }   
    }


    
}
