<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\GiveawayEntry;
use App\Http\Requests\GiveawayRequest;
use Validator;
use DrewM\Drip\Drip;
use DrewM\Drip\Dataset;
use Carbon\Carbon;
use App\Services\Shopify;
use Illuminate\Support\Facades\Mail;

class GiveawayController extends Controller
{
    public function winner(Request $request) {

        $startDate = Carbon::create($request->start_year, $request->start_month, $request->start_day, 0, 0, 0, 'America/Chicago')->timezone('UTC');

        $endDate = Carbon::create($request->end_year, $request->end_month, $request->end_day, 23, 59, 59, 'America/Chicago')->timezone('UTC');
        
        $entries = GiveawayEntry::whereBetween("created_at", [ $startDate, $endDate ])->get()->all();
        $prevWinner = GiveawayEntry::whereBetween("created_at", [ $startDate, $endDate ])
                        ->where('winner', '=', 1) 
                        ->get()->all();

        if(!empty($entries) && empty($prevWinner)) {
            $winningNumber = random_int(0, count($entries)-1);
            $winner = $entries[$winningNumber];

            GiveawayEntry::where('id', '=', $winner->id)->update(['winner' => 1]);
            
            $winner = GiveawayEntry::where('id', $winner->id)->get()->first();

            $this->sendWinnerEmail($winner->first_name, $winner->email);

            $this->sendWinnerInfoEmail($winner, $endDate->toFormattedDateString());

            return redirect()->action('API\GiveawayController@index');
        } else {
            return redirect()->back()->with('msg', 'already a winner for that time period');
        }
    }

    public function sendWinnerEmail($name, $email)
  	{
        
    }

    public function sendWinnerInfoEmail($winner, $date)
    {
        
    }

    public function index() 
    {
        $data['entries'] = GiveawayEntry::count();
        $data['winners'] = GiveawayEntry::where('winner', '=', 1)->get()->all();
        return view('giveaway.index', $data);
    }

    public function selectWinner()
    {
        return view('giveaway.winnerselect');
    }

    public function cancelWinner(int $id)
    {
        $winner = GiveawayEntry::where('id', $id)->update(['winner' => 0]);

        return redirect()->back();

    }

    public function create(Request $request) 
    {
        $excludedStates = ['Florida', 'New York', 'Rhode Island'];
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'last_name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'address1' => 'required',
            'city' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'zip' => 'required|regex:/^[0-9-]+$/u',
            'country' => 'in:United States',
        ],
        [
            'required' => 'required',
            'in' => 'We\'re sorry, this giveaway is only available in the United States',
            'entry.required_without_all' => 'Sorry, only one entry per day',
            'regex' => 'Invalid characters',
        ]
        );

        $validator->sometimes('entry', 'required_without_all:giveaway', function($input){
            return GiveawayEntry::alreadyEnteredToday($input->email);
        });
        
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $id = Shopify::getOrCreateShopifyAccount($request->all());

        Shopify::addCustomerTag($id, 'Vanson Giveaway');

        if (in_array($request->state, $excludedStates)) {

            return response()->json([
                'errors' => [
                    'state' => [
                        'Unfortunately, this giveaway is not available in your state.',
                        ]
                    ]
                ], 400);
        }

        GiveawayEntry::create($request->all());

        return response()->json(['success' => 'Awesome! You\'re entered in the giveaway!']);
    }
    
}
