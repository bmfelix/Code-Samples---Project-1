<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Facebook\Facebook;
use SammyK\FacebookQueryBuilder\FQB;

class SocialController extends Controller
{
    public function getComments($chunkSize = 100, $chunk=0)
    {
        $result['facebook'] = $this->getFacebookComments($chunkSize, $chunk);
        $result['instagram'] = $this->getInstagramComments($chunkSize, $chunk);

        return $result;
    }

    public function getInstagramComments($chunkSize=100, $chunk=0) 
    {
        $fb = new Facebook([
            'app_id' => '',
            'app_secret' => '',
            'default_graph_version' => 'v3.0',
            'default_access_token' => '',
        ]);

        $fqb = new FQB;
        
        $commentsEdge = $fqb->edge('comments')->fields(['timestamp', 'text'])->limit(50);
        $mediaEdge = $fqb->edge('media')->fields($commentsEdge)->limit(20);

        $request = $fqb->node('')->fields($mediaEdge);
        
        try {
            $response = $fb->get($request->asEndpoint());
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo $e->getMessage();
            exit;
        }

        $result = [];

        foreach ($response->getDecodedBody()['media']['data'] as $comment) {
            if (isset($comment['comments'])) {
                foreach ($comment['comments']['data'] as $comment){
                    $tmp['timestamp'] = $comment['timestamp'];
                    $tmp['comment'] = $comment['text'];
                    array_push($result, $tmp);
                }
            }
        }

        $timestamp = array_column($result, 'timestamp');
        $comment = array_column($result, 'comment');
        
        array_multisort($timestamp, SORT_DESC, $comment, SORT_ASC, $result);

        $result = array_chunk($result, $chunkSize, true);

        return array_column($result[$chunk], 'comment');
    }

    public function getFacebookComments($chunkSize=100, $chunk=0)
    {
        $fb = new Facebook([
            'app_id' => '',
            'app_secret' => '',
            'default_graph_version' => 'v3.0',
            'default_access_token' => '',
        ]);

        $fqb = new FQB;

        $commentsEdge = $fqb->edge('comments')->fields(['created_time','message'])->limit(50);
        $postsEdge = $fqb->edge('posts')->fields([$commentsEdge])->limit(20);

        $request = $fqb->node('')->fields([$postsEdge]);



        try {
            $response = $fb->get($request->asEndpoint());
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo $e->getMessage();
            exit;
        }

        $result = [];

        foreach ($response->getDecodedBody()['posts']['data'] as $post) {
            if (isset($post['comments'])){
                foreach ($post['comments']['data'] as $comment) {
                    $tmp['timestamp'] = $comment['created_time'];
                    $tmp['comment'] = $comment['message'];
                    array_push($result, $tmp);
                }
            }
        } 
        $timestamp = array_column($result, 'timestamp');
        $comment = array_column($result, 'comment');
        
        array_multisort($timestamp, SORT_DESC, $comment, SORT_ASC, $result);

        $result = array_chunk($result, $chunkSize, true);

        return array_column($result[$chunk],'comment');
    }

}