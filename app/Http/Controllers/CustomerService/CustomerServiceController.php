<?php

namespace App\Http\Controllers\CustomerService;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = $database_data = DB::table('giftcards')->paginate(20);
        $search = '';

        foreach ($rows as $row) {
            if ($row->card_status == 1) {
                $row->card_status = 'Unused';
            } elseif ($row->card_status == 2) {
                $row->card_status = 'Used';
            }
        }

        return view('customerservice.giftcards')->with(compact('rows', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * search functionality to find a specific AO code in the table.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $input = $request->all();

        $rows = DB::table('giftcards')
            ->where('mail_to_email', 'LIKE', '%'.$input['term'].'%')
            ->orWhere('card_code', 'LIKE', '%'.$input['term'].'%')
            ->orWhere('mail_to', 'LIKE', '%'.$input['term'].'%')
            ->paginate(20);

        foreach ($rows as $row) {
            if ($row->card_status == 1) {
                $row->card_status = 'Unused';
            } elseif ($row->card_status == 2) {
                $row->card_status = 'Used';
            }
        }

        $search = $input['term'];

        return view('customerservice.giftcards')->with(compact('rows', 'search'));
    }
}
