<?php

namespace App\Http\Controllers\L;

use Log;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    public function create()
    {
        return view('pages.currentcampaigns');
    }

    public function destroy($id)
    {
        Product::deleteCampaign($id);
        session()->flash('status', 'Campaign Deleted.');

        return redirect('campaign/list');
    }

    public function newcampaign()
    {
        return view('campaign.createcampaign');
    }

    public function campaign($id)
    {
        if (! isset($id) && ! is_numeric($id)) {
            return false;
        }

        $getSalesGoal = Product::getSalesGoalById($id);
        $getSoldItems = Product::getSoldItemsById($id);

        if (empty($getSoldItems)) {
            $getSoldItems = 0;
        }

        $app = Product::getActiveGslApp($id);

        foreach ($app as $a) {
            $a->end_date = Carbon::createFromFormat('Y-m-d H:i:s', $a->end_date, 'America/Chicago');
        }

        if (!empty($app[0])) {
            $remaining = $app[0]->min_quantity - $getSoldItems;

            if ($getSoldItems >= 36) {
                $payout = $getSoldItems * $app[0]->payout;
                $full_payout = money_format('$%i', $payout);
            } else {
                $full_payout = money_format('$%i', 0.0);
            }

            return view('campaign.countdown', compact('getSalesGoal', 'getSoldItems', 'app', 'full_payout', 'remaining'));
        }

        return "Invalid/Inactive ID";
    }

    public function admin()
    {
        return view('pages.admin');
    }

    public function campaign_list()
    {
        Product::check_for_expired();

        $active_rows = Product::listActiveL();

        foreach ($active_rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
        }

        $inactive_rows = Product::listInactiveL();

        foreach ($inactive_rows as $row) {
            $row->created_at = Carbon::parse(''.$row->created_at.'')->setTimezone('America/Chicago');
            $row->updated_at = Carbon::parse(''.$row->updated_at.'')->setTimezone('America/Chicago');
        }

        return view('campaign.list', ['active_rows' => $active_rows, 'inactive_rows' => $inactive_rows]);
    }

    public function campaign_edit($id)
    {
        $app = Product::getGslApp($id);
        foreach ($app as $a) {
            $a->end_date = Carbon::parse(''.$a->end_date.'');
        }

        return view('campaign.edit', ['app' => $app]);
    }

    public function campaignupdate(Request $request, $id)
    {
        $messages = [
        'product_id.required' => 'You must enter a valid product id.',
        'product_id.numeric' => 'Product ID can only be numerical.',
        'end_date.required' => 'You must select your end date.',
        'end_date.date' => 'Must be in a proper date format. ',
        'sales_goal.required' => 'You must enter your sales goal.',
        'sales_goal.numeric' => 'Sales goal must be a numerical value.',
        'min_quantity.required' => 'You must enter a minimum quantity.',
        'min_quantity.numeric' => 'Minimum Quanity must be a numerical value.',
        'payout.required' => 'You must enter a payout.',
        'payout.numeric' => 'The payout must be a numerical value.',
        'payoout_title.required' => 'You must enter a payout title.',
      ];

        $rules = [
        'product_id' => 'required|numeric',
        'end_date' => 'required|date',
        'sales_goal' => 'required|numeric',
        'min_quantity' => 'required|numeric',
        'payout' => 'required|numeric',
      ];

        $payout_display_rules = $this->payout_display_rules();

        if (! empty($payout_display_rules)) {
            $rules = array_merge($rules, $payout_display_rules);
        }

        $this->validate($request, $rules, $messages);

        $productId = $request['product_id'];
        if (Product::isL($productId) === true) {
            if ($this->update_form($id, $request) === true) {
                session()->flash('status', 'Campaign Submitted');
            } else {
                session()->flash('statuserror', 'Looks like you have some errors in your application.');
            }
        } else {
            session()->flash('statuserror', 'Not a valid Shopify product id, or not a valid L product.');
            return redirect()->route('ops.update', [$id]);
        }

        return redirect('campaign/list');
    }

    public function campaignsubmit(Request $request)
    {
        $messages = [
        'product_id.required' => 'You must enter a valid product id.',
        'product_id.numeric' => 'Product ID can only be numerical.',
        'product_id.unique' => 'That Product ID already has a campaign created.',
        'end_date.required' => 'You must select your end date.',
        'end_date.date' => 'Must be in a proper date format. ',
        'sales_goal.required' => 'You must enter your sales goal.',
        'sales_goal.numeric' => 'Sales goal must be a numerical value.',
        'min_quantity.required' => 'You must enter a minimum quantity.',
        'min_quantity.numeric' => 'Minimum Quanity must be a numerical value.',
        'payout.required' => 'You must enter a payout.',
        'payout.numeric' => 'The payout must be a numerical value.',
        'payoout_title.required' => 'You must enter a payout title.',
      ];

        $rules = [
        'product_id' => 'required|numeric|unique:gsl',
        'end_date' => 'required|date',
        'sales_goal' => 'required|numeric',
        'min_quantity' => 'required|numeric',
        'payout' => 'required|numeric',
      ];

        $payout_display_rules = $this->payout_display_rules();

        if (! empty($payout_display_rules)) {
            $rules = array_merge($rules, $payout_display_rules);
        }

        $this->validate($request, $rules, $messages);

        $id = $request['product_id'];
        if (Product::isL($id) === true) {
            if ($this->process_form($request) === true) {
                session()->flash('status', 'Campaign Submitted.');
            } else {
                session()->flash('statuserror', 'Looks like you have some errors in your application.');
            }
        } else {
            session()->flash('statuserror', 'Not a valid Shopify product id, or not a valid L product.');
            return redirect('campaign/new');
        }

        return redirect('campaign/list');
    }

    public function payout_display_rules()
    {
        $payout_display_rules = '';
        if (isset($_POST['display_payout']) && $_POST['display_payout'] == 1) {
            $payout_display_rules = [
                'payout_title' => 'required',
            ];
        }
        return $payout_display_rules;
    }

    public function update_form($id, $post)
    {
        Product::updateGslCampaign($id, $post);
        return true;
    }

    public function process_form($post)
    {
        $database_data = Product::getGslProductId($post['product_id']);

        if (empty($database_data)) {
            Product::insertGslCampaign($post);
        } else {
            $error = 'Campaign Exists: ' . $post['product_id'] . '';
            Log::error($error);
            return false;
        }

        return true;
    }

    public function activate_campaign($id)
    {
        $rows = Product::getGslApp($id);
        foreach ($rows as $row) {
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $row->end_date, 'America/Chicago');
            $now = Carbon::now()->setTimezone('America/Chicago');

            if ($end_date >= $now) {
                Product::activateL($id);
                session()->flash('status', 'Campaign Activated.');
            } else {
                session()->flash('statuserror', "Campaign cannot be activated, as it's end date has expired.");
            }
        }

        return redirect('campaign/list');
    }

    public function deactivate_campaign($id)
    {
        Product::deactivateL($id);
        session()->flash('status', 'Campaign Deactivated.');

        return redirect('campaign/list');
    }

    public function ajax_data($id)
    {
        Product::check_for_expired();
        $data = Product::ajax_data($id);
        echo json_encode($data);
        exit;
    }
}
