<?php

namespace App\Http\Controllers\Accounts;

use DB;
use Session;
use Redirect;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

/**
 * Retailer Controller.
 *
 * handles creation, editing and deletion of gs retailer accounts
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.0
 */
class RetailerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($new = false)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('retailer_app')->where('id', '=', $id)->delete();
        return view('shopify.retailapps');
    }

    public function removeApplication($id) {
        DB::table('retailer_app')->where('id', '=', $id)->update([
            'approved' => 2
        ]);
        return redirect('retailer/app');
    }

    /**
     * displays the gs retailer application form.
     *
     * @return \Illuminate\Http\Response
     */
    public function retail()
    {
        return view('shopify.retail');
    }

    /**
     * retailer form submission validation and response.
     *
     * @return \Illuminate\Http\Response
     */
    public function retail_app()
    {
        $messsages = [
            'name.required' => 'Name is required.',
            'name.regex' => 'Name can only contain letters or numbers',
            'title.required' => 'Title is required.',
            'title.regex' => 'Title can only contain letters or numbers',
            'taxid.required' => 'Federal Tax ID is required',
            'taxid.numeric' => 'Federal Tax ID can only be numbers',
            'cname.required' => 'Company Name is required.',
            'cname.regex' => 'Company Name can only contain letters, numbers or an apostrophe',
            'webaddress.regex' => 'Company Web Address must be a valid URL (http://www.example.com)',
            'phone.required' => 'Phone Number is required',
            'phone.regex' => 'Phone Number should be formated like XXX-XXX-XXXX',
            'email.required' => 'Email address is required.',
            'email.email' => 'Email address should be in an email address format',
            'caddress.required' => 'Registered Company Address is required.',
            'caddress.regex' => 'Registered Company Address can only contain letters or numbers',
            'city.required' => 'City is required.',
            'city.regex' => 'City can only contain letters or numbers',
            'state.required' => 'You must select a State.',
            'zip.required' => '5 digit Zip Code is required.',
            'zip.numeric' => 'Zip Code can only contain numbers.',
            'zip.digits' => 'Zip Code can only be 5 digits in length.',
            'businesstype.required' => 'You must select "Sole Proprietorship", "Partnership", "Corporation" or "Other".',
            'pbaddress.required' => 'Primary Business Address is required.',
            'pbaddress.regex' => 'Primary Business Address can only contain letters or numbers',
            'pbcity.required' => 'Primary Business City is required.',
            'pbcity.regex' => 'Primary Business City can only contain letters or numbers',
            'pbstate.required' => 'You must select a State for the Primary Business State.',
            'pbzip.required' => '5 digit Primary Business Zip Code is required.',
            'pbzip.numeric' => 'Primary Business Zip Code can only contain numbers.',
            'pbzip.digits' => 'Primary Business Zip Code can only be 5 digits in length.',
            'pbphone.required' => 'Primary Business Phone Number is required',
            'pbphone.regex' => 'Primary Business Phone Number should be formated like XXX-XXX-XXXX',
            'pbemail.required' => 'Primary Business Email address is required.',
            'pbemail.email' => 'Primary Business Email address should be in an email address format',
            'lengthataddress.required' => 'Length at Primary Business Address is required.',
            'lengthataddress.regex' => 'Length at Primary Business Address can only contain letters or numbers',
            'bankname.required' => 'Bank Name is required.',
            'bankname.regex' => 'Bank Name can only contain letters, numbers or an apostrophe',
            'bankaddress.required' => 'Bank Address is required.',
            'bankaddress.regex' => 'Bank Address can only contain letters or numbers',
            'bankcity.required' => 'Bank City is required.',
            'bankcity.regex' => 'Bank City can only contain letters or numbers',
            'bankstate.required' => 'You must select a State for the Bank.',
            'bankzip.required' => '5 digit Bank Zip Code is required.',
            'bankzip.numeric' => 'Bank Zip Code can only contain numbers.',
            'bankzip.digits' => 'Bank Zip Code can only be 5 digits in length.',
            'bankphone.required' => 'Bank Phone Number is required',
            'bankphone.regex' => 'Bank Phone Number should be formated like XXX-XXX-XXXX',
            'accounttype.required' => 'You must select "Savings" or "Checking".',
            'refname1.required' => 'First Business Reference Name is required.',
            'refname1.regex' => 'First Business Reference Name can only contain letters, numbers or an apostrophe',
            'refaddress1.required' => 'First Business Reference Address is required.',
            'refaddress1.regex' => 'First Business Reference Address can only contain letters or numbers',
            'refcity1.required' => 'First Business Reference City is required.',
            'refcity1.regex' => 'First Business Reference City can only contain letters or numbers',
            'refstate1.required' => 'You must select a State for the First Business Reference.',
            'refzip1.required' => '5 digit First Business Reference Zip Code is required.',
            'refzip1.numeric' => 'First Business Reference Zip Code can only contain numbers.',
            'refzip1.digits' => 'First Business Reference Zip Code can only be 5 digits in length.',
            'refphone1.required' => 'First Business Reference Phone Number is required',
            'refphone1.regex' => 'First Business Reference Phone Number should be formated like XXX-XXX-XXXX',
            'refemail1.required' => 'First Business Reference Email address is required.',
            'refemail1.email' => 'First Business Reference Email address should be in an email address format',
            'refname2.required' => 'Second Business Reference Name is required.',
            'refname2.regex' => 'Second Business Reference Name can only contain letters, numbers or an apostrophe',
            'refaddress2.required' => 'Second Business Reference Address is required.',
            'refaddress2.regex' => 'Second Business Reference Address can only contain letters or numbers',
            'refcity2.required' => 'Second Business Reference City is required.',
            'refcity2.regex' => 'Second Business Reference City can only contain letters or numbers',
            'refstate2.required' => 'You must select a State for the Second Business Reference.',
            'refzip2.required' => '5 digit Second Business Reference Zip Code is required.',
            'refzip2.numeric' => 'Second Business Reference Zip Code can only contain numbers.',
            'refzip2.digits' => 'Second Business Reference Zip Code can only be 5 digits in length.',
            'refphone2.required' => 'Second Business Reference Phone Number is required',
            'refphone2.regex' => 'Second Business Reference Phone Number should be formated like XXX-XXX-XXXX',
            'refemail2.required' => 'Second Business Reference Email address is required.',
            'refemail2.email' => 'Second Business Reference Email address should be in an email address format',
            'refname3.regex' => 'Third Business Reference Name can only contain letters, numbers or an apostrophe',
            'refaddress3.regex' => 'Third Business Reference Address can only contain letters or numbers',
            'refcity3.regex' => 'Third Business Reference City can only contain letters or numbers',
            'refzip3.numeric' => 'Third Business Reference Zip Code can only contain numbers.',
            'refzip3.digits' => 'Third Business Reference Zip Code can only be 5 digits in length.',
            'refphone3.regex' => 'Third Business Reference Phone Number should be formated like XXX-XXX-XXXX',
            'refemail3.email' => 'Third Business Reference Email address should be in an email address format',
            'signaturetitle.required' => 'Signature Title is required.',
            'signaturetitle.regex' => 'Signature Title can only contain letters or numbers',
            'signatureinitials.required' => 'Your Initials are required.',
            'signatureinitials.alpha' => 'Your Initials can only contain letters.',
            'signatureinitials.max' => 'Your Initials can only be 2 letters in length.',
        ];

        $rules = [
          'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u', // alpha - only contain letters
          'title' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'taxid' => 'required|numeric',
          'cname' => 'required|regex:/^[(\'a-zA-Z0-9\s)]+$/u',
          'webaddress' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
          'phone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'email' => 'required|email|unique:retailer_app',
          'caddress' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'city' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'state' => 'required',
          'zip' => 'required|numeric|digits:5',
          'bcommenced' =>'date',
          'businesstype' => 'required',
          'pbaddress' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'pbcity' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'pbstate' => 'required',
          'pbzip' => 'required|numeric|digits:5',
          'pbphone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'pbemail' => 'required|email',
          'lengthataddress' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'bankname' => 'required|regex:/^[(\'a-zA-Z0-9\s)]+$/u',
          'bankaddress' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'bankcity' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'bankstate' => 'required',
          'bankzip' => 'required|numeric|digits:5',
          'bankphone' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'accounttype' => 'required',
          'refname1' => 'required|regex:/^[(\'a-zA-Z0-9\s)]+$/u',
          'refaddress1' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refcity1' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refstate1' => 'required',
          'refzip1' => 'required|numeric|digits:5',
          'refphone1' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'refemail1' => 'required|email',
          'refname2' => 'required|regex:/^[(\'a-zA-Z0-9\s)]+$/u',
          'refaddress2' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refcity2' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refstate2' => 'required',
          'refzip2' => 'required|numeric|digits:5',
          'refphone2' => 'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'refemail2' => 'required|email',
          'refname3' => 'regex:/^[(\'a-zA-Z0-9\s)]+$/u',
          'refaddress3' => 'regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refcity3' => 'regex:/^[(a-zA-Z0-9\s)]+$/u',
          'refzip3' => 'numeric|digits:5',
          'refphone3' => 'regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',
          'refemail3' => 'email',
          'signaturetitle' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
          'signatureinitials' => 'required|alpha|max:2',
        ];

        $validation = Validator::make($_POST, $rules, $messsages);

        if ($validation->passes()) {

            //process the form data for insertion
            $result = $this->process_form($_POST);

            if ($result === true) {
                Session::flash('status', 'Application Submitted.  :)');
                $this->sendEmail($_POST);
            } else {
                Session::flash('statuserror', 'Looks like you have previously submitted an application.');
            }
        } else {
            return redirect('retailer/app')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
        }

        return redirect('retailer/app');
    }

	public function sendEmail($post)
	{
		
	}

    /**
     * insert form data into database.
     *
     * @param  \Illuminate\Http\Request  $post
     * @return bool
     */
    public function process_form($post)
    {
        $result = true;

        foreach ($post as $key => $value) {
            $$key = $value;
        }

        $database_data = DB::table('retailer_app')->where('cname', '=', $cname)->take(1)->get();

        if (empty($database_data)) {
            $now = Carbon::now();
            DB::table('retailer_app')->insert([
                'name' => $name,
                'title' => $title,
                'taxid' => $taxid,
                'cname' => $cname,
                'webaddress' => $webaddress,
                'phone' => $phone,
                'email' => $email,
                'caddress' => $caddress,
                'city' => $city,
                'state' => $state,
                'zip' => $zip,
                'bcommenced' => $bcommenced,
                'businesstype' => $businesstype,
                'pbaddress' => $pbaddress,
                'pbcity' => $pbcity,
                'pbstate' => $pbstate,
                'pbzip' => $pbzip,
                'pbphone' => $pbphone,
                'pbemail' => $pbemail,
                'lengthataddress' => $lengthataddress,
                'bankname' => $bankname,
                'bankaddress' => $bankaddress,
                'bankcity' => $bankcity,
                'bankstate' => $bankstate,
                'bankzip' => $bankzip,
                'bankphone' => $bankphone,
                'accounttype' => $accounttype,
                'refname1' => $refname1,
                'refaddress1' => $refaddress1,
                'refcity1' => $refcity1,
                'refstate1' => $refstate1,
                'refzip1' => $refzip1,
                'refphone1' => $refphone1,
                'refemail1' => $refemail1,
                'refname2' => $refname2,
                'refaddress2' => $refaddress2,
                'refcity2' => $refcity2,
                'refstate2' => $refstate2,
                'refzip2' => $refzip2,
                'refphone2' => $refphone2,
                'refemail2' => $refemail2,
                'refname3' => $refname3,
                'refaddress3' => $refaddress3,
                'refcity3' => $refcity3,
                'refstate3' => $refstate3,
                'refzip3' => $refzip3,
                'refphone3' => $refphone3,
                'refemail3' => $refemail3,
                'signaturetitle' => $signaturetitle,
                'signatureinitials' => $signatureinitials,
                'approved' => 0,
                'created_at' => $now->toDateTimeString(),
                'updated_at' => $now->toDateTimeString(),
            ]);
        } else {
            $error = 'Account Exists: '.$cname.'';
            \Log::error($error);
            $result = false;
        }

        return $result;
    }

    /**
     * display all pending applications in a table for approval.
     *
     * @return \Illuminate\Http\Response
     */
    public function retail_approve()
    {
        $apps = DB::table('retailer_app')->where('approved', '<=', 1)->orderBy('created_at', 'DESC')->get();
        foreach ($apps as $app) {
            $app->created_at = Carbon::parse(''.$app->created_at.'')->setTimezone('America/Chicago');
        }

        return view('shopify.retailapps', compact('apps', $apps));
    }

    /**
     * display specific customer application in form view.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function retail_application($id)
    {
        $app = DB::table('retailer_app')->where('id', '=', $id)->take(1)->get();
        foreach ($app as $a) {
            $a->created_at = Carbon::parse(''.$a->created_at.'')->setTimezone('America/Chicago');
            $a->bcommenced = Carbon::parse(''.$a->bcommenced.'')->setTimezone('America/Chicago');
        }

        return view('shopify.retailapp', compact('app', $app));
    }

    /**
     * approval process of the appication, submits data to shopify to create account.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function retail_application_approve($id)
    {

        //shopify private app url
        $api_url = '';
        $app = DB::table('retailer_app')->where('id', '=', $id)->take(1)->get();

        foreach ($app[0] as $k => $v) {
            $$k = $v;
        }

        $name_array = explode(' ', $name);

        if (sizeof($name_array) < 2) {
            $name_array[1] = $name_array[0];
        }

        //build customer array
        $customer['customer'] = [
            'first_name' => $name_array[0],
            'last_name' => $name_array[1],
            'email' => $email,
            'verified_email' => true,
            'addresses' => [
                'default_address' => [
                    'address1' => $caddress,
                    'city' => $city,
                    'province' => $state,
                    'phone' => $phone,
                    'zip' => "$zip",
                    'last_name' => $name_array[1],
                    'first_name' => $name_array[0],
                    'country' => 'US',
                ],
            ],
            'send_email_invite' => true,
            'tags' => 'Wholesale',
        ];

        //convert to JSON for data transport
        $json = json_encode($customer);
        $path = 'admin/customers.json';
        $url = $api_url.'/'.$path;

        //send data, process response
        $pdata1 = json_decode($this->post_data($url, $json));

        //no errors, and received a customer id from shopify, then proceed
        if (empty($pdata1->errors) && ! empty($pdata1->customer->id)) {
            $now = Carbon::now();
            DB::table('retailer_app')->where('id', '=', $id)->update([
                'approved' => 1,
                'updated_at' => $now->toDateTimeString(),
            ]);
            Session::flash('status', 'Customer Created in Shopify and Activation Email was sent.');
        } else {
            //there were errors... ABORT!
            $returned_errors = '';
            foreach ($pdata1->errors as $key => $value) {
                foreach ($value as $v) {
                    $returned_errors .= $key.' '.$v."\n";
                }
            }
            Session::flash('statuserror', $returned_errors);
        }

        return redirect('retailer/apps');
    }

    /**
     * pass information via curl.
     *
     * @param  string $url
     * @param  string  $json
     * @return string
     */
    public function post_data($url, $json)
    {
        usleep(500000);
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return $resp;
    }
}
