<?php

namespace App\Http\Controllers\Accounts;

use DB;
use Hash;
use Mail;
use App\Role;
use App\User;
use Redirect;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Accounts Controller.
 *
 * handles creating, updating and deleting of users
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.5
 */
class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Auth::user()->id;
        $user = User::findOrFail($id);
        $user->updated_at = Carbon::parse(''.$user->updated_at.'')->setTimezone('America/Chicago');

        return view('pages.account', compact('user', $user));
    }

    /**
     * Show the form for creating a new user and handle validation upon submission.
     *
     * @param bool $new
     * @return \Illuminate\Http\Response
     */
    public function create($new = false)
    {
        if (false === $new) {
            if (\Auth::user()->hasRole('admin')) {
                $roles = DB::table('roles')->lists('display_name', 'id');
            }

            return view('auth.register', compact('roles', $roles));
        } else {
            $rules = [
              'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u', // alpha - only contain letters
              'email' => 'required|email|unique:users',
              'roles' => 'required|not_in:0',
            ];

            $validation = Validator::make($_POST, $rules);
            $rand = rand(6, 12);
            $random_password = $this->random($rand);

            if ($validation->passes()) {
                foreach ($_POST as $key => $value) {
                    if ($key != 'roles') {
                        $$key = $value;
                    }
                }

                $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt($random_password),
                ]);

                foreach ($_POST['roles'] as $role) {
                    $this_role = DB::table('roles')->where('display_name', '=', $role)->first();
                    $dbrole = Role::findOrFail($this_role->id);
                    $user->attachRole($dbrole);
                }

               

                return view('pages.accountcreated');
            } else {
                return Redirect::route('account.create')
                    ->withInput()
                    ->withErrors($validation)
                    ->with('message', 'There were validation errors.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        foreach ($user->roles as $k => $role) {
            $index = $k + 1;
            $user_roles[$index] = strtolower($role->name);
        }

        $roles = DB::table('roles')->lists('display_name', 'id');

        foreach ($roles as $key => $value) {
            $arr[$key] = in_array(strtolower(str_replace(' ', '', $value)), $user_roles);
        }

        return view('auth.edit', compact('user', 'roles', 'user_roles', 'arr'));
    }

    /**
     * Update the specified user by user id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();

        if ($input['password']) {
            $rules = [
              'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u', // alpha - only contain letters
              'password'   => 'required|min:6|confirmed|regex:/^.*(?=.{6,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[@.!$#%]).*$/',
              'roles' => 'required|not_in:0',
            ];
        } else {
            $rules = [
              'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u', // alpha - only contain letters
              'roles' => 'required|not_in:0',
            ];
        }

        $validation = Validator::make($input, $rules);
        $missing_perms = [];

        if ($validation->passes()) {
            $user = User::find($id);

            foreach ($input['roles'] as $v) {
                $role = strtolower(str_replace(' ', '', $v));
                $role_count = count($input['roles']);
                $addOrRemove = $this->addingOrRemovingRoles($id, $input['roles'], $role);

                $roleToRemove = $this->removeWhichRole($id, $input['roles'], $role);

                if ($addOrRemove == 'same' || $addOrRemove == 'exists') {
                } elseif ($addOrRemove == 'adding') {
                    $role_id = DB::table('roles')->where('name', $role)->first();
                    $therole = Role::findOrFail($role_id->id);
                    $user->attachRole($therole);
                } elseif ($addOrRemove == 'removing') {
                    foreach ($roleToRemove as $r) {
                        $user->detachUserRoleById($r, $id);
                    }
                }
            }

            if ($input['password']) {
                $input['password'] = Hash::make($input['password']);
            }

            $user->update($input);

            return Redirect::route('admin.accounts');
        }

        return Redirect::route('account.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function addingOrRemovingRoles($id, $data, $role)
    {
        $user_roles = count($this->getUserRoles($id));
        $proposed_roles = count($data);
        $userHasRole = $this->doesUserHaveRole($role, $id);

        if ($proposed_roles > $user_roles && $userHasRole == false) {
            return 'adding';
        } elseif ($proposed_roles < $user_roles && $userHasRole == true) {
            return 'removing';
        } elseif ($proposed_roles == $user_roles && $userHasRole == true) {
            return 'same';
        } elseif ($proposed_roles > $user_roles && $userHasRole == true) {
            return 'exists';
        }
    }

    public function removeWhichRole($id, $data, $role)
    {
        $user_roles = $this->getUserRoles($id);
        $proposed_roles = $data;
        $remove_roles = [];

        foreach ($user_roles as $r) {
            $role_id = DB::table('roles')->where('name', $role)->first();

            if ($role_id->id != $r->role_id) {
                $remove_roles[] = $r->role_id;
            }
        }

        return $remove_roles;
    }

    public function doesUserHaveRole($role, $id)
    {
        $user_roles = $this->getUserRolesById($id, $role);

        if (count($user_roles) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserRoles($id)
    {
        $user_roles = DB::table('role_user')->where('user_id', $id)->get();

        return $user_roles;
    }

    public function getUserRolesById($id, $role)
    {
        $role_id = DB::table('roles')->where('name', $role)->first();
        $user_roles = DB::table('role_user')->where('user_id', $id)->where('role_id', $role_id->id)->get();

        return $user_roles;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->destroy($id);

        return Redirect::route('admin.accounts');
    }

    /**
     * deletes role based on id passed in.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function roledestroy($id)
    {
        $role = Role::find($id);
        $role->destroy($id);

        return Redirect::route('admin.roles');
    }

    /**
     * generates random password for new users, based on passed in length of password to be generated.
     *
     * @param int $length
     * @return string
     */
    public function random($length)
    {
        $random_string = '';
        $valid_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@.!$#%';

        $num_valid_chars = strlen($valid_chars);

        for ($i = 0; $i < $length; $i++) {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick - 1];
            $random_string .= $random_char;
        }

        return $random_string;
    }

    /**
     * Show the form for creating a new role. also validates the form upon submission and creates role in database.
     *
     * @param bool $new
     * @return \Illuminate\Http\Response
     */
    public function rolecreate($new = false)
    {
        if (false === $new) {
            $new = true;

            return view('admin.createrole', compact('new', $new));
        } else {
            $rules = [
              'name' => 'required|unique:roles|regex:/^[(a-zA-Z\s)]+$/u', // alpha - only contain letters
              'display_name' => 'required|regex:/^[(a-zA-Z\s)]+$/u', // alpha - only contain letters
              'description' => 'required',
            ];

            $validation = Validator::make($_POST, $rules);

            if ($validation->passes()) {
                foreach ($_POST as $key => $value) {
                    $$key = $value;
                }

                $role = Role::create([
                    'name' => strtolower($name),
                    'display_name' => $display_name,
                    'description' => $description,
                ]);

                return view('pages.rolecreated');
            } else {
                return Redirect::route('role.create')
                    ->withInput()
                    ->withErrors($validation)
                    ->with('message', 'There were validation errors.');
            }
        }
    }

    /**
     * updates user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userupdate(Request $request, $id)
    {
        //
        $input = $request->all();

        $rules = [
          'password'   => 'required|min:6|confirmed|regex:/^.*(?=.{6,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[@.!$#%]).*$/',
        ];

        $validation = Validator::make($input, $rules);
        $missing_perms = [];

        if ($validation->passes()) {
            $user = User::find($id);

            if ($input['password']) {
                $input['password'] = Hash::make($input['password']);
            }

            $user->update($input);

            \Session::flash('message', 'Password Updated!');
            \Session::flash('alert-class', 'alert-success');

            return Redirect::route('user.edit', $id);
        }

        return Redirect::route('user.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }
}
