<?php

namespace App\Http\Controllers\Exports;

use App\Services\Curl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ExportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getNumberOf($api, $count_orders = false)
    {
        if ($count_orders === false) {
            $api_count = $api->base.'/'.$api->count;
            $num_of = json_decode(Curl::get($api_count));
        } elseif ($count_orders == 'partial') {
            $api_count = $api->base.'/'.$api->partialcount;
            $num_of = json_decode(Curl::get($api_count));
        } elseif ($count_orders == 'unshipped') {
            $api_count = $api->base.'/'.$api->unshippedcount;
            $num_of = json_decode(Curl::get($api_count));
        } elseif ($count_orders == 'shipped') {
            $api_count = $api->base.'/'.$api->shippedcount;
            $num_of = json_decode(Curl::get($api_count));
        }

        return $num_of->count;
    }

    protected function totalPagesOfData($num_of)
    {
        $limit = 250;
        $total_pages = ceil($num_of / $limit);

        return $total_pages;
    }

    protected function getApiEndPoints($type, $start = "", $stop = "")
    {
        $api = new \stdClass();

        if($start == ""){
            $start = Carbon::now()->setTimezone('America/Chicago')->format('Y-m-d');
        }

        if($stop == ""){
            $stop = Carbon::now()->setTimezone('America/Chicago')->format('Y-m-d');
        }

        switch ($type) {
            case 'products':
                $api->base = '';
                $api->end_point = 'admin/products.json?order='.rawurlencode('created_at DESC').'&limit=250';
                $api->count = 'admin/products/count.json';
                break;
            case 'customers':
                $api->base = '';
                $api->end_point = 'admin/customers.json?order='.rawurlencode('created_at DESC').'&limit=250';
                $api->count = 'admin/customers/count.json';
                break;
            case 'orders':
                $api->base = '';
                $api->end_point = 'admin/orders.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=any';
                $api->count = 'admin/orders.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=any';
                $api->partial = 'admin/orders.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=partial';
                $api->unshipped = 'admin/orders.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=unshipped';
                $api->shipped = 'admin/orders.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=shipped';
                $api->partialcount = 'admin/orders/count.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=partial';
                $api->unshippedcount = 'admin/orders/count.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=unshipped';
                $api->shippedcount = 'admin/orders/count.json?created_at_min='.rawurlencode(''.$start.' 00:00:00 CST -06:00').'&created_at_max='.rawurlencode(''.$stop.' 23:59:59 CST -06:00').'&limit=250&fulfillment_status=shipped';
                break;
            case 'retailproducts':
                $api->base = '';
                $api->end_point = 'admin/products.json?order='.rawurlencode('created_at DESC').'&limit=250';
                $api->count = 'admin/products/count.json';
                break;
            case 'retailcustomers':
                $api->base = '';
                $api->end_point = 'admin/customers.json?order='.rawurlencode('created_at DESC').'&limit=250';
                $api->count = 'admin/customers/count.json';
                break;
            case 'retailorders':
                $api->base = '';
                $api->end_point = 'admin/orders.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=any';
                $api->count = 'admin/orders.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=any';
                $api->partial = 'admin/orders.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=partial';
                $api->unshipped = 'admin/orders.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=unshipped';
                $api->shipped = 'admin/orders.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=shipped';
                $api->partialcount = 'admin/orders/count.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=partial';
                $api->unshippedcount = 'admin/orders/count.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=unshipped';
                $api->shippedcount = 'admin/orders/count.json?created_at_min='.rawurlencode('2017-08-31 01:00:00 CST -05:00').'&created_at_max='.rawurlencode('2017-09-01 23:59:59 CST -05:00').'&limit=250&fulfillment_status=shipped';
                break;
        }

        return $api;
    }
}
