<?php

namespace App\Http\Controllers\Exports;

use App\Services\Curl;
use App\Jobs\RunOdooCancelViaWebhook;
use App\Jobs\RunOdooFulfill;
use App\Jobs\RunOdooGenLabel;
use App\Jobs\RunOdooImportBulk;
use App\Jobs\RunOdooLineItemFix;
use App\Jobs\RunOdooMakeAvailable;
use App\Jobs\RunOdooViaWebhook;
use App\Jobs\RunOdooViaWebhookImport;
use App\Jobs\RunOdooViaWebhookExtra;
use App\Jobs\RunOdooViaWebhookProcess;
use App\Jobs\RunOdooViaWebhookProcessArchive;
use App\Jobs\RunOdooUnfuck;
use App\Jobs\RunOdooQuoteCron;
use App\Jobs\RunOdooWACron;
use App\Jobs\RunOdooDupesCron;
use App\Jobs\RunOdooFixClubCron;
use App\Jobs\RunShopifyExportWithPage;
use App\Jobs\RunOdooImportBulkByID;
use App\Jobs\RunOdooConfirm;
use App\Jobs\RunCatchUp;
use App\Jobs\RunOrderUpdate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use OdooClient\Client;
use Ripcord\Ripcord;
use Log;
use DB;

class OdooController extends ExportsController
{
    protected $uid;
    protected $rpcConn;
    protected $models;

    public function __construct()
    {
        $this->uid = 1;
        $this->rpcConn = $this->buildRpcConn();
        $this->models = Ripcord::client($this->rpcConn->url.'/xmlrpc/2/object');
    }

    /**
     * Compare the computed HMAC digest based on the shared secret and the request contents.
     *
     * @param  string $request
     * @return bool
     */
    private function validateSignature($request)
    {
        if ($request->header('x-shopify-hmac-sha256')) {
            $data = file_get_contents('php://input');
            $calculated_hmac = base64_encode(hash_hmac('sha256', $data, ''), true));

            return $calculated_hmac === $request->header('x-shopify-hmac-sha256');
        }

        return false;
    }

    public function bulkImportByNumber()
    {   
        set_time_limit(0);
        $numbers = [];

        foreach($numbers as $number){
            $job = (new RunOdooImportBulk($number))->onQueue('fixes');
            $this->dispatch($job);
            //$this->syncBulkInbound($number);
        }

        return view('fun.jesus');

    }

    public function fixShippingInfo()
    {
        $numbers = [];

        foreach($numbers as $key => $number){
            
            $result = $this->updateShopifyByOrigin($number);


            if($this->startsWith($number, '')){
                unset($numbers[$key]);
            }
            //$job = (new RunOdooFulfill($number))->onQueue('crons');
            //$this->dispatch($job);
             
            
            
        }

        echo '<pre>';
        print_r($numbers);
        echo '</pre>';
        die;
        

    }

    private function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 

    public function getFulfillments($id)
    {
        $shopify_api = '';
        $api_endpoint = 'admin/api/2019-07/orders/'.$id.'/fulfillments.json';
        $info = json_decode(Curl::get($shopify_api .'/'. $api_endpoint));

        if(empty($info->fulfillments)){
        //     'location_id' => 45061574,
         }
    }

    public function getShopifyOrderData($number)
    {
        $shopify_api = '';
        $api_endpoint = 'admin/orders.json?name='.urlencode($number);
        $info = json_decode(Curl::get($shopify_api .'/'. $api_endpoint));
        return $info->orders[0]->id;
    }

    public function checkOdooStatus($number)
    {
        $orderRPC = new \stdClass();
        $orderRPC->cols = ['id','name'];
        $orderRPC->limit = 1;
        $orderRPC->filter = [['client_order_ref', '=', $number]];
        $orderRPC->table = 'sale.order';

        $odoo_order = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit, 'search_read');
        $so_num = $odoo_order[0]['name'];
        
        $orderRPC = new \stdClass();
        $orderRPC->cols = ['id','state'];
        $orderRPC->limit = 1;
        $orderRPC->filter = [['origin', '=', $so_num]];
        $orderRPC->table = 'stock.picking';
        $odoo_status = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit, 'search_read');

        return $odoo_status[0]['state'];
    }

    public function testConnection()
    {
    
        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_quotes_last_month', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);

        echo '<pre>';
        print_r($order_ids);
        echo '</pre>';
        die;
        
    }

    public function fixQuotesCron()
    {


        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_quotes', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);

        foreach($order_ids as $id){
            
            if(empty($sales)){
                //build job and dispatch it
                $job = (new RunOdooQuoteCron($id['id']))->onQueue('crons');
                $this->dispatch($job);
            }
            //$res = $this->quoteCronTask($id['id'], $rpcConn, $uid, $models);
        }

        return view('fun.jesus');
    }

    public function unreserveClub()
    {

        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_club_unreserve', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);
        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooFixClubCron($id['id']))->onQueue('fixes');
            $this->dispatch($job);
            //$this->unreserveCronTask($id['id'], $rpcConn, $uid);
        }

        //$this->fixLastMonthClubCron();
        return view('fun.jesus');
           
    }

    public function fixQuotesLastMonthCron()
    {
        

        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_quotes_last_month', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);

        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooQuoteCron($id))->onQueue('crons');
            $this->dispatch($job);
            //$res = $this->quoteCronTask($id['id'], $rpcConn, $uid, $models);
        }
            
        return view('fun.jesus');
    }

    public function quoteCronTask($id)
    {
        //build XMLRPC object
        $oRPC = new \stdClass();
        $oRPC->cols = [];
        $oRPC->limit = 1;
        $oRPC->filter = [(int) $id];
        $oRPC->table = 'sale.order';

        

        //run query
        $res = $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'order_cancel');

        return $res;
        
    }

    public function fixWACron()
    {


        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_waiting_available', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit, 'search_read', '', 'min_date asc');
        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooWACron($id['id']))->onQueue('waiting_available_crons');
            $this->dispatch($job);
            //$res = $this->availableCronTask($id['id']);
        }
         
        return view('fun.jesus');
    }

    public function fixPACron()
    {

        
        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_partial_available', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit, 'search_read', '', 'min_date asc');
        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooWACron($id['id']))->onQueue('partial_available_crons');
            $this->dispatch($job);
            //$res = $this->availableCronTask($id['id'], $rpcConn, $uid);
        }
            
        
        return view('fun.jesus');
    }

    public function fixLastMonthWACron()
    {

        

        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_waiting_available_last_month', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit, 'search_read', '', 'min_date asc');
        
        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooWACron($id['id']))->onQueue('crons');
            $this->dispatch($job);
            //$res = $this->availableCronTask($id['id'], $rpcConn, $uid, $models);
        }
            
        return view('fun.jesus');
    }

    public function fixLastMonthPACron()
    {

        
        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_partial_available_last_month', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit, 'search_read', '', 'min_date asc');
        
        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooWACron($id['id']))->onQueue('crons');
            $this->dispatch($job);
            //$res = $this->availableCronTask($id['id'], $rpcConn, $uid, $models);
        }
            
        
        return view('fun.jesus');
    }

    

    public function fixLastMonthClubCron()
    {

        
        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_club_reserve', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit, 'search_read', '', 'id asc');

        foreach($order_ids as $id){

            //build job and dispatch it
            $job = (new RunOdooWACron($id['id']))->onQueue('fixes');
            $this->dispatch($job);
            //$res = $this->availableCronTask($id['id'], $rpcConn, $uid, $models);
        }
            
        return view('fun.jesus');
    }

    public function displayJesus()
    {
        return view('fun.jesus');
    }
    

    public function availableCronTask($id)
    {
        //build XMLRPC object
        $oRPC = new \stdClass();
        $oRPC->cols = [];
        $oRPC->limit = 1;
        $oRPC->filter = [(int) $id];
        $oRPC->table = 'stock.picking';

        //run query
        $res = $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'available');

        return $res;
        
    }

    public function unreserveCronTask($id)
    {
        //build XMLRPC object
        $oRPC = new \stdClass();
        $oRPC->cols = [];
        $oRPC->limit = 1;
        $oRPC->filter = [(int) $id];
        $oRPC->table = 'stock.picking';
        
        //run query
        $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'unreserve');

        return;
        
    }

    public function fixDupesCron()
    {

        
        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_all_open_sales', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);
        foreach($order_ids as $id){
            //build job and dispatch it
            $job = (new RunOdooDupesCron($id['client_order_ref']))->onQueue('crons');
            $this->dispatch($job);
            //$res = $this->dupesCronTask($id['client_order_ref'], $rpcConn, $uid);
        }
            
        return view('fun.jesus');
    }

    public function fixClubDupesCron()
    {
  

        $orderlookupObj = $this->buildRpcOrders('', '', 'lookup_all_open_club_sales', '', '');
        $order_ids = $this->runQuery($orderlookupObj->table, $orderlookupObj->filter, $orderlookupObj->cols, $orderlookupObj->limit);

        foreach($order_ids as $id){
            //build job and dispatch it
            $job = (new RunOdooDupesCron($id['client_order_ref'], $this->rpcConn, $this->uid))->onQueue('crons');
            $this->dispatch($job);
            //$res = $this->dupesCronTask($id['client_order_ref'], true);
        }
           
        return view('fun.jesus');
    }

    public function dupesCronTask($id, $is_club = false)
    {
        if($is_club === true){
            $olookupObj = $this->buildRpcOrders('', '', 'lookup_club', $id, '');
        } else {
            $olookupObj = $this->buildRpcOrders('', '', 'lookup', $id, '');
        }
        $oids = $this->runQuery($olookupObj->table, $olookupObj->filter, $olookupObj->cols, $olookupObj->limit);
        if( sizeof($oids) > 1 ){
            $result = [];
            foreach($oids as $k => $v){
                
                if($k !== 0){
                    //build XMLRPC object
                    $oRPC = new \stdClass();
                    $oRPC->cols = [];
                    $oRPC->limit = 1;
                    $oRPC->filter = [(int) $v['id']];
                    $oRPC->table = 'sale.order';
                    //run query
                    $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'sales_delete');
                }
                
            }
        }
       
        return;
    }
    

    public function unFuckOdoo()
    {
        $ids = [];

                
        //loop ids
        foreach ($ids as $id) {
            //build job and dispatch it
            //$job = (new RunOdooUnfuck($id, $rpcConn, $uid))->onQueue('fixes');
            //$this->dispatch($job);
            $this->unFuckOdooNow($id);
        }
            
        return view('fun.jesus');
    }

    public function unFuckOdooNow($id, $is_club = false)
    {
        $cols = '';
        $limit = 99999;
       
        if($is_club === false){
            $olookupObj = $this->buildRpcOrders('', '', 'lookup', $id, '');  
            $oids = $this->runQuery($olookupObj->table, $olookupObj->filter, $olookupObj->cols, $olookupObj->limit);
            
        } else {
            $olookupObj = $this->buildRpcOrders('', '', 'lookup_club', $id, '');  
            $oids = $this->runQuery($olookupObj->table, $olookupObj->filter, $olookupObj->cols, $olookupObj->limit);
            
        }

        if(!empty($oids)){
            foreach($oids as $k => $v){
                
                if($k !== 0){
                    //build XMLRPC object
                    $oRPC = new \stdClass();
                    $oRPC->cols = [];
                    $oRPC->limit = 1;
                    $oRPC->filter = [(int) $v['id']];
                    $oRPC->table = 'sale.order';
                    //run query
                    $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'sales_delete');
                } else {
                    //build XMLRPC object
                    $oRPC = new \stdClass();
                    $oRPC->cols = [];
                    $oRPC->limit = 1;
                    $oRPC->filter = [(int) $v['id']];
                    $oRPC->table = 'sale.order';   
                    
                    //run query
                    $result = $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'order_confirm');
                    return $result;
                } 
                
            }
        } else {
            return false;
        }
    }

    /**
     * force orders to be available by looping through array of stock pickking id's.
     *
     * Note: creates jobs in queue
     *
     * @param  none
     * @return none
     */
    public function getClubOrderStockIDs()
    {
        $ids = [];


        //loop ids
        foreach ($ids as $id) {
            //build job and dispatch it
            $job = (new RunOdooMakeAvailable($id, $rpcConn, $uid))->onQueue('fixes');
            $this->dispatch($job);
            //$this->updateClubOrders($id, $rpcConn, $uid, $models);
        }
           
        
        return view('fun.jesus');
    }

    /**
     *  this function is eecuted by the RunOdooMakeAvailable Job.
     *
     * @param  int $id
     * @param  object $rpcConn
     * @param  int $uid
     * @param  object $models
     * @return none
     */
    public function updateClubOrders($id)
    {
        //build XMLRPC object
        $oRPC = new \stdClass();
        $oRPC->cols = [];
        $oRPC->limit = 1;
        $oRPC->filter = [(int) $id];
        $oRPC->table = 'stock.picking';

        

        //run query
        $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'available');
        
    }

    /**
     * fired for the live import.
     *
     * Note: this creates jobs in the queue
     *
     * @param  object $request
     * @return none
     */
    public function syncOrder(Request $request)
    {

        //validate request from shopify
        if ($this->validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $decode_data = json_decode($data);
            //$this->syncInbound($decode_data);
            if (! empty($decode_data->note_attributes) || $decode_data->gateway == "Braintree") {
                if($this->has_tag('Subscription', $decode_data->tags)){
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('club');
                    $this->dispatch($job);
                } else if ($this->has_tag('Edit Order', $decode_data->tags)) {
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('edits');
                    $this->dispatch($job);
                } else if ($this->has_tag('twoFour', $decode_data->tags) || $this->has_tag('twoFour', $decode_data->tags)) {
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('twoFour');
                    $this->dispatch($job);
                } else {
                    $job = (new RunOdooViaWebhookExtra($decode_data))->onQueue('imports');
                    $this->dispatch($job);
                }
                
            }
        }
    }

    /**
     * fired for the live import.
     *
     * Note: this creates jobs in the queue
     *
     * @param  object $request
     * @return none
     */
    public function syncOrderAfterPaid(Request $request)
    {

        //validate request from shopify
        if ($this->validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $decode_data = json_decode($data);
            
           // $this->syncInbound($decode_data);
            if (empty($decode_data->note_attributes)) {
                //$this->syncInbound($decode_data);
                if($this->has_tag('Subscription', $decode_data->tags)){
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('club');
                    $this->dispatch($job);
                } else if ($this->has_tag('Edit Order', $decode_data->tags)) {
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('edits');
                    $this->dispatch($job);
                } else if ($this->has_tag('twoFour', $decode_data->tags) || $this->has_tag('twoFour', $decode_data->tags)) {
                    $job = (new RunOdooViaWebhook($decode_data))->onQueue('twoFour');
                    $this->dispatch($job);
                } else {
                    $job = (new RunOdooViaWebhookExtra($decode_data))->onQueue('imports');
                    $this->dispatch($job);
                }
            }
        }
    }

    public function noneOfTheAbove($decode_data)
    {
        if($this->isOrdertwoFour($decode_data)){
            $job = (new RunOdooViaWebhook($decode_data))->onQueue('twoFour');
            $this->dispatch($job);
        } else if($this->isStarbucks($decode_data)){
            return true;
        } else {
            $job = (new RunOdooViaWebhook($decode_data))->onQueue('imports');
            $this->dispatch($job);
        }
    }

    /**
     * fired when orders cancelled in Shopify, to cancel in Odoo.
     *
     * Note: this creates jobs in the queue
     *
     * @param  object $request
     * @return none
     */
    public function cancelOdooOrder(Request $request)
    {
        if ($this->validateSignature($request) === true) {
            $data = file_get_contents('php://input');
            $decode_data = json_decode($data);
            //$status = $this->cancelOrderNow($decode_data);
            $job = (new RunOdooCancelViaWebhook($decode_data));
            $this->dispatch($job);
        }
    }

    
    /**
     * fired to mark orders fulfilled in shopify from Odoo.
     *
     * Note: this creates jobs in the queue
     *
     * @param  string $origin
     * @return none
     */
    public function fulfillOdooOrder($origin)
    {
        $job = (new RunOdooFulfill($origin));
        $this->dispatch($job);
        //$this->updateShopifyByOrigin($origin);
    }

    /**
     * cancels order in odoo.
     *
     * @param  object $order
     * @return none
     */
    public function cancelOrderNow($order)
    {
        
        $name = $order->name;
        $orderRPC = new \stdClass();
        $orderRPC->cols = ['id'];
        $orderRPC->limit = 1;
        $orderRPC->filter = [['client_order_ref', '=', $name]];
        $orderRPC->table = 'sale.order';

        $odoo_order = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit, 'search_read');

        if(!empty($odoo_order[0]['id'])){
            $odoo_order_id = $odoo_order[0]['id'];

            $oRPC = new \stdClass();
            $oRPC->cols = [];
            $oRPC->limit = 1;
            $oRPC->filter = [$odoo_order_id];
            $oRPC->table = 'sale.order';

            $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'sales_delete');
        }

        return;
            
    }

    /**
     * import a single order into Odoo from shopify.
     *
     * @param  int $id
     * @return none
     */
    public function importOrderByID($id)
    {
        $api = '';
        $end_point = 'admin/orders/'.$id.'.json';

        $url = $api.'/'.$end_point;

        $new_prod_array = [];

        $orders = json_decode(Curl::get($url));

        if(isset($orders->errors) && $orders->errors === "Not Found" ){
            $rows = DB::table('need_import_live')->where('shopify_id',$id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
            return;
        }

        foreach ($orders as $order) {

            $this->syncInboundManual($order);
            
            
        }
    }

    /**
     * runs the live import.
     *
     * Note: fired by RunOdooViaWebhook Job
     *
     * @param  object $data
     * @return none
     */
    public function syncInbound($order)
    {
        $order_array = $this->splitClubItems($order);

        foreach($order_array as $k => $v){
            if(!empty($v)){
                if($k === "club"){
                    $is_club = true;
                } else {
                    $is_club = false;
                }
                //$result = $this->importOrderViaWebhook($v, $is_club);
                $istwoFour = $this->isOrdertwoFour($v);

                $data = DB::table('need_import_live')->where('shopify_id','=',$v->id)->get();
                if(!empty($data)){
                    $odoo_order = $this->isOrderInOdoo($v->name, $is_club);
                    if(!empty($odoo_order)){
                        if($odoo_order[0]['state'] === 'sale'){
                            $rows = DB::table('need_import_live')->where('shopify_id',$v->id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
                        } else if($odoo_order[0]['state'] === 'draft') {
                            $this->goConfirmOdoo($v->name, $is_club);
                        }
                    }
                } else {
                    $this->insertNeededImport($v->id, 'live', $is_club, $istwoFour);      
                }
            }
        }
        return;
           
    }

    /**
     * runs the live import.
     *
     * Note: fired by RunOdooViaWebhook Job
     *
     * @param  object $data
     * @return none
     */
    public function syncInboundManual($order)
    {
        $order_array = $this->splitClubItems($order);
        

        foreach($order_array as $k => $v){
            if(!empty($v)){
                if($k === "club"){
                    $is_club = true;
                } else {
                    $is_club = false;
                }

                $istwoFour = $this->isOrdertwoFour($v);
                //$result = $this->importOrderViaWebhook($v, $is_club);

                $data = DB::table('need_import')->where('shopify_id','=',$v->id)->get();
                if(!empty($data)){
                    $odoo_order = $this->isOrderInOdoo($v->name, $is_club);
                    if(!empty($odoo_order)){
                        if($odoo_order[0]['state'] === 'sale'){
                            $rows = DB::table('need_import')->where('shopify_id',$v->id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
                        } else if($odoo_order[0]['state'] === 'draft') {
                            $this->goConfirmOdoo($v->name, $is_club);
                        }
                    }
                } else {
                    $this->insertNeededImport($v->id, 'manual', $is_club, $istwoFour);      
                }
            }
        }
        return;
          
    }

    /**
     * mark orders fulfilled in shopify from Odoo.
     *
     * Note: fired from RunOdooFulfill Job
     *
     * @param  string $origin
     * @return none
     */
    public function updateShopifyByOrigin($origin)
    {
        
        $odoo_status = $this->checkOdooStatus($origin);
        if($odoo_status == "done"){

        $oRPC = new \stdClass();
        $oRPC->cols = ['id','name'];
        $oRPC->limit = 1;
        $oRPC->filter = [['client_order_ref', '=', $origin]];
        $oRPC->table = 'sale.order';
        $order = $this->runQuery($oRPC->table, $oRPC->filter, $oRPC->cols, $oRPC->limit, 'search_read');

        if (empty($order)) {
            return $number;
        }

        $number = $origin;
        $origin = $order[0]['name'];
        $shopify_data = $this->checkIfAllItemsFulfilled($number);

        if(empty($shopify_data->orders)){
            return $number;
        }

        $pickRPC = new \stdClass();
        $pickRPC->cols = ['carrier_tracking_ref', 'carrier_id'];
        $pickRPC->limit = 1;
        $pickRPC->filter = [['origin', '=', $origin], ['state', '=', 'done']];
        $pickRPC->table = 'stock.picking';
        $pick = $this->runQuery($pickRPC->table, $pickRPC->filter, $pickRPC->cols, $pickRPC->limit, 'search_read');

        foreach ($pick as $p) {
            $moveRPC = new \stdClass();
            $moveRPC->cols = ['product_id'];
            $moveRPC->limit = 999999;
            $moveRPC->filter = [['origin', '=', $origin], ['state', '=', 'done']];
            $moveRPC->table = 'stock.move';
            $moves = $this->runQuery($moveRPC->table, $moveRPC->filter, $moveRPC->cols, $moveRPC->limit, 'search_read');

            $line_items = [];
            $line_item_ids = [];

            foreach ($moves as $move) {
                preg_match_all("/\[([a-zA-Z0-9- ]+)\]/", $move['product_id'][1], $matches);

                if (! empty($shopify_data->orders[0]->line_items)) {
                    if (isset($matches[1][0])) {
                        
                        $search_result = $this->find_in_object($matches[1][0], $shopify_data->orders[0]->line_items);
                        if (is_object($search_result) && $search_result->fulfillment_status != 'fulfilled') {
                            $line_item_ids[] = $search_result->id;
                        }
                    }
                }
            }

            if (! empty($line_item_ids)) {
                foreach ($line_item_ids as $id) {
                    $line_items[] = ['id' => $id];
                }

                $delivery_dhl = [15];
                $delivery_fedex = [4, 5, 11, 12, 13, 14, 56, 86, 87, 90, 91, 93, 94];
                $delivery_usps = [59, 66];
                $carrier = 'FedEx';

                if (in_array($p['carrier_id'][0], $delivery_dhl)) {
                    $carrier = 'DHL eCommerce';
                }

                if (in_array($p['carrier_id'][0], $delivery_fedex)) {
                    $carrier = 'FedEx';
                }

                if (in_array($p['carrier_id'][0], $delivery_usps)) {
                    $carrier = 'USPS';
                }

                $fulfillment = [
                    'fulfillment' => [
                        'tracking_company' => $carrier,
                        'tracking_number' => $p['carrier_tracking_ref'],
                        'line_items' => $line_items,
                        'notify_customer' => false,
                        'location_id' => 45061574,
                    ],
                ];

                $json = json_encode($fulfillment);

                $shopify_api = '';
                $fulfillment_endpoint = 'admin/orders/'.$shopify_data->orders[0]->id.'/fulfillments.json';
                $url = $shopify_api.'/'.$fulfillment_endpoint;

                //push it to shopify
                sleep(3);
                $put_resp = json_decode(Curl::post($url, $json));

                return $put_resp;
            }
        }
        }
            
    }

    /**
     * search object for specific value.
     *
     * @param  string $value
     * @param  object $obj
     * @return object $obj[$key]
     */
    private function find_in_object($val, $obj)
    {
        foreach ($obj as $key => $value) {
           
 
            if ($value->sku === $val) {
                return $obj[$key];
            }
        }

        return false;
    }

    /**
     * get shopify order data by  #.
     *
     * @param  string $number
     * @return object $shopify_data
     */
    private function checkIfAllItemsFulfilled($number)
    {
        $shopify_api = '';
        $api_endpoint = 'admin/orders.json?name='.urlencode($number);

        $api_url = $shopify_api.'/'.$api_endpoint;
        $shopify_data = json_decode(Curl::get($api_url));

        return $shopify_data;
    }

    

    /**
     * compare array and object for intersection.
     *
     * @param  array $needle
     * @param  object $haystack
     * @return bool
     */
    public function has_club_item($needle, $haystack)
    {
        foreach ($haystack as $stack) {
            if ($needle == $stack) {
                return true;
            }
        }

        return false;
    }

    public function splitClubItems($order)
    {
        //$order = json_decode(json_encode($order));

        $order_copy = clone $order;
        $order_copy->line_items = [];

        $order_with_club = clone $order_copy;
        $order_without_club = clone $order_copy;
        $order_array = [];
        $order_array['club'] = [];
        $order_array['without_club'] = [];
        $club = [];
        $no_club = [];

        $club_ids = [];

        foreach ($order->line_items as $k => $v) {
            if ($this->has_club_item($v->product_id, $club_ids) === true) {
                $club[] = $v;
            } else {
                $no_club[] = $v;
            }
        }
        
        $order_with_club->line_items = $club;
        $order_with_club->isClubOrder = true;
        $order_with_club->hasClubItems = true;
        $order_with_club->club_order_date = Carbon::parse(''.$order->created_at.'')->toDateString();
        if(!empty($club)){
            $order_array['club'] = $order_with_club;
        }
    
        $order_without_club->line_items = $no_club;
        if(!empty($order_array['club'])){
            $order_without_club->hasClubItems = true;
        } else {
            $order_without_club->hasClubItems = false;
        }
        $order_without_club->isClubOrder = false;
        
        $order_without_club->club_order_date = '';
        if(!empty($no_club)){
            $order_array['without_club'] = $order_without_club;
        }
        
        return $order_array;
    }


    private function processOrder($order,$is_club)
    {
        $refid = $order->name;
        $cols = '';
        $limit = 1;
        $id = '';
        
        if(!isset($order->shipping_address->address1)){
            return;
        }

        $customerObj = $this->buildRpcOrders($cols, $limit, 'customerlookup', $id, $order);
        $customer_id = $this->runQuery($customerObj->table, $customerObj->filter, $customerObj->cols, $customerObj->limit);

        if (empty($customer_id)) {
            $modOrder = $this->modOrderArray($cols, $limit, $id, $order);
            $ccustomerObj = $this->buildRpcOrders($cols, $limit, 'createcustomer', $id, $modOrder);
            
            if(empty($ccustomerObj->filter)){
                return "customer cannot be created";
            }
            $customer_id = $this->runQuery($ccustomerObj->table, $ccustomerObj->filter, $ccustomerObj->cols, $ccustomerObj->limit, 'create');
            $order->odoo_cid = $customer_id;
        } else {
            $order->odoo_cid = $customer_id[0]['id'];
        }

        $order = $this->USPSTruncatedAddress($order);
        $shipping_id = $this->checkOrderShippingAddress($order);
        $order->shipping_id = $shipping_id;

        if ($order->referring_site == ""){
            
            if( strtolower($order->shipping_address->address1) == strtolower("") || strtolower($order->shipping_address->address1) == strtolower("")){
                $rpcObj = $this->buildRpcOrders($cols, $limit, 'createwalkin', $id, $order);
                $result = $this->runQuery($rpcObj->table, $rpcObj->filter, $rpcObj->cols, $rpcObj->limit, 'create');
                $res = $this->unFuckOdooNow($refid, $is_club);

                if((bool)$res !== true && is_numeric($result)){
                    $this->addToConfirm($result);
                } else if(! is_numeric($result)) {
                    return false;
                }
                return;
            }  else {

                $rpcObj = $this->buildRpcOrders($cols, $limit, 'create', $id, $order);
                $result = $this->runQuery($rpcObj->table, $rpcObj->filter, $rpcObj->cols, $rpcObj->limit, 'create');
                $res = $this->unFuckOdooNow($refid, $is_club);

                if((bool)$res !== true && is_numeric($result)){
                    $this->addToConfirm($result);
                } else if(! is_numeric($result)) {
                    return false;
                }
                
            }
        } else {

            if ($this->has_tag('walkin', $order->tags) || $this->has_tag('walk in', $order->tags)) {

                $rpcObj = $this->buildRpcOrders($cols, $limit, 'createwalkin', $id, $order);
                $result = $this->runQuery($rpcObj->table, $rpcObj->filter, $rpcObj->cols, $rpcObj->limit, 'create');
                $res = $this->unFuckOdooNow($refid, $is_club);

                if((bool)$res !== true && is_numeric($result)){
                    $this->addToConfirm($result);
                } 
                return;
            } else {

                $rpcObj = $this->buildRpcOrders($cols, $limit, 'create', $id, $order);
                $result = $this->runQuery($rpcObj->table, $rpcObj->filter, $rpcObj->cols, $rpcObj->limit, 'create');
                $res = $this->unFuckOdooNow($refid, $is_club);

                if((bool)$res !== true && is_numeric($result)){
                    $this->addToConfirm($result);
                } else if(! is_numeric($result)) {
                    return false;
                }
                
            }
        }
            

        return;
    }

    /**
     * live import heavy lifter, will add missing items to orders if order ID exists in odoo.
     *
     * @param  object $order
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @return none
     */
    public function importOrderViaWebhook($id)
    {
        set_time_limit(60);
        sleep(5);


        $api = '';
        $end_point = 'admin/orders/'.$id.'.json';
        $url = $api.'/'.$end_point;
        $new_prod_array = [];
        $orders = json_decode(Curl::get($url));

        foreach ($orders as $order){

            if($order === "Not Found"){
                return;
            }

            $order_array = $this->splitClubItems($order);
            $refid = $order->name;
            $cols = '';
            $limit = 1;

            foreach($order_array as $k => $v){
                if(!empty($v)){
                    if($k === "club"){
                        $is_club = true;
                    } else {
                        $is_club = false;
                    }

                    $odoo_order = $this->isOrderInOdoo($v->name, $is_club);
                    
                    if(!empty($odoo_order)){                                   
                        if($odoo_order[0]['state'] === 'sale'){
                            $rows = DB::table('need_import_live')->where('shopify_id',$v->id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
                        } else if($odoo_order[0]['state'] === 'draft') {
                            $this->goConfirmOdoo($v->name, $is_club);
                        }
                    } else {              

                        $result = $this->processOrder($v, $is_club);

                    }

                    
                }
            }
        }
        

        return;
    }

    /**
     * live import heavy lifter, will add missing items to orders if order ID exists in odoo.
     *
     * @param  object $order
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @return none
     */
    public function importOrderViaWebhookArchive($id)
    {
        set_time_limit(300);
        sleep(5);

        $api = '';
        $end_point = 'admin/orders/'.$id.'.json';
        $url = $api.'/'.$end_point;
        $new_prod_array = [];
        $orders = json_decode(Curl::get($url));

        foreach ($orders as $order){
           
            if($order === "Not Found"){
                return;
            }

            $order_array = $this->splitClubItems($order);
            $refid = $order->name;
            $cols = '';
            $limit = 1;

            foreach($order_array as $k => $v){
                if(!empty($v)){
                    if($k === "club"){
                        $is_club = true;
                    } else {
                        $is_club = false;
                    }

                    $odoo_order = $this->isOrderInOdoo($v->name, $is_club);
                    if(!empty($odoo_order)){                                   
                        if($odoo_order[0]['state'] === 'sale'){
                            $rows = DB::table('need_import')->where('shopify_id',$v->id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
                        } else if($odoo_order[0]['state'] === 'draft') {
                            $this->goConfirmOdoo($v->name, $is_club);
                        }
                    } else {
                        if(!isset($v->shipping_address->address1)){
                            $rows = DB::table('need_import')->where('shopify_id',$v->id)->update(['status' => 0, 'updated_at' => Carbon::now()]);
                        } else {       
                            $result = $this->processOrder($v, $is_club);
                            echo '<pre>';
                            print_r($result);
                            echo '</pre>';
                            die;
                        }

                    }

                    
                }
            }
        }
        

        return;
    }

    private function addToConfirm($id)
    {
        
        DB::table('order_confirm')->insert([
            [
                'record_id' => $id,
                'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), 
            ]
        ]);
        Log::info("Odoo Order ID: ".$id.", was not confirmed, added to table");
        
    }

    private function USPSTruncatedAddress($order) {

        if(!isset($order->shipping_address->address1)){
            return $order;
        }

        if(!isset($this->models) || !isset($this->rpcConn)){
            $this->uid = 1;
            $this->rpcConn = $this->buildRpcConn();
            $this->models = Ripcord::client($this->rpcConn->url.'/xmlrpc/2/object');
        }


        //get odoo customer id
        $customerEmail = $order->email;
        $filter = [[['email', '=', $customerEmail]]];
        $odooCustomerData = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,'res.partner', 'search_read', $filter, ['fields' => ['city', 'contact_address', 'street2', 'street']]);
        $odooCustomerId = $odooCustomerData[0]['id'];

        //create xml request for usps api
        $userId = '';

        $xml = '<AddressValidateRequest USERID="'.$userId.'">
        <Address>
        <Address1>'.$order->shipping_address->address1.'</Address1> 
        <Address2>'.$order->shipping_address->address2.'</Address2> 
        <City>'.$order->shipping_address->city.'</City> 
        <State>'.$order->shipping_address->province_code.'</State> 
        <Zip5>'.$order->shipping_address->zip.'</Zip5> 
        <Zip4></Zip4> 
        </Address> 
        </AddressValidateRequest>';

        $myXML = new \SimpleXMLElement('<AddressValidateRequest />');
        $myXML->addAttribute('USERID',$userId);
        $track = $myXML->addChild('Address');
        $track->addChild('Address1', $order->shipping_address->address1);

        if(isset($order->shipping_address->address2)){
            $track->addChild('Address2', htmlspecialchars($order->shipping_address->address2));
        } else {
            $track->addChild('Address2', '');
        }
        $track->addChild('City', $order->shipping_address->city);
        $track->addChild('State', $order->shipping_address->province_code);
        $track->addChild('Zip5', $order->shipping_address->zip);
        $track->addChild('Zip4', '');

        $url = 'https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML='.urlencode($myXML->asXML()).'';


        $data = file_get_contents($url);

      //extract usps standard shortened address
      $response = json_decode(json_encode(simplexml_load_string($data)));

      if(isset($response->Address->Address1)) {
        $address2 = $response->Address->Address1;
      }
      else {
        $address2 = $order->shipping_address->address2;
      }

      if(isset($response->Address->Address2)) {
        $address1 = $response->Address->Address2;
      }
      else {
        $address1 = $order->shipping_address->address1;
      }

      //get customer data from shopify
      $customerId = $order->customer->id;
      $url = '/admin/customers/'.$customerId.'.json';
      $shopifyCustomerData = json_decode(Curl::get($url));

      //if usps address is shorter than the address stored in shopify, update the customer address in shopfiy and odoo
      if(strlen($address1) + strlen($address2) < strlen($shopifyCustomerData->customer->default_address->address1) + strlen($shopifyCustomerData->customer->default_address->address2)) {
        $addressId = $order->customer->default_address->id;
        //update shopify
        $url = '/admin/customers/'.$customerId.'/addresses/'.$addressId.'.json';
        $data = [
          "address" => [
            "id" => $addressId,
            "address1" => "$address1"
          ]
        ];
        $json = json_encode($data);
        Curl::put($url, $json);
      }

        $order->shipping_address->address1 = $address1;
        $order->shipping_address->address2 = $address2;
      

      return $order;
    }

    /**
     * modify order array to include state and country codes.
     *
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @param  array $cols
     * @param  int $limit
     * @param  int $id
     * @param  object $order
     * @return none
     */
    private function modOrderArray($cols, $limit, $id, $order)
    {
        $state_id = null;
        $country_id = null;

        if (! empty($order->shipping_address->country_code) && isset($order->shipping_address->country_code)) {
            $countryObj = $this->buildRpcOrders($cols, $limit, 'lookupcountry', $id, $order);
            $country_data = $this->runQuery($countryObj->table, $countryObj->filter, $countryObj->cols, $countryObj->limit);

            $country_id = $country_data[0]['id'];

            if (! empty($order->shipping_address->province_code) && isset($order->shipping_address->province_code)) {
                $stateObj = $this->buildRpcOrders($cols, $limit, 'lookupstate', $country_id, $order);
                $state_data = $this->runQuery($stateObj->table, $stateObj->filter, $stateObj->cols, $stateObj->limit);

                if (! empty($state_data)) {
                    $state_id = $state_data[0]['id'];
                }
            }
        } elseif (! empty($order->billing_address->country_code) && isset($order->billing_address->country_code)) {
            $countryObj = $this->buildRpcOrders($cols, $limit, 'lookupcountry', $id, $order);
            $country_data = $this->runQuery($countryObj->table, $countryObj->filter, $countryObj->cols, $countryObj->limit);

            $country_id = $country_data[0]['id'];

            if (! empty($order->billing_address->province_code) && isset($order->billing_address->province_code)) {
                $stateObj = $this->buildRpcOrders($cols, $limit, 'lookupstate', $country_id, $order);
                $state_data = $this->runQuery($stateObj->table, $stateObj->filter, $stateObj->cols, $stateObj->limit);

                if (! empty($state_data)) {
                    $state_id = $state_data[0]['id'];
                }
            }
        }

        $order->state_id = $state_id;
        $order->country_id = $country_id;

        return $order;
    }

    /**
     * handles a bunch of different cases to appropriately build XMLRPC objects for orders and products being created.
     *
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @param  array $cols
     * @param  int $limit
     * @param  int $id
     * @param  object $order
     * @return none
     */
    private function buildRpcOrders($cols, $limit, $type, $id = null, $order = null)
    {
        $vars = new \stdClass();
        $street2 = '';

        switch ($type) {

            case 'customerlookup':
                $columns = ['id'];
                $filter = [['email', '=', $order->email]];
                $table = 'res.partner';
                break;

            case 'lookupstate':

                if (empty($order->shipping_address) || ! isset($order->shipping_address)) {
                    if (empty($order->billing_address) || ! isset($order->billing_address)) {
                        $code = '';
                    } else {
                        $code = $order->billing_address->province_code;
                    }
                } else {
                    $code = $order->shipping_address->province_code;
                }

                $columns = ['id'];
                $filter = [
                    ['code', '=', $code],
                    ['country_id', '=', $id],
                ];
                $table = 'res.country.state';
                break;

            case 'lookupcountry':

                if (empty($order->shipping_address) || ! isset($order->shipping_address)) {
                    if (empty($order->billing_address) || ! isset($order->billing_address)) {
                        $code = '';
                    } else {
                        $code = $order->billing_address->country_code;
                    }
                } else {
                    $code = $order->shipping_address->country_code;
                }

                $columns = ['id'];
                $filter = [['code', '=', $code]];
                $table = 'res.country';
                break;

            case 'lookupproduct':
                $columns = ['id'];
                $filter = [['default_code', '=', $id]];
                $table = 'product.product';
                break;

            case 'product_template_lookup':
                $columns = ['id'];
                $filter = [['name', '=', $id]];
                $table = 'product.template';
                break;

            case 'createcustomer':
                $customer_array = [];
                if (! empty($order->shipping_address)) {
                    if (empty($order->shipping_address->address1)) {
                        $street = $order->shipping_address->address2;
                    } else {
                        $street = $order->shipping_address->address1;
                        if (! empty($order->shipping_address->address2)) {
                            $street2 = $order->shipping_address->address2;
                        } else {
                            $street2 = '';
                        }
                    }

                    $phone = $order->shipping_address->phone;
                    if (is_null($phone)) {
                        $phone = '5555555555';
                    }

                    $customer_array = [
                        'name'=>$order->shipping_address->name,
                        'email'=>$order->email,
                        'street'=>$street,
                        'city'=>$order->shipping_address->city,
                        'state_id'=>$order->state_id,
                        'zip'=>$order->shipping_address->zip,
                        'company_type'=>'person',
                        'country_id'=>$order->country_id,
                        'street2'=> $street2,
                        'phone'=>$phone,
                    ];
                } elseif (! empty($order->billing_address)) {
                    if (empty($order->billing_address->address1)) {
                        $street = $order->billing_address->address2;
                    } else {
                        $street = $order->billing_address->address1;
                        if (! empty($order->billing_address->address2)) {
                            $street2 = $order->billing_address->address2;
                        } else {
                            $street2 = '';
                        }
                    }

                    $phone = $order->billing_address->phone;
                    if (is_null($phone)) {
                        $phone = '5555555555';
                    }

                    $customer_array = [
                        'name'=>$order->billing_address->name,
                        'email'=>$order->email,
                        'street'=>$street,
                        'city'=>$order->billing_address->city,
                        'state_id'=>$order->state_id,
                        'zip'=>$order->billing_address->zip,
                        'company_type'=>'person',
                        'country_id'=>$order->country_id,
                        'street2'=> $street2,
                        'phone'=>$phone,
                    ];
                }

                $columns = [];
                $filter = $customer_array;
                $table = 'res.partner';

                break;

            case 'createcustomer_newaddress':
                $customer_array = [];
                if (! empty($order->shipping_address)) {
                    if (empty($order->shipping_address->address1)) {
                        $street = $order->shipping_address->address2;
                    } else {
                        $street = $order->shipping_address->address1;
                        if (! empty($order->shipping_address->address2)) {
                            $street2 = $order->shipping_address->address2;
                        } else {
                            $street2 = '';
                        }
                    }

                    $phone = $order->shipping_address->phone;
                    if (is_null($phone)) {
                        $phone = '5555555555';
                    }

                    $customer_array = [
                        'email'=>$order->email,
                        'street'=>$street,
                        'city'=>$order->shipping_address->city,
                        'state_id'=>$order->state_id,
                        'zip'=>$order->shipping_address->zip,
                        'company_type'=>'person',
                        'country_id'=>$order->country_id,
                        'street2'=> $street2,
                        'parent_id'=>$order->odoo_cid,
                        'name'=>$order->shipping_address->name,
                        'type' => 'delivery',
                        'phone'=>$phone,
                    ];
                } elseif (! empty($order->billing_address)) {
                    if (empty($order->billing_address->address1)) {
                        $street = $order->billing_address->address2;
                    } else {
                        $street = $order->billing_address->address1;
                        if (! empty($order->billing_address->address2)) {
                            $street2 = $order->billing_address->address2;
                        } else {
                            $street2 = '';
                        }
                    }

                    $phone = $order->billing_address->phone;
                    if (is_null($phone)) {
                        $phone = '5555555555';
                    }

                    $customer_array = [
                        'email'=>$order->email,
                        'street'=>$street,
                        'city'=>$order->billing_address->city,
                        'state_id'=>$order->state_id,
                        'zip'=>$order->billing_address->zip,
                        'company_type'=>'person',
                        'country_id'=>$order->country_id,
                        'street2'=> $street2,
                        'parent_id'=>$order->odoo_cid,
                        'name'=>$order->billing_address->name,
                        'type' => 'delivery',
                        'phone'=>$phone,
                    ];
                }

                $columns = [];
                $filter = $customer_array;
                $table = 'res.partner';

                break;

            case 'lookup':
                $columns = ['id','state'];
                if (is_numeric($id)) {
                    $filter = [
                        ['id', '=', $id],
                    ];
                } else {
                    $filter = [
                        ['client_order_ref', '=', $id],
                        ['create_uid','=',1],
                        ['is_club','=',False],
                        ['state', 'in', ['sale','draft']],
                    ];
                }
                $table = 'sale.order';

                break;
            case 'lookup_club':
                $columns = ['id','state'];
                if (is_numeric($id)) {
                    $filter = [
                        ['id', '=', $id],
                    ];
                } else {
                    $filter = [
                        ['client_order_ref', '=', $id],
                        ['create_uid','=',1],
                        ['is_club','=',true],
                        ['state', 'in', ['sale','draft']],
                    ];
                }
                $table = 'sale.order';

                break;

            case 'existing':
                $columns = ['id','state'];
                if (is_numeric($id)) {
                    $filter = [
                        ['id', '=', $id],
                    ];
                } else {
                    $filter = [
                        ['id', '=', $id],
                    ];
                }
                $table = 'sale.order';

                break;
            
            case 'lookup_all_open_sales':
                $columns = ['id','client_order_ref'];
                $date = Carbon::now()->setTimezone('America/Chicago')->format('Y-m-d');
                if (is_numeric($id)) {
                    $filter = [
                        ['id', '=', $id],
                    ];
                } else {
                    $filter = [
                        ['state', 'in', ['sale']],
                        ['create_date', '>=', $date],
                        ['is_club','=',False],
                        ['create_uid','=',1],
                    ];
                }
                $table = 'sale.order';

                break;
            case 'lookup_all_open_club_sales':
                $columns = ['id','client_order_ref'];
                $date = Carbon::now()->setTimezone('America/Chicago')->format('Y-m');
                if (is_numeric($id)) {
                    $filter = [
                        ['id', '=', $id],
                    ];
                } else {
                    $filter = [
                        ['state', 'in', ['sale']],
                        ['create_date', 'like', $date],
                        ['is_club','=',true],
                        ['create_uid','=',1],
                    ];
                }
                $table = 'sale.order';

                break;

            case 'lookup_quotes':
                $columns = ['id','client_order_ref'];
                $date = Carbon::now()->setTimezone('America/Chicago')->format('Y-m');

                
                $filter = [
                    ['state', 'in', ['draft']],
                    ['create_date', 'like', $date],

                ];
                
                $table = 'sale.order';

                break;

            case 'lookup_club_unreserve':
                $columns = ['id'];
                $date = Carbon::now()->setTimezone('America/Chicago')->format('Y-m');
                
                $filter = [
                    ['state', 'in', ['assigned']],
                    ['club_order_date', 'like', $date],
                    ['is_club','=',True]

                ];
                
                $table = 'stock.picking';
                break;

            case 'lookup_quotes_last_month':
                $columns = ['id'];
                $date = Carbon::now()->setTimezone('America/Chicago')->subMonth()->format('Y-m');

                
                $filter = [
                    ['state', 'in', ['draft']],
                    ['create_date', 'like', $date],

                ];
                
                $table = 'sale.order';

                break;

            case 'lookup_waiting_available':
                $columns = ['id'];
                $date = Carbon::now()->setTimezone('America/Chicago')->subMonthsNoOverflow(1)->format('Y-m-d');
                $filter = [
                    ['state', 'in', ['confirmed']],
                    ['is_club','=',False],

                ];
                
                $table = 'stock.picking';

                break;

            case 'lookup_partial_available_last_month':
                $columns = ['id','min_date',];
                $date = Carbon::now()->setTimezone('America/Chicago')->subMonthsNoOverflow(1)->format('Y-m-d');
                
                $filter = [
                    ['state', 'in', ['partially_available']],
                    ['create_date', '<', $date],
                    ['is_club','=',False],
                ];
                
                $table = 'stock.picking';

                break;

            case 'lookup_partial_available':
                $columns = ['id'];
                $date = Carbon::now()->setTimezone('America/Chicago')->subMonthsNoOverflow(1)->format('Y-m-d');
                
                $filter = [
                    ['state', 'in', ['partially_available']],
                    ['is_club','=',False],
                ];
                
                $table = 'stock.picking';

                break;

            case 'lookup_club_reserve':
                $columns = ['id'];
                $now = Carbon::now()->format('d');

                if ($now >= 10){
                    $date = Carbon::now()->setTimezone('America/Chicago')->format('Y-m');
                } else {
                    $date = Carbon::now()->setTimezone('America/Chicago')->subMonthsNoOverflow(1)->format('Y-m');
                }
                
                
                $filter = [
                    ['state', 'in', ['confirmed','partially_available']],
                    ['club_order_date', 'like', $date],
                    ['is_club','=',True]

                ];
                
                $table = 'stock.picking';

                break;
        
            
            case 'lookup_waiting_available_last_month':
                $columns = ['id','min_date'];
                $date = Carbon::now()->setTimezone('America/Chicago')->subMonthsNoOverflow(1)->format('Y-m-d');
                
                $filter = [
                    ['state', 'in', ['confirmed']],
                    ['create_date', '<', $date],
                    ['is_club','=',False],
                ];
                
                $table = 'stock.picking';

                break;

            case 'create':
                $api_url = '';

                $istwoFour = $this->isOrdertwoFour($order);

                if ($order->source_name !== "pos"){
	                if (!empty($order->shipping_lines)) {
                        if($order->total_weight >= 453){
                            $carrier_id = 59;
                        } else {
                            switch ($order->shipping_lines[0]->code) {
                                case 'Standard Shipping':
                                    $carrier_id = 86;
                                    break;
                                case 'flat-rate':
                                    $carrier_id = 86;
                                    break;
                                case 'FEDEX_2_DAY':
                                    $carrier_id = 11;
                                    break;
                                case 'FEDEX_2_DAY_AM':
                                    $carrier_id = 12;
                                    break;
                                case 'FEDEX_EXPRESS_SAVER':
                                    $carrier_id = 90;
                                    break;
                                case 'FEDEX_GROUND':
                                    $carrier_id = 5;
                                    break;
                                case 'FIRST_OVERNIGHT':
                                    $carrier_id = 13;
                                    break;
                                case 'GROUND_HOME_DELIVERY':
                                    $carrier_id = 87;
                                    break;
                                case 'INTERNATIONAL_ECONOMY':
                                    $carrier_id = 99;
                                    break;
                                case 'INTERNATIONAL_FIRST':
                                    $carrier_id = 99;
                                    break;
                                case 'INTERNATIONAL_GROUND':
                                    $carrier_id = 99;
                                    break;
                                case 'INTERNATIONAL_PRIORITY':
                                    $carrier_id = 99;
                                    break;
                                case 'PRIORITY_OVERNIGHT':
                                    $carrier_id = 14;
                                    break;
                                case 'SMART_POST':
                                    $carrier_id = 86;
                                    break;
                                case 'STANDARD_OVERNIGHT':
                                    $carrier_id = 56;
                                    break;
                                case '':
                                    $carrier_id = 86;
                                    break;
                                default:
                                    $carrier_id = 86;
                                    break;
                            }
                        }
	                } else {
	                    $carrier_id = 86;
	                }
	            } else {
		            $carrier_id = 53;
	            }

                $created = Carbon::parse(''.$order->created_at.'')->setTimezone('UTC')->toDateTimeString();
                $written = Carbon::parse(''.$order->updated_at.'')->setTimezone('UTC')->toDateTimeString();
                $line_item_array = $this->add_line_item($cols, $limit, $type, $id, $order);

				if ($order->shipping_id !== FALSE){
					$order_array = [
	                    'date_order' => $created,
	                    'user_id' => 1,
	                    'partner_id' => $order->odoo_cid,
	                    'client_order_ref' => $order->name,
	                    'partner_invoice_id' => $order->odoo_cid,
	                    'pricelist_id' => 1,
	                    'state' => 'draft',
	                    'payment_term_id' => 1,
	                    'invoice_status' => 'invoiced',
	                    'write_date' => $written,
	                    'carrier_id' => $carrier_id,
	                    'is_twoFour' => $istwoFour,
	                    'partner_shipping_id' => $order->shipping_id,
	                    'is_club' => $order->isClubOrder,
	                    'club_order_date' => $order->club_order_date,
	                    'order_line' =>  $line_item_array,
	                ];
				} else {
					$order_array = [
	                    'date_order' => $created,
	                    'user_id' => 1,
	                    'partner_id' => $order->odoo_cid,
	                    'client_order_ref' => $order->name,
	                    'partner_invoice_id' => $order->odoo_cid,
	                    'pricelist_id' => 1,
	                    'state' => 'draft',
	                    'payment_term_id' => 1,
	                    'invoice_status' => 'invoiced',
	                    'write_date' => $written,
	                    'carrier_id' => $carrier_id,
	                    'is_twoFour' => $istwoFour,
	                    'is_club' => $order->isClubOrder,
	                    'club_order_date' => $order->club_order_date,
                        'order_line' =>  $line_item_array,
                        'partner_shipping_id' => $order->odoo_cid,
	                ];
				}
                

                $columns = [];
                $filter = $order_array;
                $table = 'sale.order';

                break;

                case 'createwalkin':

                    $api_url = '';
                    $line_items = $order->line_items;
                    $customer = $order->customer;
                    $twoFour_count = 0;
                    $line_item_count = sizeof($line_items);
                    $istwoFour = false;

                    if($order->hasClubItems === false) {
                        foreach ($line_items as $item) {
                            $product_id = $item->product_id;
                            $path = 'admin/products/'.$product_id.'.json';
                            $url = $api_url.'/'.$path;
                            $product_data = json_decode(Curl::get($url));

                            if (! isset($product_data->product)) {
                                continue;
                            }

                            if ($this->has_tag('twoFour', $product_data->product->tags) === true || $this->has_tag('twoFour', $product_data->product->tags) === true) {
                                $twoFour_count++;
                            }
                        }

                        if ($twoFour_count === $line_item_count) {
                            $istwoFour = true;
                        }
                    }

                    
                    $carrier_id = 53;
                    

                    $created = Carbon::parse(''.$order->created_at.'')->setTimezone('UTC')->toDateTimeString();
                    $written = Carbon::parse(''.$order->updated_at.'')->setTimezone('UTC')->toDateTimeString();
                    $line_item_array = $this->add_line_item($cols, $limit, $type, $id, $order);
                    
                    if ($order->shipping_id !== FALSE){
                        $order_array = [
                            'date_order' => $created,
                            'user_id' => 1,
                            'partner_id' => $order->odoo_cid,
                            'client_order_ref' => $order->name,
                            'partner_invoice_id' => $order->odoo_cid,
                            'pricelist_id' => 1,
                            'state' => 'sale',
                            'payment_term_id' => 1,
                            'invoice_status' => 'invoiced',
                            'write_date' => $written,
                            'carrier_id' => $carrier_id,
                            'is_twoFour' => $istwoFour,
                            'partner_shipping_id' => $order->shipping_id,
                            'is_club' => $order->isClubOrder,
                            'club_order_date' => $order->club_order_date,
                            'order_line' =>  $line_item_array,
                        ];
                    } else {
                        $order_array = [
                            'date_order' => $created,
                            'user_id' => 1,
                            'partner_id' => $order->odoo_cid,
                            'client_order_ref' => $order->name,
                            'partner_invoice_id' => $order->odoo_cid,
                            'pricelist_id' => 1,
                            'state' => 'sale',
                            'payment_term_id' => 1,
                            'invoice_status' => 'invoiced',
                            'write_date' => $written,
                            'carrier_id' => $carrier_id,
                            'is_twoFour' => $istwoFour,
                            'is_club' => $order->isClubOrder,
                            'club_order_date' => $order->club_order_date,
                            'order_line' =>  $line_item_array,
                            'partner_shipping_id' => $order->odoo_cid,
                        ];
                    }
                    

                    $columns = [];
                    $filter = $order_array;
                    $table = 'sale.order';

                break;

            case 'lookuppav':
                $columns = ['id'];
                $filter = [['name', '=', $id]];
                $table = 'product.attribute.value';
                break;

            case 'lookuppap':
                $columns = ['id'];
                $filter = [['product_tmpl_id', '=', $order->template_id], ['value_id', '=', $id]];
                $table = 'product.attribute.price';
                break;

            case 'create_product':

                $product = $order;

                $created = Carbon::parse(''.$product->created_at.'')->setTimezone('UTC')->toDateTimeString();
                $written = Carbon::parse(''.$product->updated_at.'')->setTimezone('UTC')->toDateTimeString();

                $variant = [
                    'create_date' => $created,
                    'weight' => $product->weight,
                    'default_code' => $product->sku,
                    'barcode' => $product->sku,
                    'product_tmpl_id' => $product->template_id,
                    'write_date' => $written,
                    'actve' => $product->active,
                    'x_shopify_id' => (string) $product->product_id,
                ];

                $limit = 1;
                $columns = [];
                $filter = $variant;
                $table = 'product.product';
                break;

            case 'create_product_template':

                $product = $order;

                $created = Carbon::parse(''.$product->created_at.'')->setTimezone('UTC')->toDateTimeString();
                $written = Carbon::parse(''.$product->updated_at.'')->setTimezone('UTC')->toDateTimeString();

                if (isset($product->published_scope)) {
                    $active = true;
                } else {
                    $active = false;
                }

                if (isset($product->variants)) {
                    $product_variants = $product->variants;
                    $price = $product_variants[0]->price;
                }

                $product_template_array = [
                    'list_price' => $price,
                    'sale_ok' => true,
                    'create_date' => $created,
                    'categ_id' => 1,
                    'company_id' => 1,
                    'uom_po_id' => 1,
                    'description' => $product->body_html,
                    'volume' => 0,
                    'write_date' =>  $written,
                    'active' => $active,
                    'rental' => false,
                    'name' => $product->title,
                    'type' => 'product',
                    'track_service' => 'manual',
                    'invoice_policy' => 'order',
                    'sale_delay' => 7,
                    'tracking' => 'none',
                    'purchase_ok' => true,
                    'purchase_method' => 'receive',
                    'produce_delay' => 1,
                    'barcode' => $product->variants[0]->sku,
                    'default_code' => $product->variants[0]->sku,
                    'standard_price' => $product->variants[0]->price,
                    'weight' => $product->variants[0]->weight,
                    'x_shopify_id' => (string) $product->id,
                ];

                $limit = 1;
                $columns = [];
                $filter = $product_template_array;
                $table = 'product.template';

                break;

            case 'build_pap':

                $variant = $order;

                $price_diff = $variant->price - $variant->base_price;

                $created = Carbon::parse(''.$variant->created_at.'')->setTimezone('UTC')->toDateTimeString();
                $written = Carbon::parse(''.$variant->updated_at.'')->setTimezone('UTC')->toDateTimeString();

                $pap_array = [
                    'create_date' => $created,
                    'write_date' => $written,
                    'product_tmpl_id' => $variant->template_id,
                    'value_id' => $variant->value_id,
                    'price_extra' => $price_diff,
                ];

                $limit = 1;
                $columns = [];
                $filter = $pap_array;
                $table = 'product.attribute.price';
                break;

            case 'build_rel':

                $variant = $order;

                $rel_array = [
                    'att_id' => (int) $variant->value_id,
                    'prod_id' => (int) $variant->odoo_product_id,
                ];

                $limit = 1;
                $columns = [];
                $filter = $rel_array;
                $table = 'product.attribute.value.product.product.rel';
                break;

            default:
                abort(400);
        }

        $vars->cols = $columns;
        $vars->limit = $limit;
        $vars->filter = $filter;
        $vars->table = $table;

        return $vars;
    }

    public function isOrdertwoFour($order)
    {
        $api_url = '';
        $line_items = $order->line_items;
        //$customer = $order->customer;
        $twoFour_count = 0;
        $line_item_count = sizeof($line_items);
        $istwoFour = false;

        if( isset($order->hasClubItems) && $order->hasClubItems === false) {
            foreach ($line_items as $item) {
                $product_id = $item->product_id;
                $path = 'admin/products/'.$product_id.'.json';
                $url = $api_url.'/'.$path;
                $product_data = json_decode(Curl::get($url));

                if (! isset($product_data->product)) {
                    continue;
                }

                if ($this->has_tag('twoFour', $product_data->product->tags) === true || $this->has_tag('twoFour', $product_data->product->tags) === true) {
                    $twoFour_count++;
                }
            }

            if ($twoFour_count === $line_item_count) {
                $istwoFour = true;
            }
        }

        return $istwoFour;
    }

    public function isStarbucks($order)
    {
        $line_items = $order->line_items;
        $sb_ids = [];

        foreach ($order->line_items as $k => $v) {
            if ($this->has_club_item($v->sku, $sb_ids) === true) {
                return true;
            }
        }
        
    }

    private function add_line_item($cols, $limit, $type, $id, $order){
        $api_url = '';
        $line_items = $order->line_items;
        
        if(isset($order->customer)){
            $customer = $order->customer;
        }
        
       
        $columns = [];
        $line_item_array = [];
        foreach ($order->line_items as $line) {
            if ($line->fulfillment_status == 'fulfilled') {
                continue;
            }

            $productRPC = new \stdClass();
            $productRPC->cols = ['id'];
            $productRPC->limit = 1;
            $productRPC->filter = [['default_code', '=', $line->sku]];
            $productRPC->table = 'product.product';

            $product_id = $this->runQuery($productRPC->table, $productRPC->filter, $productRPC->cols, $productRPC->limit);

            if (empty($product_id)) {
                continue;
            }
            $pid = $product_id[0]['id'];

            $line_item_vals = [
                'product_uom_qty' => $line->quantity,
                'product_id' => $pid,
                'state' => 'sale',
                'invoice_status' => 'invoiced',
                'product_uom' => 1,
                'order_partner_id' => $order->odoo_cid,
                'qty_invoiced' => $line->quantity,
            ];

            $line_item_array[] = array(0,0,$line_item_vals);

        }

        return $line_item_array;
    }

    /**
     * search shopify tags for specific string.
     *
     * @param  string $needle
     * @param  string $haystack
     * @return bool
     */
    private function has_tag($needle, $haystack)
    {
        $haystack = explode(', ', $haystack);
        foreach ($haystack as $stack) {
            if (strtolower($needle) == strtolower($stack)) {
                return true;
            }
        }

        return false;
    }

    /**
     * calculate tax via taxlines from shopify order.
     *
     * @param  object $taxLines
     * @return float $tax_amount
     */
    private function calcTotalTax($taxLines)
    {
        $tax_amount = 0.00;

        if (! empty($taxLines)) {
            foreach ($taxLines as $tax) {
                $tax_amount = $tax_amount + $tax->price;
            }
        }

        return $tax_amount;
    }

    /**
     * checks if order needs to have a new address created for shipping or not.
     *
     * @param  object $order
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @return none
     */
    public function checkOrderShippingAddress($order)
    {
        if (isset($order->odoo_cid)) {
            $cRPC = new \stdClass();
            $cRPC->cols = [];
            $cRPC->limit = 1;
            $cRPC->filter = [['id', '=', $order->odoo_cid]];
            $cRPC->table = 'res.partner';

            $new_shipping_address_id = '';

            $odoo_customer = $this->runQuery($cRPC->table, $cRPC->filter, $cRPC->cols, $cRPC->limit, 'search_read');
            $odoo_cust_add = $odoo_customer[0]['street'];

            if(!isset($order->shipping_address)){
                return false;
            }

            if (! isset($order->shipping_address->address1)) {
                return FALSE;
            }

            $order_shipping_add = $order->shipping_address->address1;


            if ($odoo_cust_add != $order_shipping_add) {
                $custRPC = new \stdClass();
                $custRPC->cols = ['id', 'street'];
                $custRPC->limit = 99999;
                $custRPC->filter = [['parent_id', '=', $order->odoo_cid]];
                $custRPC->table = 'res.partner';

                $odoo_customer_additional = $this->runQuery($custRPC->table, $custRPC->filter, $custRPC->cols, $custRPC->limit, 'search_read');

                if (empty($odoo_customer_additional)) {
                    $cols = '';
                    $limit = 1;
                    $id = '';
                    $modOrder = $this->modOrderArray($cols, $limit, $id, $order);

                    $ccustomerObj = $this->buildRpcOrders($cols, $limit, 'createcustomer_newaddress', $id, $modOrder);
                    $new_shipping_address_id = $this->runQuery($ccustomerObj->table, $ccustomerObj->filter, $ccustomerObj->cols, $ccustomerObj->limit, 'create');
                    return $new_shipping_address_id;
                } else {
                    $address_found = false;

                    foreach ($odoo_customer_additional as $oca) {
                        if ($oca['street'] == $order_shipping_add) {
                            $address_found = $oca;
                        }
                    }

                    if ($address_found === false) {
                        $cols = '';
                        $limit = 1;
                        $id = '';
                        $modOrder = $this->modOrderArray($cols, $limit, $id, $order);

                        $ccustomerObj = $this->buildRpcOrders($cols, $limit, 'createcustomer_newaddress', $id, $modOrder);
                        $new_shipping_address_id = $this->runQuery($ccustomerObj->table, $ccustomerObj->filter, $ccustomerObj->cols, $ccustomerObj->limit, 'create');
                        return $new_shipping_address_id;
                    } else {
                        return $address_found['id'];
                    }
                }
                
            } else {
	            return FALSE;
            }
        }
    }

    /**
     * update odoo database with shopify product id's.
     *
     * @param  none
     * @return none
     */
    public function updateOdooDBWithShopID()
    {
        $data = '';
        

        //$res = $models->execute_kw($rpcConn->db, $uid, $rpcConn->password, 'product.product', 'write', array(array(6360), array('x_shopify_id'=>'6293951238')));
        $api = ExportsController::getApiEndPoints('products');
        $num_of = ExportsController::getNumberOf($api);
        $total_pages = ExportsController::totalPagesOfData($num_of);
        $new_prod_array = [];

        for ($i = 1; $i <= $total_pages; $i++) {
            $api_url = $api->base.'/'.$api->end_point.'&page='.$i;

            $products = json_decode(Curl::get($api_url));

            foreach ($products->products as $product) {
                $explode_sku = explode('-', $product->variants[0]->sku);

                if ((substr($explode_sku[0], 0, 2) === '' || substr($explode_sku[0], 0, 2) === 'CG') && strpos($explode_sku[0], '.') === false) {
                    $new_prod_array[$product->id] = $explode_sku[0];
                }
            }
        }

        foreach ($new_prod_array as $k => $v) {
            $sku = $v.'-';

            $pRPC = new \stdClass();;
            $pRPC->cols = ['id'];
            $pRPC->limit = 99999;
            $pRPC->filter = [
                ['default_code', 'like', ''.$sku.''],
            ];
            $pRPC->table = 'product.product';

            $data = $this->runQuery($pRPC->table, $pRPC->filter, $pRPC->cols, $pRPC->limit);

            foreach ($data as $d) {
                $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,'product.product', 'write', [[$d['id']], ['x_shopify_id'=>''.$k.'']]);
            }
        }
            
    }

    /**
     * updates shopify product inventory amounts from odoo.
     *
     * @param  string $sku
     * @param  int $qty
     * @param  int $shopify_id
     * @return none
     */
    public function liveUpdateInv($sku, $qty, $shopify_id)
    {
        return;
        
        if (! isset($sku) || $sku == null || $shopify_id === 'False') {
            return;
        }

        $product_array = [];
        $variant_array = [];

        $product = $this->getShopifyDataByID('products', $shopify_id);

        $api = ExportsController::getApiEndPoints('products');

        //if ($product->product !== false && is_object($product)) {
        if (isset($product->products)) {
            foreach ($product->products as $p) {
                $variant_id = '';

                foreach ($p->variants as $variant) {
                    if ($variant->sku == $sku) {
                        $path = 'admin/api/2019-04/products/'.(int)$shopify_id.'/variants/'.(int)$variant->id.'.json';
                        $url = $api->base.'/'.$path;
                        $resp = json_decode(Curl::get($url));
                        $inventory_item_id = $resp->variant->inventory_item_id;


                        $variant_array = [
                            'inventory_item_id' => (int) $inventory_item_id,
                            'available' => (int) $qty,
                            'location_id' => 45061574,
                        ];
                    }
                }
            }

            $json = json_encode($variant_array);
            $path = 'admin/api/2019-04/inventory_levels/set.json';
            $url = $api->base.'/'.$path;
            $resp = json_decode(Curl::post($url, $json));
        }
    }

    /**
     * heavy lifting function for mass inventory sync.
     *
     * @param  array $data
     * @param  string $type
     * @return bool
     */
    private function syncProduct($data, $type)
    {
        $product = $this->getShopifyDataByTitle($type, $data[0]['name_template']);
        $api = ExportsController::getApiEndPoints($type);

        $product_array = [];
        $variant_array = [];

        foreach ($data as $d) {
            foreach ($product->products as $p) {
                $product_array['id'] = $p->id;

                foreach ($p->variants as $variant) {
                    if ($variant->sku == $d['default_code']) {
                        $variant_array[] = [
                            'id' => $variant->id,
                            'inventory_quantity' => $d['virtual_available'],
                        ];
                    }
                }

                $product_array['variants'] = $variant_array;
            }
        }

        $json = json_encode(['product'=>$product_array]);
        $path = 'admin/products/'.$product_array['id'].'.json';
        $url = $api->base.'/'.$path;
        json_encode(Curl::put($url, $json));

        return true;
    }

    /**
     * get shopify product data by product title.
     *
     * @param  string $type
     * @param  string $title
     * @return bool or object
     */
    private function getShopifyDataByTitle($type, $title)
    {
        $type = strtolower($type);

        $api = ExportsController::getApiEndPoints($type);
        $api->search_end_point = 'admin/products.json?title='.rawurlencode($title).'';
        $api_url = $api->base.'/'.$api->search_end_point;

        $data = json_decode(Curl::get($api_url));

        if (! empty($data)) {
            foreach ($data->products as $d) {
                if ($d->title === $title) {
                    return $d;
                }
            }
        }

        return false;
    }

    /**
     * get shopify product data by product id.
     *
     * @param  string $type
     * @param  int $id
     * @return bool or object
     */
    private function getShopifyDataByID($type, $id)
    {
        $type = strtolower($type);

        $api = ExportsController::getApiEndPoints($type);
        $api->search_end_point = 'admin/products.json?ids='.$id;
        $api_url = $api->base.'/'.$api->search_end_point;

        $data = json_decode(Curl::get($api_url));

        if (! empty($data)) {
            return $data;
        }

        return false;
    }

    /**
     * builds XMLRPC connection object from env file.
     *
     * @param  none
     * @return object $rpcConn
     */
    private function buildRpcConn()
    {
        $rpcConn = new \stdClass();
        $rpcConn->url = '');
        $rpcConn->db = '');
        $rpcConn->username = '');
        $rpcConn->password = '');

        return $rpcConn;
    }

    /**
     * builds XMLRPC objects for different cases needed by different functions within this class.
     *
     * @param  object $models
     * @param  int $uid
     * @param  object $rpcConn
     * @param  array $cols
     * @param  int $limit
     * @param  string $type
     * @param  string $sku
     * @return object $vars
     */
    private function buildRpcObj($cols, $limit, $type, $sku = '')
    {
        $vars = new \stdClass();
        $filter = [];
        $columns = [];
        $table = '';

        switch ($type) {

            case 'customers':
                $columns = ($cols == 'all' ? [] : ['id', 'name', 'email']);
                $filter = [
                    ['customer', '=', true],
                ];
                $table = 'res.partner';
                break;

            case 'unfuck_find_order':
                $columns = ($cols == 'all' ? [] : ['create_date', 'order_line', 'invoice_ids', 'date_order', 'partner_id', 'picking_policy', 'state', 'amount_total', 'payment_term_id', 'partner_invoice_id', 'product_id']);

                $table = 'sale.order';

                if (! empty($sku)) {
                    $orderRPC = new \stdClass();
                    $orderRPC->cols = ['id'];
                    $orderRPC->limit = 1;
                    $orderRPC->filter = [['name', '=', $sku], ['state', '!=', 'cancel']];
                    $orderRPC->table = 'sale.order';

                    $order_id = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit);

                    if (! empty($order_id)) {
                        $filter = [$order_id[0]['id']];
                    } else {
                        $filter = false;
                    }
                } else {
                    $filter = false;
                }
                break;

            case 'unfuck_find_order_by_id':
                $columns = ($cols == 'all' ? [] : ['create_date', 'order_line', 'invoice_ids', 'date_order', 'partner_id', 'picking_policy', 'state', 'amount_total', 'payment_term_id', 'partner_invoice_id', 'product_id']);

                $table = 'stock.picking';

                if (! empty($sku)) {
                    $filter = [
                        ['id', '=', $sku],
                    ];
                } else {
                    $filter = false;
                }
                break;

            case 'orders':
                $columns = ($cols == 'all' ? [] : ['create_date', 'order_line', 'invoice_ids', 'date_order', 'partner_id', 'picking_policy', 'state', 'amount_total', 'payment_term_id', 'partner_invoice_id', 'product_id']);

                $table = 'sale.order';

                if (! empty($sku)) {
                    $orderRPC = new \stdClass();
                    $orderRPC->cols = ['id'];
                    $orderRPC->limit = 1;
                    $orderRPC->filter = [['client_order_ref', 'like', $sku]];
                    $orderRPC->table = 'sale.order';

                    $order_id = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit);

                    if (! empty($order_id)) {
                        $filter = [$order_id[0]['id']];
                    } else {
                        $filter = false;
                    }
                } else {
                    $filter = [
                        ['state', '=', 'sale'],
                    ];
                }
                break;

            case 'get_all_orders':
                $columns = ['id', 'client_order_ref'];
                $table = 'sale.order';
                $filter = [['state', '=', 'sale']];

                break;

            case 'order_lines':
                $columns = [];

                $table = 'sale.order.line';

                if (! empty($sku)) {
                    $orderRPC = new \stdClass();
                    $orderRPC->cols = ['id'];
                    $orderRPC->limit = 1;
                    $orderRPC->filter = [['client_order_ref', 'like', $sku]];
                    $orderRPC->table = 'sale.order';

                    $order_id = $this->runQuery($orderRPC->table, $orderRPC->filter, $orderRPC->cols, $orderRPC->limit);
                    if (! empty($order_id)) {
                        $filter = [
                            ['order_id', '=', $order_id[0]['id']],
                        ];
                    } else {
                        $filter = false;
                    }
                } else {
                    $filter = false;
                }
                break;

            case 'order_create_object':
                $columns = [];

                $table = 'sale.order.line';

                if (! empty($sku)) {
                    $filter = [$sku];
                } else {
                    $filter = false;
                }
                break;

            case 'unfuck_build_object':
                $columns = [];

                $table = 'stock.picking';

                if (! empty($sku)) {
                    $filter = [$sku];
                } else {
                    $filter = false;
                }
                break;

            case 'products':
                $columns = ($cols == 'all' ? [] : ['id', 'default_code', 'qty_available', 'name_template', 'type', 'virtual_available']);

                if (! empty($sku)) {
                    $filter = [
                        ['active', '=', true],
                        ['default_code', 'like', ''.$sku.''],
                        ['type', '=', 'product'],
                    ];
                } else {
                    $filter = [
                        ['active', '=', true],
                        ['type', '=', 'product'],
                    ];
                }

                $table = 'product.product';
                break;

            case 'unfuck':

                $columns = [];

                $filter = [
                    ['state', '!=', 'cancel'],
                    ['state', '!=', 'done'],
                ];

                $table = 'stock.picking';

                break;

            case 'unfuck_find_all':

                $columns = [];

                $filter = [
                    ['origin', '=', $sku],
                ];

                $table = 'stock.picking';

                break;

            default:
                abort(400);
        }

        $vars->cols = $columns;
        $vars->limit = $limit;
        $vars->filter = $filter;
        $vars->table = $table;

        return $vars;
    }

    /**
     * execute our XMLRPC queries.
     *
     * @param  object $models
     * @param  string $db
     * @param  int $uid
     * @param  string $password
     * @param  string $table
     * @param  array $filter
     * @param  array $cols
     * @param  int $qLimit
     * @param  string $type
     * @param  int $offset
     * @return object $data
     */
    private function runQuery($table, $filter, $cols, $qLimit, $type = 'search_read', $offset = '', $order = '')
    {
        set_time_limit(0);
        $data = '';
        if(!isset($this->models) || !isset($this->rpcConn)){
            $this->uid = 1;
            $this->rpcConn = $this->buildRpcConn();
            $this->models = Ripcord::client($this->rpcConn->url.'/xmlrpc/2/object');
        }

        switch ($type) {

            case 'create':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,
                    $table, 'create',
                    [$filter]);

                break;

            case 'create_lineitems':

                foreach ($filter as $f) {
                    $data[] = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,
                        $table, 'create',
                        [$f]);
                }

                break;

            case 'search_read':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,
                    $table, 'search_read',
                    [$filter],
                    ['fields'=>$cols, 'limit'=>$qLimit, 'offset'=>$offset, 'order'=>$order]);

                break;

            case 'sales_delete':

                $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'sale.order', 'action_cancel', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;

            case 'available':

               $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'stock.picking', 'action_assign', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;

            case 'order_confirm':

               $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'sale.order', 'action_confirm', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;
            case 'order_cancel':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'sale.order', 'action_cancel', [$filter]);
 
                 //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));
 
                 break;
            case 'order_done':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'stock.picking', 'action_done', [$filter]);
 
                 //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));
 
                 break;

            case 'order_draft':

                 $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'sale.order', 'action_draft', [$filter]);
  
                  //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));
  
                  break;

            case 'order_shipping_label':

                $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'stock.picking', 'generate_shipping', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;

            case 'create_shipping_label':

                $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'shipping.response', 'generate_tracking_no', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;

            case 'unreserve':

                $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'stock.picking', 'do_unreserve', [$filter]);

                //$data = $models->execute_kw($db, $uid, $password, 'sale.order', 'unlink',array($filter));

                break;

            case 'list':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,
                    $table, 'search',
                    [$filter]);

                break;

            case 'paginate':

                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password,
                    $table, 'search',
                    [$filter],
                    ['offset'=>$offset, 'limit'=>$qLimit]);
                break;

            case 'unfuck_shit':
                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'stock.picking', 'action_cancel', [$filter]);
                break;

            case 'remove_address':
                $data = $this->models->execute_kw($this->rpcConn->db, $this->uid, $this->rpcConn->password, 'res.partner', 'unlink', [$filter]);
                break;
        }
        unset($this->models);
        return $data;
    }



}
