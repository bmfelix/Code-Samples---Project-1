<?php

namespace App\Http\Controllers\Shipping;

use DB;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Shipping Controller.
 *
 * handles the timer in shipping, the methods and functionality involed with processing walking orders for the warehouse and the front desk
 *
 * @author    Brad Felix <bradfelix1@gmail.com>
 * @version   v1.2
 */
class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * displays the shipping timer and walkin html view.
     *
     * @param none
     * @return \Illuminate\Http\Response
     */
    public function timer()
    {
        return view('shipping.timer');
    }

    /**
     * updates the clock times in the database to reflect if we've paused the timer or not, so that the timer can be viewed from multiple devices
     * and maintain the synchronus time between them.
     *
     * @param bool $pause
     * @return none
     */
    public function clock_update($pause = false)
    {
        if ($pause == true) {
            $now = Carbon::now();
            DB::table('shipping_clock')->where('id', '=', 1)->update([
                'clock_stopped' => $now->toDateTimeString(),
                'paused' => 1,
                'updated_at' => $now->toDateTimeString(),
            ]);

            return;
        } else {
            $data = DB::table('shipping_clock')->where('id', '=', 1)->take(1)->get();
            $diff = '';

            $start = Carbon::now();
            $end = Carbon::parse(''.$data[0]->updated_at.'');
            $diff = $end->diffInSeconds($start);

            $clock = Carbon::parse(''.$data[0]->clock.'');
            $clock->addSeconds($diff);

            DB::table('shipping_clock')->where('id', '=', 1)->update([
                'clock' => $clock->toDateTimeString(),
                'paused' => 0,
            ]);
        }
    }

    /**
     * resets the shipping clock back to the 45 min increment by getting the current time, adding the requested time and setting the database values accordingly.
     *
     * @param none
     * @return int
     */
    public function clock_reset()
    {
        $now = Carbon::now();
        $then = clone $now;
        DB::table('shipping_clock')->where('id', '=', 1)->update([
            'clock' => $then->addMinutes(45),
            'paused' => 1,
            'clock_stopped' => $now->toDateTimeString(),
            'created_at' => $now->toDateTimeString(),
            'updated_at' => $now->toDateTimeString(),
        ]);

        $diff = $now->diffInSeconds($then);

        return $diff;
    }

    /**
     * determines if the clock is paused and returns a numerical value to our ajax script.
     *
     * @param none
     * @return int
     */
    public function clock_paused()
    {
        $data = DB::table('shipping_clock')->where('id', '=', 1)->take(1)->get();
        if (isset($data[0])) {
            return $data[0]->paused;
        }

        return 0;
    }

    /**
     * initialize the shipping clock.
     *
     * @param bool $reset
     * @return int
     */
    public function clock_init($reset = false)
    {
        $data = DB::table('shipping_clock')->where('id', '=', 1)->take(1)->get();
        $diff = '';

        if ($reset != true) {
            if (isset($data[0])) {
                if ($data[0]->paused == 1) {
                    $start = Carbon::parse(''.$data[0]->clock_stopped.'')->setTimezone('America/Chicago');
                    $end = Carbon::parse(''.$data[0]->clock.'')->setTimezone('America/Chicago');
                    $diff = $end->diffInSeconds($start);

                    return $diff;
                } else {
                    $start = Carbon::now()->setTimezone('America/Chicago');
                    $end = Carbon::parse(''.$data[0]->clock.'')->setTimezone('America/Chicago');
                    $diff = $end->diffInSeconds($start);

                    return $diff;
                }
            }
        } else {
            if (empty($data)) {
                $now = Carbon::now();
                $then = clone $now;
                DB::table('shipping_clock')->insert([
                    'clock' => $then->addMinutes(45),
                    'paused' => 0,
                    'clock_stopped' => $now->toDateTimeString(),
                    'created_at' => $now->toDateTimeString(),
                    'updated_at' => $now->toDateTimeString(),
                ]);

                $diff = $now->diffInSeconds($then);

                return $diff;
            } else {
                $now = Carbon::now();
                $then = clone $now;
                DB::table('shipping_clock')->where('id', '=', 1)->update([
                    'clock' => $then->addMinutes(45),
                    'paused' => 0,
                    'clock_stopped' => $now->toDateTimeString(),
                    'created_at' => $now->toDateTimeString(),
                    'updated_at' => $now->toDateTimeString(),
                ]);

                $diff = $now->diffInSeconds($then);

                return $diff;
            }
        }
    }

    /**
     * displays the walkins pending html view for shipping.
     *
     * @param none
     * @return \Illuminate\Http\Response
     */
    public function walkin()
    {
        return view('shipping.walkins');
    }

    /**
     * the walkin ajax method to get the data from the database.
     *
     * @param none
     * @return JSON
     */
    public function walkindata()
    {
        $orders = DB::table('walkin_customers')->where('completed', '=', 0)->get();
        foreach ($orders as $order) {
            $order->created_at = Carbon::parse(''.$order->created_at.'')->setTimezone('America/Chicago');
            $order_detail = DB::table('walkin_customers_order_detail')->where('parent_id', '=', $order->id)->get();
            $order->detail = $order_detail;
        }

        echo json_encode($orders);
        exit;
    }

    /**
     * sets walkin order status to procecssing, if order id exists.
     *
     * @param int $id
     * @return none
     */
    public function updatewalkindata($id)
    {
        if ($id !== '') {
            DB::table('walkin_customers')->where('id', '=', $id)->where('completed', '=', 0)->update([
                'status' => 'processing',
            ]);

            return;
        }
    }

    /**
     * marks walkin order complete in the database.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function completewalkin($id)
    {
        $data = DB::table('walkin_customers')->where('id', '=', $id)->where('status', '!=', 'completed')->get();
        if ($id !== '' && ! empty($data)) {
            $now = Carbon::now();
            $data[0]->created_at = Carbon::parse(''.$data[0]->created_at.'');
            $time_diff = ($now->diffInSeconds($data[0]->created_at) / 60);

            DB::table('walkin_customers')->where('id', '=', $id)->update([
                'status' => 'completed',
                'completed' => 1,
                'elapsed_time' => $time_diff,
                'updated_at' => $now->toDateTimeString(),
            ]);
        }

        return Redirect::route('cs.walkins');
    }

    /**
     * front desk view of walkin orders.
     *
     * @param none
     * @return \Illuminate\Http\Response
     */
    public function cswalkins()
    {
        $orders = DB::table('walkin_customers')->where('completed', '=', 0)->get();
        foreach ($orders as $order) {
            $order->created_at = Carbon::parse(''.$order->created_at.'')->setTimezone('America/Chicago');
            $order_detail = DB::table('walkin_customers_order_detail')->where('parent_id', '=', $order->id)->get();
            $order->detail = $order_detail;
        }

        return view('customerservice.walkins', compact('orders', $orders));
    }
}
