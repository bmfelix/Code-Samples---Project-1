<?php

namespace App\Http\Controllers\Club;

use App\Services\Curl;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ClubController extends Controller
{
    public function index()
    {
        return view('club.club');
    }

    public function active()
    {
        return view('club.active');
    }
    /**
     * accept POST data from the deactivate form to turn off club tags from customer accounts via csv.
     *
     * @param  Request  $request
     * @return REDIRECT TO VIEW
     */
    public function deactivate(Request $request)
    {
        $this->validate($request, [
            'csv' => 'required|mimes:csv,txt'
        ], [
            'csv.required' => 'You need to upload a file!',
            'csv.mimes' => 'That file is not the correct format!'
        ]);

        $path = storage_path().'/app/';
        $fileName = Uuid::generate(4).'.txt'; // renameing image
        $request->file('csv')->move($path, $fileName); // uploading file to given path
        $result = $this->processfile($fileName);

        if ($result === true) {
            $request->session()->flash('status', 'Imported successfully');
        } else {
            $request->session()->flash('statuserror', 'Not a valid file');
        }

        return redirect('club/update');
    }
    /**
     * accepts POST data from the bulk add tags to customer accounts via email address in a csv.
     *
     * @param  Request  $request
     * @return REDIRECT TO VIEW
     */
    public function activate(Request $request)
    {
        $this->validate($request, [
            'csv' => 'required|mimes:csv,txt'
        ], [
            'csv.required' => 'You need to upload a file!',
            'csv.mimes' => 'That file is not the correct format!'
        ]);

        $path = storage_path().'/app/';
        $fileName = Uuid::generate(4).'.txt'; // renaming image
        $request->file('csv')->move($path, $fileName); // uploading file to given path
        $result = $this->processActivateFile($fileName);

        if ($result === true) {
            $request->session()->flash('status', 'Imported successfully');
        } else {
            $request->session()->flash('statuserror', 'Not a valid file');
        }
        return redirect('club/bulktag');
    }
    /**
     * process the csv file from the bulk removal.
     *
     * @param  string  $fileName
     * @return bool
     */
    public function processfile($fileName)
    {
        ini_set('max_execution_time', 0);
        $path = storage_path('app/'.$fileName.'');

        if (($handle = fopen($path, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                $email = $data[2];
                $this->processUser($email);
            }
            fclose($handle);
            return true;
        }
        return false;
    }
    /**
     * process the csv file from the bulk adding.
     *
     * @param  string  $fileName
     * @return bool
     */
    public function processActivateFile($fileName)
    {
        ini_set('max_execution_time', 0);
        $path = storage_path('app/'.$fileName.'');

        if (($handle = fopen($path, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                $email = $data[0];
                $this->activateUser($email);
            }
            fclose($handle);
            return true;
        }
        return false;
    }
    /**
     * takes email address from parsed csv and builds a proper json object and then pushes it to shopify via cURL.  This is for bulk removal.
     *
     * @param  string  $email
     * @return boolean
     */
    public function processUser($email)
    {
        $url = 'https://' . '';
        $url_params = 'admin/customers/search.json?query='.$email.'';
        $crafted_url = $url.'/'.$url_params;
        $shopify_data = json_decode(Curl::get($crafted_url));

        if (! empty($shopify_data->customers)) {
            foreach ($shopify_data->customers as $customer) {
                $tags = explode(',', $customer->tags);

                foreach ($tags as $key => $value) {
                    if ($value == '') {
                        unset($tags[$key]);
                        $new_tags = array_values($tags);
                        $new_tags = implode(',', $new_tags);
                        if (substr($new_tags, 0, 1) === ' ') {
                            $new_tags = substr($new_tags, 1);
                        }
                        $new_customer_data = [
                            'customer' => [
                                'id' => $customer->id,
                                'tags' => $new_tags,
                            ],
                        ];

                        $json = json_encode($new_customer_data);
                        $update_params = 'admin/customers/'.$customer->id.'.json';
                        $update_customer_url = $url.'/'.$update_params;

                        Curl::put($update_customer_url, $json);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /**
     * takes email address from parsed csv and builds a proper json object and then pushes it to shopify via cURL. This is for bulk adding.
     *
     * @param  string  $email
     * @return boolean
     */
    public function activateUser($email)
    {
        $url = 'https://' . '';
        $url_params = 'admin/customers/search.json?query='.$email.'';
        $crafted_url = $url.'/'.$url_params;
        $shopify_data = json_decode(Curl::get($crafted_url));

        if (! empty($shopify_data->customers)) {
            foreach ($shopify_data->customers as $customer) {
                $tags = explode(',', $customer->tags);
                if ($customer->tags == '') {
                    $tags = [];
                }

                $club_tag_found = self::has_tag('', $customer->tags);

                if ($club_tag_found === false) {
                    $tags[] = '';
                    $new_tags = implode(',', $tags);

                    $new_customer_data = [
                        'customer' => [
                            'id' => $customer->id,
                            'tags' => $new_tags,
                        ],
                    ];

                    $json = json_encode($new_customer_data);
                    $update_params = 'admin/customers/'.$customer->id.'.json';
                    $update_customer_url = $url.'/'.$update_params;

                    Curl::put($update_customer_url, $json);
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * checks string of tags for a specific tag.
     *
     * @param  string  $needle
     * @param  string   $haystack
     * @return boolean
     */
    private function has_tag($needle, $haystack)
    {
        $haystack = explode(', ', $haystack);
        foreach ($haystack as $stack) {
            if (strtolower($needle) == strtolower($stack)) {
                return true;
            }
        }
        return false;
    }
}
