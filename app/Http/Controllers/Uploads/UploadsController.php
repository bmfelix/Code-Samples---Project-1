<?php

namespace App\Http\Controllers\Uploads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;

class UploadsController extends Controller
{
    public function index() 
    {
        return view('pages.imageupload');
    }

    public function UploadImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'bail|required|image',
        ]);

        $file = $request->file('image');

        Storage::disk('gdrive')->put(
            preg_split('/[ \.\-\_]/', $file->getClientOriginalName())[0] . '.' . $file->getClientOriginalExtension(),
            file_get_contents($file->getRealPath())
        );

        return redirect('image-upload')->with('message', "Image uploaded successfully.");
    }
}

?>