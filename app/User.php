<?php

namespace App;

use DB;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Detach all roles from a user.
     *
     * @return object
     */
    public function detachUserRoleById($role_id, $user_id)
    {
        $matchThese = ['user_id' => $user_id, 'role_id' => $role_id];
        $data = DB::table('role_user')->where($matchThese)->delete();

        return $data;
    }
}
