<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thresholds extends Model
{
    protected $table = 'thresholds';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = array('id', 'created_at', 'updated_at');
}
