<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RechargeCustomer extends Model
{
    /**
     * @var string
     */
    protected $table = 'recharge_customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_name', 'customer_email', 'old_variant', 'new_variant', 'order_number',
        'created_at', 'updated_at', 'changed_in_shopify'
    ];

    /**
     * @param $id
     * @return array|static[]
     */
    public static function getCustomer($id)
    {
        return DB::table('recharge_customers')
            ->where('id', '=', $id)
            ->get();
    }

    public static function returnCustomers()
    {
        return DB::table('recharge_customers')
            ->where('changed_in_shopify', '<=', 2)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public static function updateOne($order_name, $update)
    {
    	$now = Carbon::now();
        $result = DB::table('recharge_customers')
            ->where('order_number', '=', $order_name)
            ->pluck('changed_in_shopify');

        if ($result[0] < 4) {

        	if ($update == 1) {
		        DB::table('recharge_customers')
		         ->where('order_number', '=', $order_name)
		         ->update([
			         'changed_in_shopify' => $update,
			         'updated_at' => $now
		         ]);
	        } else {
		        DB::table('recharge_customers')
		          ->where('order_number', '=', $order_name)
		          ->update([ 'changed_in_shopify' => $update, ]);
	        }
        }

        return;
    }

    public static function updateChangedOrders()
    {
        DB::table('recharge_customers')
            ->where('changed_in_shopify', '=', 1)
            ->update(['changed_in_shopify' => 4]);
    }

    public static function removeOrders($order)
    {
        DB::table('recharge_customers')
            ->where('order_number', '=', $order)
            ->update(['changed_in_shopify' => 4]);
    }

    public static function getChanges()
    {
        return DB::table('recharge_customers')
            ->where('changed_in_shopify', '<', 4)
            ->get();
    }
}
