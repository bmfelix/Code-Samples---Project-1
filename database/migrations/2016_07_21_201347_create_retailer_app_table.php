<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailerAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retailer_app', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('title', 100);
            $table->bigInteger('taxid');
            $table->string('cname');
            $table->string('phone', 20);
            $table->string('email', 100);
            $table->string('caddress');
            $table->string('city', 100);
            $table->string('state', 50);
            $table->string('zip', 5);
            $table->date('bcommenced');
            $table->smallInteger('businesstype');
            $table->string('pbaddress', 100);
            $table->string('pbcity', 100);
            $table->string('pbstate', 50);
            $table->string('pbzip', 5);
            $table->string('pbphone', 20);
            $table->string('pbemail', 100);
            $table->string('lengthataddress', 100);
            $table->string('bankname', 100);
            $table->string('bankaddress', 100);
            $table->string('bankcity', 100);
            $table->string('bankstate', 50);
            $table->string('bankzip', 5);
            $table->string('bankphone', 20);
            $table->smallInteger('accounttype');
            $table->string('refname1', 100);
            $table->string('refaddress1', 100);
            $table->string('refcity1', 100);
            $table->string('refstate1', 50);
            $table->string('refzip1', 5);
            $table->string('refphone1', 20);
            $table->string('refemail1', 100);
            $table->string('refname2', 100);
            $table->string('refaddress2', 100);
            $table->string('refcity2', 100);
            $table->string('refstate2', 50);
            $table->string('refzip2', 5);
            $table->string('refphone2', 20);
            $table->string('refemail2', 100);
            $table->string('refname3', 100);
            $table->string('refaddress3', 100);
            $table->string('refcity3', 100);
            $table->string('refstate3', 50);
            $table->string('refzip3', 5);
            $table->string('refphone3', 20);
            $table->string('refemail3', 100);
            $table->string('signaturetitle', 100);
            $table->string('signatureinitials', 5);
            $table->tinyInteger('approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('retailer_app');
    }
}
