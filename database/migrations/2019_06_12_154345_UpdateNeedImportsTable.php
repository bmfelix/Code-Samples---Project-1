<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNeedImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('need_import_live', function (Blueprint $table) {
            //
            $table->tinyInteger('is_club');
            $table->tinyInteger('is_twoFour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('need_import_live', function (Blueprint $table) {
            //
        });
    }
}
