<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('customer_data');
        Schema::create('customer_data', function(Blueprint $table)
        {
          $table->increments('id');
          $table->string('name');
          $table->string('email');
          $table->integer('shopify_id');
          $table->float('lifetime_value');
          $table->string('last_order');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_data');
    }
}
