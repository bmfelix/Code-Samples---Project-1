<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('shipping_classifications');
        Schema::create('shipping_classifications', function (Blueprint $table)
        {
          $table->increments('id');
          $table->string('month');
          $table->string('type');
          $table->integer('shopify_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('shipping_classifications');
    }
}
