<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingClock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_clock', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('clock');
            $table->dateTime('clock_stopped');
            $table->tinyInteger('paused');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_clock');
    }
}
