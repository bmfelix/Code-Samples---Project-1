<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingClassificationsTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('shipping_classifications_totals');
        Schema::create('shipping_classifications_totals', function (Blueprint $table)
        {
          $table->increments('id');
          $table->string('month');
          $table->string('type');
          $table->integer('quantity');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('shipping_classifications_totals');
    }
}