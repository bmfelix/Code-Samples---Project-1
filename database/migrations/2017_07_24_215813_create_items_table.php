<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->integer('ordernumber');
            $table->string('ordertype');
            $table->date('orderdate');
            $table->string('offertype');
            $table->string('offercode');
            $table->string('itemcode');
            $table->integer('quantity');
            $table->float('productamount');
            $table->float('shamount');
            $table->string('linestatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
