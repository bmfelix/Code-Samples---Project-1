<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class twoFourInventoryTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twoFour_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->string('threshold');
            $table->integer('quantity');
            $table->dateTime('last_sent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('twoFour_inventory');
    }
}
