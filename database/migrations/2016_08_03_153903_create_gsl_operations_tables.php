<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGslOperationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gsl', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('end_date');
            $table->integer('sales_goal');
            $table->integer('payout');
            $table->integer('min_quantity');
            $table->integer('items_sold')->nullable();
            $table->bigInteger('product_id');
            $table->tinyInteger('campaign_active');
            $table->tinyInteger('display_payout');
            $table->string('payout_title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gsl');
    }
}
