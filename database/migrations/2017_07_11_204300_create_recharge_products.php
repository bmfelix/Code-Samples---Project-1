<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRechargeProducts extends Migration
{
    public function up()
    {
        Schema::create('recharge_products', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('variant_id');
            $table->string('product_title');
            $table->string('variant_title');
            $table->bigInteger('product_id');
            $table->float('price');
            $table->integer('value');
        });
    }

    public function down()
    {
        Schema::drop('recharge_products');
    }
}
