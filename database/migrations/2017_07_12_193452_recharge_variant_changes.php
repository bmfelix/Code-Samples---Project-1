<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RechargeVariantChanges extends Migration
{
    public function up()
    {
        Schema::create('recharge_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name');
            $table->string('customer_email');
            $table->string('old_variant');
            $table->string('new_variant');
            $table->string('order_number');
            $table->integer('changed_in_shopify');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('recharge_customers');
    }
}
