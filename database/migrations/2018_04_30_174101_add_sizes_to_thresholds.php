<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSizesToThresholds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('thresholds', function (Blueprint $table) {
	        $table->string('s_m_low')->nullable()->after('s_low');
	        $table->string('s_m_high')->nullable()->after('s_high');
	        $table->string('l_xl_low')->nullable()->after('l_low');
	        $table->string('l_xl_high')->nullable()->after('l_high');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('thresholds', function (Blueprint $table) {
	        $table->dropColumn('s_m_low');
	        $table->dropColumn('l_xl_low');
	        $table->dropColumn('s_m_high');
	        $table->dropColumn('l_xl_high');
        });
    }
}
