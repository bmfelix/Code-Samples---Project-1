<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Thresholds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thresholds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->string('title');
            $table->integer('xs_low')->nullable();
            $table->integer('s_low')->nullable();
            $table->integer('m_low')->nullable();
            $table->integer('l_low')->nullable();
            $table->integer('xl_low')->nullable();
            $table->integer('xxl_low')->nullable();
            $table->integer('xxxl_low')->nullable();
            $table->integer('xs_high')->nullable();
            $table->integer('s_high')->nullable();
            $table->integer('m_high')->nullable();
            $table->integer('l_high')->nullable();
            $table->integer('xl_high')->nullable();
            $table->integer('xxl_high')->nullable();
            $table->integer('xxxl_high')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thresholds');
    }
}
