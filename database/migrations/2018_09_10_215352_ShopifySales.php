<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopifySales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopify_sales', function (Blueprint $table) {
            $table->string('id');
            $table->string('email');
            $table->float('total_price');
            $table->float('discount');
            $table->string('discount_code');
            $table->float('free_shipping_discount');
            $table->string('product_name');
            $table->string('sku');
            $table->string('billing_address');
            $table->string('billing_city');
            $table->string('billing_region');
            $table->string('billing_zip');
            $table->string('shipping_address');
            $table->string('shipping_city');
            $table->string('shipping_region');
            $table->string('shipping_zip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('shopify_sales');
    }
}
