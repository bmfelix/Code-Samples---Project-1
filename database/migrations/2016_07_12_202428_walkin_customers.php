<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WalkinCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walkin_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('order_id');
            $table->string('status');
            $table->tinyInteger('completed');
            $table->float('elapsed_time');
            $table->timestamps();
        });

        Schema::create('walkin_customers_order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('parent_id');
            $table->bigInteger('order_id');
            $table->bigInteger('product_id');
            $table->string('sku');
            $table->string('product');
            $table->string('size');
            $table->integer('quantity');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('walkin_customers');
        Schema::drop('walkin_customers_order_detail');
    }
}
