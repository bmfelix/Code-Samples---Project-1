<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->integer('customernumber');
            $table->integer('membershipnumber');
            $table->integer('ordernumber');
            $table->date('orderdate');
            $table->string('shiplegno');
            $table->string('paymenttype');
            $table->string('creditcardtype');
            $table->float('totalamount');
            $table->float('totalpaid');
            $table->integer('totalreturns');
            $table->integer('totaladjustments');
            $table->integer('totalrefunds');
            $table->integer('totalchargebacks');
            $table->integer('totalwriteoffs');
            $table->float('tax');
            $table->float('taxpercent');
            $table->float('sh');
            $table->float('shtax');
            $table->float('shtaxpercent');
            $table->string('billing_titlecode');
            $table->string('billing_midinitial')->nullable();
            $table->string('billing_lastname');
            $table->string('billing_addr1');
            $table->string('billing_addr2')->nullable();
            $table->string('billing_zip');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_tld');
            $table->string('billing_phonenumber')->nullable();
            $table->string('billing_email')->nullable();
            $table->string('shipping_titlecode');
            $table->string('shipping_midinitial')->nullable();
            $table->string('shipping_lastname');
            $table->string('shipping_addr1');
            $table->string('shipping_addr2')->nullable();
            $table->string('shipping_zip');
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_tld');
            $table->string('shipping_phonenumber')->nullable();
            $table->string('shipping_email')->nullable();
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
