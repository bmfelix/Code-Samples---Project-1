<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShirtDesignerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('garment_type');
            $table->string('garment_color');
            $table->string('sex');
            $table->tinyInteger('leftsleeve');
            $table->tinyInteger('rightsleeve');
            $table->string('sleeve_ink')->nullable();
            $table->string('front_ink_number')->nullable();
            $table->string('back_ink_number')->nullable();
            $table->integer('total_quantity')->nullable();
            $table->integer('small_men');
            $table->integer('medium_men');
            $table->integer('large_men');
            $table->integer('xlarge_men');
            $table->integer('xxlarge_men');
            $table->integer('xxxlarge_men');
            $table->integer('xsmall_women');
            $table->integer('small_women');
            $table->integer('medium_women');
            $table->integer('large_women');
            $table->integer('xlarge_women');
            $table->integer('xxlarge_women');
            $table->string('front_image')->nullable();
            $table->string('back_image')->nullable();
            $table->string('frontcomposite');
            $table->string('backcomposite');
            $table->string('complex')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('designer');
    }
}
