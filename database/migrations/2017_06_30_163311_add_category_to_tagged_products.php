<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToTaggedProducts extends Migration
{
    public function up()
    {
        Schema::table('TaggedProducts', function (Blueprint $table) {
            $table->string('category')->after('id');
        });
    }

    public function down()
    {
        Schema::table('TaggedProducts', function (Blueprint $table) {
            //
        });
    }
}
